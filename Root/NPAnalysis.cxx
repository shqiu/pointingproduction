#include "NonPointingAnalysis/NPAnalysis.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include "HGamAnalysisFramework/HGamVariables.h"
#include "OfflineTimingTool/OfflineTimingTool.h"
#include "PhotonVertexSelection/PhotonVertexHelpers.h"
#include "PhotonVertexSelection/PhotonPointingTool.h"
#include "TrigBunchCrossingTool/WebBunchCrossingTool.h"
#include "TrigBunchCrossingTool/StaticBunchCrossingTool.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"
// ROOT include(s):
#include "TTree.h"
#include "TBranch.h"


// this is needed to distribute the algorithm to the workers
ClassImp(NPAnalysis)

CP::PhotonPointingTool * PPT = new CP::PhotonPointingTool("PPT");
OfflineTimingTool * m_ott = new OfflineTimingTool(false);
NPAnalysis::NPAnalysis(const char * name): HgammaAnalysis(name) {
    // Here you put any code for the base initialization of variables,
    // e.g. initialize all pointers to 0.  Note that you should only put
    // the most basic initialization here, since this method will be
    // called on both the submission and the worker node.  Most of your
    // initialization code will go into histInitialize() and
    // initialize().

    //The following code initializes the triggers we want to save in our output file along with the minimum nad maximum thresholds
    triggerMap["HLT_g10_loose"] = 0;
    triggerMap["HLT_g15_loose_L1EM7"] = 0;
    triggerMap["HLT_g20_loose_L1EM12"] = 0;
    triggerMap["HLT_g25_loose_L1EM15"] = 0;
    triggerMap["HLT_g35_loose_L1EM15"] = 0;
    triggerMap["HLT_g40_loose_L1EM15"] = 0;
    triggerMap["HLT_g45_loose_L1EM15"] = 0;
    triggerMap["HLT_g50_loose_L1EM15"] = 0;
    triggerMap["HLT_g60_loose"] = 0;
    triggerMap["HLT_g70_loose"] = 0;
    triggerMap["HLT_g80_loose"] = 0;
    triggerMap["HLT_g100_loose"] = 0;
    triggerMap["HLT_g120_loose"] = 0;
    triggerMap["HLT_g140_loose"] = 0;
    triggerMap["HLT_g35_loose_g25_loose"] = 0;
    triggerMap["HLT_g35_medium_g25_medium"] = 0;
    triggerMap["HLT_2g50_loose"] = 0;
    triggerMap["HLT_2g20_tight"] = 0; //triggerMap["HLT_g35_medium_g25_medium"] = 0;
    triggerPtLow["HLT_g10_loose"] = 15;
    triggerPtLow["HLT_g15_loose_L1EM7"] = 20;
    triggerPtLow["HLT_g20_loose_L1EM12"] = 25;
    triggerPtLow["HLT_g25_loose_L1EM15"] = 30;
    triggerPtLow["HLT_g35_loose_L1EM15"] = 40;
    triggerPtLow["HLT_g40_loose_L1EM15"] = 45;
    triggerPtLow["HLT_g45_loose_L1EM15"] = 50;
    triggerPtLow["HLT_g50_loose_L1EM15"] = 55;
    triggerPtLow["HLT_g60_loose"] = 65;
    triggerPtLow["HLT_g70_loose"] = 75;
    triggerPtLow["HLT_g80_loose"] = 85;
    triggerPtLow["HLT_g100_loose"] = 105;
    triggerPtLow["HLT_g120_loose"] = 125;
    triggerPtLow["HLT_g140_loose"] = 145;
    triggerPtHigh["HLT_g10_loose"] = 20;
    triggerPtHigh["HLT_g15_loose_L1EM7"] = 25;
    triggerPtHigh["HLT_g20_loose_L1EM12"] = 30;
    triggerPtHigh["HLT_g25_loose_L1EM15"] = 40;
    triggerPtHigh["HLT_g35_loose_L1EM15"] = 45;
    triggerPtHigh["HLT_g40_loose_L1EM15"] = 50;
    triggerPtHigh["HLT_g45_loose_L1EM15"] = 55;
    triggerPtHigh["HLT_g50_loose_L1EM15"] = 65;
    triggerPtHigh["HLT_g60_loose"] = 75;
    triggerPtHigh["HLT_g70_loose"] = 85;
    triggerPtHigh["HLT_g80_loose"] = 105;
    triggerPtHigh["HLT_g100_loose"] = 125;
    triggerPtHigh["HLT_g120_loose"] = 145;
    triggerPtHigh["HLT_g140_loose"] = 10000;

    //Electron triggers
    triggerMap["HLT_e26_lhtight_nod0_ivarloose"] = 0;
    triggerMap["HLT_e26_lhtight_smooth_ivarloose"] = 0;
    triggerMap["HLT_e24_lhtight_nod0_ivarloose"] = 0;
    triggerMap["HLT_e24_lhmedium_nod0_ivarloose"] = 0;
    triggerMap["HLT_e24_medium_iloose_EM18VH"] = 0;
    triggerMap["HLT_e24_medium_iloose_EM20VH"] = 0;
    triggerMap["HLT_e24_lhmedium_L1EM20VH"] = 0;
    triggerMap["HLT_e24_tight_iloose_EM20VH"] = 0;
    triggerMap["HLT_e24_tight_iloose"] = 0;
    triggerMap["HLT_e26_tight_iloose"] = 0;
    triggerMap["HLT_e120_loose"] = 0;
    triggerMap["HLT_e140_loose"] = 0;
    triggerMap["HLT_e24_lhmedium_iloose_EM18VH"] = 0;
    triggerMap["HLT_e24_lhmedium_iloose_EM20VH"] = 0;
    triggerMap["HLT_e24_lhtight_iloose_EM20VH"] = 0;
    triggerMap["HLT_e24_lhtight_iloose"] = 0;
    triggerMap["HLT_e26_lhtight_iloose"] = 0;
    triggerMap["HLT_e60_lhmedium"] = 0;
    triggerMap["HLT_e120_lhloose"] = 0;
    triggerMap["HLT_e140_lhloose"] = 0;

    triggerMap["HLT_e60_lhmedium_nod0"] = 0;
    triggerMap["HLT_e60_medium"] = 0;
    triggerMap["HLT_e140_lhloose_nod0"] = 0;
    triggerMap["HLT_e120_lhloose_nod0"] = 0;
    triggerMap["HLT_2e17_lhvloose_nod0"] = 0;
    triggerMap["HLT_2e15_lhvloose_nod0_L12EM13VH"] = 0;
    triggerMap["HLT_2e12_lhlvoose_nod0_L12EM10VH"] = 0;
    triggerMap["HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH"] = 0;
    triggerMap["HLT_e17_lhloose_nod0_2e9_lhloose_nod0"] = 0;
    //Muon triggers
    triggerMap["HLT_mu26_ivarmedium"] = 0;
    triggerMap["HLT_mu28_ivarmedium"] = 0;
    triggerMap["HLT_mu26_imedium"] = 0;
    triggerMap["HLT_mu24_ivarmedium"] = 0;
    triggerMap["HLT_mu24_imedium"] = 0;
    triggerMap["HLT_mu24_ivarloose"] = 0;
    triggerMap["HLT_mu50"] = 0;
    triggerMap["HLT_mu40"] = 0;
    triggerMap["HLT_mu22_mu8noL1"] = 0;
    triggerMap["HLT_mu20_mu8noL1"] = 0;
    triggerMap["HLT_mu20_2mu4noL1"] = 0;
    triggerMap["HLT_3mu6_msonly"] = 0;
    triggerMap["HLT_mu20_iloose_L1MU15"] = 0;

    //Combined triggers
    triggerMap["HLT_e7_lhmedium_nod0_mu24"] = 0;
    triggerMap["HLT_g25_medium_mu24"] = 0;
    triggerMap["HLT_e17_lhloose_nod0_mu14"] = 0;
    triggerMap["HLT_2e12_lhloose_nod0_mu10"] = 0;
    triggerMap["HLT_e70_lhloose_nod0_xe70noL1"] = 0;
    triggerMap["HLT_j80_bmv2c2060_split_xe60_L12J50_XE40"] = 0;
    //MET triggers
    triggerMap["HLT_xe130_mht_L1XE50"] = 0;
    triggerMap["HLT_xe100_L1XE50"] = 0;
    triggerMap["HLT_xe110_mht_L1XE50_AND_xe70_L1XE50"] = 0;
    triggerMap["HLT_xe100_mht_L1XE50"] = 0;
    triggerMap["HLT_xe110_mht_L1XE50"] = 0;
    triggerMap["HLT_xe80_tc_lcw_L1XE50"] = 0;
    triggerMap["HLT_xe90_mht_L1XE50"] = 0;

    //2017 triggers
    triggerMap["HLT_e28_lhtight_nod0_ivarloose"] = 0;
    triggerMap["HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM"] = 0;
    triggerMap["HLT_e26_lhtight_nod0_ivarloose"] = 0;
    triggerMap["HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM"] = 0;
    triggerMap["HLT_e60_lhmedium_nod0_L1EM24VHI"] = 0;
    triggerMap["HLT_e60_lhmedium_nod0"] = 0;
    triggerMap["HLT_e140_lhloose_nod0_L1EM24VHI"] = 0;
    triggerMap["HLT_e140_lhloose_nod0"] = 0;
    triggerMap["HLT_e300_etcut"] = 0;
    triggerMap["HLT_e300_etcut_L1EM24VHI"] = 0;
    triggerMap["HLT_2e24_lhvloose_nod0"] = 0;
    triggerMap["HLT_2e17_lhvloose_nod0_L12EM15VHI"] = 0;

    triggerMap["HLT_g140_tight"] = 0;
    triggerMap["HLT_g200_loose"] = 0;
    triggerMap["HLT_g300_etcut_L1EM24VHI"] = 0;
    triggerMap["HLT_g300_etcut"] = 0;
    triggerMap["HLT_2g20_tight_icalovloose_L12EM15VHI"] = 0;
    triggerMap["HLT_2g25_tight_L12EM20VH"] = 0;
    triggerMap["HLT_g35_medium_g25_medium_L12EM20VH"] = 0;
    triggerMap["HLT_2g50_loose_L12EM20VH"] = 0;

    triggerMap["HLT_mu60"] = 0;
    triggerMap["HLT_mu24_mu8noL1"] = 0;
    triggerMap["HLT_mu24_mu8noL1_calotag_0eta010"] = 0;
    triggerMap["HLT_mu26_mu10noL1"] = 0;

    triggerPtLow["HLT_e28_lhtight_nod0_ivarloose"] = 0;
    triggerPtLow["HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM"] = 0;
    triggerPtLow["HLT_e26_lhtight_nod0_ivarloose"] = 0;
    triggerPtLow["HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM"] = 0;
    triggerPtLow["HLT_e60_lhmedium_nod0_L1EM24VHI"] = 0;
    triggerPtLow["HLT_e60_lhmedium_nod0"] = 0;
    triggerPtLow["HLT_e140_lhloose_nod0_L1EM24VHI"] = 0;
    triggerPtLow["HLT_e140_lhloose_nod0"] = 0;
    triggerPtLow["HLT_e300_etcut"] = 0;
    triggerPtLow["HLT_e300_etcut_L1EM24VHI"] = 0;
    triggerPtLow["HLT_2e24_lhvloose_nod0"] = 0;
    triggerPtLow["HLT_2e17_lhvloose_nod0_L12EM15VHI"] = 0;
    triggerPtLow["HLT_g140_tight"] = 0;
    triggerPtLow["HLT_g200_loose"] = 0;
    triggerPtLow["HLT_g300_etcut_L1EM24VHI"] = 0;
    triggerPtLow["HLT_g300_etcut"] = 0;
    triggerPtLow["HLT_2g20_tight_icalovloose_L12EM15VHI"] = 0;
    triggerPtLow["HLT_2g25_tight_L12EM20VH"] = 0;
    triggerPtLow["HLT_g35_medium_g25_medium_L12EM20VH"] = 0;
    triggerPtLow["HLT_2g50_loose_L12EM20VH"] = 0;
    triggerPtLow["HLT_mu60"] = 0;
    triggerPtLow["HLT_mu24_mu8noL1"] = 0;
    triggerPtLow["HLT_mu24_mu8noL1_calotag_0eta010"] = 0;
    triggerPtLow["HLT_mu26_mu10noL1"] = 0;
    triggerPtLow["HLT_e24_lhmedium_L1EM20VH"] = 0;
    triggerPtHigh["HLT_e24_lhmedium_L1EM20VH"] = 10000;

    triggerPtHigh["HLT_e28_lhtight_nod0_ivarloose"] = 10000;
    triggerPtHigh["HLT_e28_lhtight_nod0_ivarloose_L1EM24VHIM"] = 10000;
    triggerPtHigh["HLT_e26_lhtight_nod0_ivarloose"] = 10000;
    triggerPtHigh["HLT_e26_lhtight_nod0_ivarloose_L1EM22VHIM"] = 10000;
    triggerPtHigh["HLT_e60_lhmedium_nod0_L1EM24VHI"] = 10000;
    triggerPtHigh["HLT_e60_lhmedium_nod0"] = 10000;
    triggerPtHigh["HLT_e140_lhloose_nod0_L1EM24VHI"] = 10000;
    triggerPtHigh["HLT_e140_lhloose_nod0"] = 10000;
    triggerPtHigh["HLT_e300_etcut"] = 10000;
    triggerPtHigh["HLT_e300_etcut_L1EM24VHI"] = 10000;
    triggerPtHigh["HLT_2e24_lhvloose_nod0"] = 10000;
    triggerPtHigh["HLT_2e17_lhvloose_nod0_L12EM15VHI"] = 10000;
    triggerPtHigh["HLT_g140_tight"] = 10000;
    triggerPtHigh["HLT_g200_loose"] = 10000;
    triggerPtHigh["HLT_g300_etcut_L1EM24VHI"] = 10000;
    triggerPtHigh["HLT_g300_etcut"] = 10000;
    triggerPtHigh["HLT_2g20_tight_icalovloose_L12EM15VHI"] = 10000;
    triggerPtHigh["HLT_2g25_tight_L12EM20VH"] = 10000;
    triggerPtHigh["HLT_g35_medium_g25_medium_L12EM20VH"] = 10000;
    triggerPtHigh["HLT_2g50_loose_L12EM20VH"] = 10000;
    triggerPtHigh["HLT_mu60"] = 10000;
    triggerPtHigh["HLT_mu24_mu8noL1"] = 10000;
    triggerPtHigh["HLT_mu24_mu8noL1_calotag_0eta010"] = 10000;
    triggerPtHigh["HLT_mu26_mu10noL1"] = 10000;

    //Trigger pT low
    triggerPtLow["HLT_e26_lhtight_nod0_ivarloose"] = 0;
    triggerPtLow["HLT_e26_lhtight_smooth_ivarloose"] = 0;
    triggerPtLow["HLT_e24_lhtight_nod0_ivarloose"] = 0;
    triggerPtLow["HLT_e24_lhmedium_nod0_ivarloose"] = 0;
    triggerPtLow["HLT_e60_lhmedium_nod0"] = 0;
    triggerPtLow["HLT_e60_medium"] = 0;
    triggerPtLow["HLT_e140_lhloose_nod0"] = 0;
    triggerPtLow["HLT_e120_lhloose_nod0"] = 0;
    triggerPtLow["HLT_2e17_lhvloose_nod0"] = 0;
    triggerPtLow["HLT_2e15_lhvloose_nod0_L12EM13VH"] = 0;
    triggerPtLow["HLT_2e12_lhlvoose_nod0_L12EM10VH"] = 0;
    triggerPtLow["HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH"] = 0;
    triggerPtLow["HLT_e17_lhloose_nod0_2e9_lhloose_nod0"] = 0;
    triggerPtLow["HLT_mu26_ivarmedium"] = 0;
    triggerPtLow["HLT_mu26_imedium"] = 0;
    triggerPtLow["HLT_mu24_ivarmedium"] = 0;
    triggerPtLow["HLT_mu24_imedium"] = 0;
    triggerPtLow["HLT_mu24_ivarloose"] = 0;
    triggerPtLow["HLT_mu50"] = 0;
    triggerPtLow["HLT_mu40"] = 0;
    triggerPtLow["HLT_mu22_mu8noL1"] = 0;
    triggerPtLow["HLT_mu20_mu8noL1"] = 0;
    triggerPtLow["HLT_mu20_2mu4noL1"] = 0;
    triggerPtLow["HLT_3mu6_msonly"] = 0;
    triggerPtLow["HLT_e7_lhmedium_nod0_mu24"] = 0;
    triggerPtLow["HLT_g25_medium_mu24"] = 0;
    triggerPtLow["HLT_e17_lhloose_nod0_mu14"] = 0;
    triggerPtLow["HLT_2e12_lhloose_nod0_mu10"] = 0;
    triggerPtLow["HLT_e70_lhloose_nod0_xe70noL1"] = 0;
    triggerPtLow["HLT_j80_bmv2c2060_split_xe60_L12J50_XE40"] = 0;
    triggerPtLow["HLT_xe130_mht_L1XE50"] = 0;
    triggerPtLow["HLT_xe100_L1XE50"] = 0;
    triggerPtLow["HLT_xe110_mht_L1XE50_AND_xe70_L1XE50"] = 0;
    triggerPtLow["HLT_xe100_mht_L1XE50"] = 0;
    triggerPtLow["HLT_xe110_mht_L1XE50"] = 0;
    triggerPtLow["HLT_xe80_tc_lcw_L1XE50"] = 0;
    triggerPtLow["HLT_xe90_mht_L1XE50"] = 0;
    triggerPtLow["HLT_mu20_iloose_L1MU15"] = 0;
    triggerPtLow["HLT_e24_medium_iloose_EM18VH"] = 0;
    triggerPtLow["HLT_e24_medium_iloose_EM20VH"] = 0;
    triggerPtLow["HLT_e24_tight_iloose_EM20VH"] = 0;
    triggerPtLow["HLT_e24_tight_iloose"] = 0;
    triggerPtLow["HLT_e26_tight_iloose"] = 0;
    triggerPtLow["HLT_e120_loose"] = 0;
    triggerPtLow["HLT_e140_loose"] = 0;
    triggerPtLow["HLT_e24_lhmedium_iloose_EM18VH"] = 0;
    triggerPtLow["HLT_e24_lhmedium_iloose_EM20VH"] = 0;
    triggerPtLow["HLT_e24_lhtight_iloose_EM20VH"] = 0;
    triggerPtLow["HLT_e24_lhtight_iloose"] = 0;
    triggerPtLow["HLT_e26_lhtight_iloose"] = 0;
    triggerPtLow["HLT_e60_lhmedium"] = 0;
    triggerPtLow["HLT_e120_lhloose"] = 0;
    triggerPtLow["HLT_e140_lhloose"] = 0;

    //Trigger pT high
    triggerPtHigh["HLT_e26_lhtight_nod0_ivarloose"] = 10000;
    triggerPtHigh["HLT_e26_lhtight_smooth_ivarloose"] = 10000;
    triggerPtHigh["HLT_e24_lhtight_nod0_ivarloose"] = 10000;
    triggerPtHigh["HLT_e24_lhmedium_nod0_ivarloose"] = 10000;
    triggerPtHigh["HLT_e60_lhmedium_nod0"] = 10000;
    triggerPtHigh["HLT_e60_medium"] = 10000;
    triggerPtHigh["HLT_e140_lhloose_nod0"] = 10000;
    triggerPtHigh["HLT_e120_lhloose_nod0"] = 10000;
    triggerPtHigh["HLT_2e17_lhvloose_nod0"] = 10000;
    triggerPtHigh["HLT_2e15_lhvloose_nod0_L12EM13VH"] = 10000;
    triggerPtHigh["HLT_2e12_lhlvoose_nod0_L12EM10VH"] = 10000;
    triggerPtHigh["HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH"] = 10000;
    triggerPtHigh["HLT_e17_lhloose_nod0_2e9_lhloose_nod0"] = 10000;
    triggerPtHigh["HLT_mu26_ivarmedium"] = 10000;
    triggerPtHigh["HLT_mu26_imedium"] = 10000;
    triggerPtHigh["HLT_mu24_ivarmedium"] = 10000;
    triggerPtHigh["HLT_mu24_imedium"] = 10000;
    triggerPtHigh["HLT_mu24_ivarloose"] = 10000;
    triggerPtHigh["HLT_mu50"] = 10000;
    triggerPtHigh["HLT_mu40"] = 10000;
    triggerPtHigh["HLT_mu22_mu8noL1"] = 10000;
    triggerPtHigh["HLT_mu20_mu8noL1"] = 10000;
    triggerPtHigh["HLT_mu20_2mu4noL1"] = 10000;
    triggerPtHigh["HLT_3mu6_msonly"] = 10000;
    triggerPtHigh["HLT_e7_lhmedium_nod0_mu24"] = 10000;
    triggerPtHigh["HLT_g25_medium_mu24"] = 10000;
    triggerPtHigh["HLT_e17_lhloose_nod0_mu14"] = 10000;
    triggerPtHigh["HLT_2e12_lhloose_nod0_mu10"] = 10000;
    triggerPtHigh["HLT_e70_lhloose_nod0_xe70noL1"] = 10000;
    triggerPtHigh["HLT_j80_bmv2c2060_split_xe60_L12J50_XE40"] = 10000;
    triggerPtHigh["HLT_xe130_mht_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe100_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe110_mht_L1XE50_AND_xe70_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe100_mht_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe110_mht_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe80_tc_lcw_L1XE50"] = 10000;
    triggerPtHigh["HLT_xe90_mht_L1XE50"] = 10000;
    triggerPtHigh["HLT_mu20_iloose_L1MU15"] = 10000;
    triggerPtHigh["HLT_e24_medium_iloose_EM18VH"] = 10000;
    triggerPtHigh["HLT_e24_medium_iloose_EM20VH"] = 10000;
    triggerPtHigh["HLT_e24_tight_iloose_EM20VH"] = 10000;
    triggerPtHigh["HLT_e24_tight_iloose"] = 10000;
    triggerPtHigh["HLT_e26_tight_iloose"] = 10000;
    triggerPtHigh["HLT_e120_loose"] = 10000;
    triggerPtHigh["HLT_e140_loose"] = 10000;
    triggerPtHigh["HLT_e24_lhmedium_iloose_EM18VH"] = 10000;
    triggerPtHigh["HLT_e24_lhmedium_iloose_EM20VH"] = 10000;
    triggerPtHigh["HLT_e24_lhtight_iloose_EM20VH"] = 10000;
    triggerPtHigh["HLT_e24_lhtight_iloose"] = 10000;
    triggerPtHigh["HLT_e26_lhtight_iloose"] = 10000;
    triggerPtHigh["HLT_e60_lhmedium"] = 10000;
    triggerPtHigh["HLT_e120_lhloose"] = 10000;
    triggerPtHigh["HLT_e140_lhloose"] = 10000;

    triggerMap["HLT_g35_loose_L1EM15_g25_loose_L1EM15"] = 0;
    triggerMap["HLT_2g22_tight"] = 0;
    triggerPtLow["HLT_g35_loose_L1EM15_g25_loose_L1EM15"] = 25;
    triggerPtLow["HLT_2g22_tight"] = 0;
    triggerPtHigh["HLT_2g22_tight"] = 10000;
    triggerPtHigh["HLT_g35_loose_L1EM15_g25_loose_L1EM15"] = 10000;
    triggerMap["HLT_2mu14"] = 0;
    triggerMap["HLT_mu80"] = 0;
    triggerMap["HLT_mu22_mu8noL1_calotag_0eta010"] = 0;
    triggerMap["HLT_mu28_mu8noL1_calotag_0eta010"] = 0;
    triggerMap["HLT_mu26_mu10noL1"] = 0;
    triggerMap["HLT_e80_lhmedium_nod0_L1EM24VH"] = 0;
    triggerMap["HLT_mu28_mu8noL1"] = 0;
    triggerPtLow["HLT_2mu14"] = 0;
    triggerPtLow["HLT_mu28_mu8noL1"] = 0;
    triggerPtHigh["HLT_mu28_mu8noL1"] = 10000;
    triggerPtLow["HLT_mu80"] = 0;
    triggerPtLow["HLT_mu22_mu8noL1_calotag_0eta010"] = 0;
    triggerPtLow["HLT_mu28_mu8noL1_calotag_0eta010"] = 0;
    triggerPtLow["HLT_mu26_mu10noL1"] = 0;
    triggerPtLow["HLT_e80_lhmedium_nod0_L1EM24VH"] = 0;
    triggerPtHigh["HLT_mu80"] = 10000;
    triggerPtHigh["HLT_mu22_mu8noL1_calotag_0eta010"] = 10000;
    triggerPtHigh["HLT_2mu14"] = 0;
    triggerPtLow["HLT_mu28_ivarmedium"] = 0;
    triggerPtHigh["HLT_mu28_ivarmedium"] = 10000;
    triggerPtHigh["HLT_mu28_mu8noL1_calotag_0eta010"] = 10000;
    triggerPtHigh["HLT_mu26_mu10noL1"] = 10000;
    triggerPtHigh["HLT_e80_lhmedium_nod0_L1EM24VH"] = 10000;

}

EL::StatusCode NPAnalysis::initialize() {
    HgammaAnalysis::initialize(); // keep this line!
    //  cout<<"--- HgammaAnalysis::initialize() ---"<<endl;
    /*  if(HG::isData()){
      Trig::WebBunchCrossingTool *bcTool= new Trig::WebBunchCrossingTool("WebBunchCrossingTool");
      bcTool ->setProperty("OutputLevel", MSG::INFO);
      bcTool ->setProperty("ServerAddress", "atlas-trigconf.cern.ch");
      m_bcTool = bcTool;
      m_bcTool->initialize();
      }*/
    //JBdV     
    //  cout<<"--- Done with bcTool ---"<<endl;
    m_trkPtCut = 1e3;

    //Define all configurations used annd read from the DERIVATION.cfg file
    runPhotons = config() -> getBool("NPAnalysis.runPhotons");
    runPhotonTiming = config() -> getBool("NPAnalysis.runPhotonTiming");
    doUnbiasedPhotonPerformanceStudy = config() -> getBool("NPAnalysis.doUnbiasedPhotonPerformanceStudy");
    runPhotonnoIDIsoCleaning = config() -> getBool("NPAnalysis.donoPhotonIdIsoCleaning");
    runElectronTiming = config() -> getBool("NPAnalysis.runElectronTiming");
    runJets = config() -> getBool("NPAnalysis.runJets");
    runMet = config() -> getBool("NPAnalysis.runMet");
    runElectrons = config() -> getBool("NPAnalysis.runElectrons");
    runMuons = config() -> getBool("NPAnalysis.runMuons");

    m_applySystematicLoop = config() -> getBool("NPAnalysis.SystematicLoop");
    //cout<<"--- Run many different things ---"<<endl;
    SetupOutputTree("");
    return EL::StatusCode::SUCCESS;

}

NPAnalysis::~NPAnalysis() {
    // Here you delete any memory you allocated during your analysis.
}

void NPAnalysis::SetupOutputTree(TString sysname) {

    TString treename = "output";
    if (sysname != "") treename = treename + "_" + sysname;
    m_output_tree = new TTree(treename, treename);
    //m_output_tree->SetDirectory(file);
    map_outputTree[sysname] = m_output_tree;

    m_output_tree -> Branch("runNumber", & runNumber);
    m_output_tree -> Branch("eventNumber", & eventNumber);
    m_output_tree -> Branch("m_mcChannelNumber", & m_mcChannelNumber);
    m_output_tree -> Branch("PV_z", & PV_z);
    m_output_tree -> Branch("mu", & mu);
    m_output_tree -> Branch("mu_actual", & mu_actual);
    m_output_tree -> Branch("tru_ph_pt", & tru_ph_pt);
    m_output_tree -> Branch("tru_ph_eta", & tru_ph_eta);
    m_output_tree -> Branch("tru_ph_phi", & tru_ph_phi);
    m_output_tree -> Branch("tru_ph_E", & tru_ph_E);
    m_output_tree -> Branch("tru_ph_type", & tru_ph_type);
    m_output_tree -> Branch("tru_ph_origin", & tru_ph_origin);
    m_output_tree -> Branch("tru_mom_pt", & tru_mom_pt);
    m_output_tree -> Branch("tru_mom_eta", & tru_mom_eta);
    m_output_tree -> Branch("tru_mom_phi", & tru_mom_phi);
    m_output_tree -> Branch("tru_mom_E", & tru_mom_E);
    m_output_tree -> Branch("tru_mom_pdgid", & tru_mom_pdgid);
    m_output_tree -> Branch("tru_dau_pt", & tru_dau_pt);
    m_output_tree -> Branch("tru_dau_eta", & tru_dau_eta);
    m_output_tree -> Branch("tru_dau_phi", & tru_dau_phi);
    m_output_tree -> Branch("tru_dau_E", & tru_dau_E);
    m_output_tree -> Branch("tru_lep_pt", & tru_lep_pt);
    m_output_tree -> Branch("tru_lep_eta", & tru_lep_eta);
    m_output_tree -> Branch("tru_lep_phi", & tru_lep_phi);
    m_output_tree -> Branch("tru_lep_E", & tru_lep_E);
    m_output_tree -> Branch("tru_jets_pt", & tru_jets_pt);
    m_output_tree -> Branch("tru_jets_eta", & tru_jets_eta);
    m_output_tree -> Branch("tru_jets_phi", & tru_jets_phi);
    m_output_tree -> Branch("tru_jets_E", & tru_jets_E);
    m_output_tree -> Branch("tru_lep_pdgid", & tru_lep_pdgid);
    m_output_tree -> Branch("tru_dau_pdgid", & tru_dau_pdgid);
    m_output_tree -> Branch("tru_phprod_vx", & tru_phprod_vx);
    m_output_tree -> Branch("tru_phprod_vy", & tru_phprod_vy);
    m_output_tree -> Branch("tru_phprod_vz", & tru_phprod_vz);
    m_output_tree -> Branch("tru_phprod_vt", & tru_phprod_vt);
    m_output_tree -> Branch("tru_momprod_vx", & tru_momprod_vx);
    m_output_tree -> Branch("tru_momprod_vy", & tru_momprod_vy);
    m_output_tree -> Branch("tru_momprod_vz", & tru_momprod_vz);
    m_output_tree -> Branch("tru_momprod_vt", & tru_momprod_vt);
    m_output_tree -> Branch("tru_n_ph", & tru_n_ph);
    m_output_tree -> Branch("tru_n_lep", & tru_n_lep);
    m_output_tree -> Branch("tru_n_jets", & tru_n_jets);
    m_output_tree -> Branch("tru_MET", & tru_MET);

    m_output_tree -> Branch("tru_ph_t", & tru_ph_t);
    m_output_tree -> Branch("tru_ph_z", & tru_ph_z);
    m_output_tree -> Branch("tru_mom_tof", & tru_mom_tof);
    m_output_tree -> Branch("tru_ph_tof", & tru_ph_tof);
    m_output_tree -> Branch("tru_promptph_t", & tru_promptph_t);

    m_output_tree -> Branch("ph_SF", & ph_SF);
    m_output_tree -> Branch("ph_pt", & ph_pt);
    m_output_tree -> Branch("ph_eta", & ph_eta);
    m_output_tree -> Branch("ph_phi", & ph_phi);
    m_output_tree -> Branch("ph_E", & ph_E);
    m_output_tree -> Branch("ph_cl_E", & ph_cl_E);
    m_output_tree -> Branch("n_ph", & n_ph);
    m_output_tree -> Branch("ph_t", & ph_t);
    m_output_tree -> Branch("ph_convType", & ph_convType);
    m_output_tree -> Branch("ph_isTight", & ph_isTight);
    m_output_tree -> Branch("ph_isLoose", & ph_isLoose);
    m_output_tree -> Branch("ph_isMedium", & ph_isMedium);
    m_output_tree -> Branch("ph_passIso", & ph_passIso);
    m_output_tree -> Branch("ph_isClean", & ph_isClean);

    m_output_tree -> Branch("ph_conv_z", & ph_conv_z);
    m_output_tree -> Branch("ph_conv_z_err", & ph_conv_z_err);
    m_output_tree -> Branch("ph_calo_z", & ph_calo_z);
    m_output_tree -> Branch("ph_calo_z_err", & ph_calo_z_err);

    m_output_tree -> Branch("wt_xs", & wt_xs);
    m_output_tree -> Branch("wt_kf", & wt_kf);
    m_output_tree -> Branch("wt_ge", & wt_ge);
    m_output_tree -> Branch("wt_mc", & wt_mc);
    m_output_tree -> Branch("wt_nEvents", & wt_nEvents);
    m_output_tree -> Branch("wt_wt", & wt_wt);
    m_output_tree -> Branch("wt_vtx", & wt_vtx);
    m_output_tree -> Branch("wt_pu", & wt_pu);
    m_output_tree -> Branch("wt_SF", & wt_SF);
    m_output_tree -> Branch("wt_trigSF", & wt_trigSF);
    m_output_tree -> Branch("intlumi", & intlumi);

    m_output_tree -> Branch("ph_topoetcone20", & ph_topoetcone20);
    m_output_tree -> Branch("ph_topoetcone40", & ph_topoetcone40);
    m_output_tree -> Branch("ph_ptcone20", & ph_ptcone20);

    // Variables needed for Timing calibraiton              

    m_output_tree -> Branch("ph_f1", & ph_f1);
    m_output_tree -> Branch("ph_f3", & ph_f3);
    m_output_tree -> Branch("ph_etas2", & ph_etas2);
    m_output_tree -> Branch("ph_phis2", & ph_phis2);
    m_output_tree -> Branch("ph_etas1", & ph_etas1);
    m_output_tree -> Branch("ph_phis1", & ph_phis1);
    m_output_tree -> Branch("el_f1", & el_f1);
    m_output_tree -> Branch("el_f3", & el_f3);
    m_output_tree -> Branch("el_etas2", & el_etas2);
    m_output_tree -> Branch("el_phis2", & el_phis2);
    m_output_tree -> Branch("el_etas1", & el_etas1);
    m_output_tree -> Branch("el_phis1", & el_phis1);

    m_output_tree -> Branch("ph_maxEcell_onlId", & ph_maxEcell_onlId);
    m_output_tree -> Branch("ph_maxEcell_gain", & ph_maxEcell_gain);
    m_output_tree -> Branch("ph_maxEcell_E", & ph_maxEcell_energy);
    m_output_tree -> Branch("ph_maxEcell_x", & ph_maxEcell_x);
    m_output_tree -> Branch("ph_maxEcell_y", & ph_maxEcell_y);
    m_output_tree -> Branch("ph_maxEcell_z", & ph_maxEcell_z);
    m_output_tree -> Branch("ph_maxEcell_time", & ph_maxEcell_time);

    //Shower shape variables
    m_output_tree -> Branch("ph_Reta", & ph_Reta);
    m_output_tree -> Branch("ph_Rphi", & ph_Rphi);
    m_output_tree -> Branch("ph_dEs", & ph_dEs);
    m_output_tree -> Branch("ph_Rmax2", & ph_Rmax2);
    m_output_tree -> Branch("ph_wstot", & ph_wstot);
    m_output_tree -> Branch("ph_ws3", & ph_ws3);
    m_output_tree -> Branch("ph_Fside", & ph_Fside);
    m_output_tree -> Branch("ph_Eratio", & ph_Eratio);
    m_output_tree -> Branch("ph_Rhad", & ph_Rhad);

    m_output_tree -> Branch("m_met", & m_met);
    m_output_tree -> Branch("m_mpx", & m_mpx);
    m_output_tree -> Branch("m_mpy", & m_mpy);
    m_output_tree -> Branch("m_weight", & m_weight);
    m_output_tree -> Branch("m_sumet", & m_sumet);
    m_output_tree -> Branch("n_mu", & n_mu);
    m_output_tree -> Branch("mu_isTight", & mu_isTight);
    m_output_tree -> Branch("mu_isMedium", & mu_isMedium);

    m_output_tree -> Branch("mu_SF", & mu_SF);
    m_output_tree -> Branch("mu_trigSF", & mu_trigSF);
    m_output_tree -> Branch("mu_pt", & mu_pt);
    m_output_tree -> Branch("mu_eta", & mu_eta);
    m_output_tree -> Branch("mu_phi", & mu_phi);
    m_output_tree -> Branch("mu_E", & mu_E);
    m_output_tree -> Branch("n_jet", & n_jet);
    m_output_tree -> Branch("jet_pt", & jet_pt);
    m_output_tree -> Branch("jet_eta", & jet_eta);
    m_output_tree -> Branch("jet_phi", & jet_phi);
    m_output_tree -> Branch("jet_E", & jet_E);
    m_output_tree -> Branch("m_jet_btag60", & m_jet_btag60);
    m_output_tree -> Branch("m_jet_btag70", & m_jet_btag70);
    m_output_tree -> Branch("m_jet_btag77", & m_jet_btag77);
    m_output_tree -> Branch("m_jet_btag85", & m_jet_btag85);

    m_output_tree -> Branch("n_el", & n_el);
    m_output_tree -> Branch("el_isTight", & el_isTight);
    m_output_tree -> Branch("el_isMedium", & el_isMedium);
    m_output_tree -> Branch("el_isLoose", & el_isLoose);
    m_output_tree -> Branch("el_istrigmatch", & el_istrigmatch);
    m_output_tree -> Branch("el_istrigmatch_HLT_e24_lhmedium_L1EM20VH", & el_istrigmatch_HLT_e24_lhmedium_L1EM20VH);
    m_output_tree -> Branch("el_istrigmatch_HLT_e60_lhmedium", & el_istrigmatch_HLT_e60_lhmedium);
    m_output_tree -> Branch("el_istrigmatch_HLT_e120_lhloose", & el_istrigmatch_HLT_e120_lhloose);
    m_output_tree -> Branch("el_istrigmatch_HLT_e26_lhtight_nod0_ivarloose", & el_istrigmatch_HLT_e26_lhtight_nod0_ivarloose);
    m_output_tree -> Branch("el_istrigmatch_HLT_e60_lhmedium_nod0", & el_istrigmatch_HLT_e60_lhmedium_nod0);
    m_output_tree -> Branch("el_istrigmatch_HLT_e140_lhloose_nod0", & el_istrigmatch_HLT_e140_lhloose_nod0);
    m_output_tree -> Branch("mu_istrigmatch", & mu_istrigmatch);
    m_output_tree -> Branch("mu_istrigmatch_HLT_mu20_iloose_L1MU15", & mu_istrigmatch_HLT_mu20_iloose_L1MU15);
    m_output_tree -> Branch("mu_istrigmatch_HLT_mu50", & mu_istrigmatch_HLT_mu50);
    m_output_tree -> Branch("mu_istrigmatch_HLT_mu26_ivarmedium", & mu_istrigmatch_HLT_mu26_ivarmedium);

    m_output_tree -> Branch("el_SF", & el_SF);
    m_output_tree -> Branch("el_trigSF", & el_trigSF);
    m_output_tree -> Branch("el_pt", & el_pt);
    m_output_tree -> Branch("el_eta", & el_eta);
    m_output_tree -> Branch("el_phi", & el_phi);
    m_output_tree -> Branch("el_E", & el_E);
    m_output_tree -> Branch("el_t", & el_t);
    m_output_tree -> Branch("el_calo_z", & el_calo_z);
    m_output_tree -> Branch("el_calo_z_err", & el_calo_z_err);
    m_output_tree -> Branch("el_Reta", & el_Reta);
    m_output_tree -> Branch("el_Rphi", & el_Rphi);
    m_output_tree -> Branch("el_dEs", & el_dEs);
    m_output_tree -> Branch("el_Rmax2", & el_Rmax2);
    m_output_tree -> Branch("el_wstot", & el_wstot);
    m_output_tree -> Branch("el_ws3", & el_ws3);
    m_output_tree -> Branch("el_Fside", & el_Fside);
    m_output_tree -> Branch("el_Eratio", & el_Eratio);
    m_output_tree -> Branch("el_Rhad", & el_Rhad);
    m_output_tree -> Branch("el_maxEcell_onlId", & el_maxEcell_onlId);
    m_output_tree -> Branch("el_maxEcell_gain", & el_maxEcell_gain);
    m_output_tree -> Branch("el_maxEcell_E", & el_maxEcell_energy);
    m_output_tree -> Branch("el_maxEcell_x", & el_maxEcell_x);
    m_output_tree -> Branch("el_maxEcell_y", & el_maxEcell_y);
    m_output_tree -> Branch("el_maxEcell_z", & el_maxEcell_z);
    m_output_tree -> Branch("el_maxEcell_time", & el_maxEcell_time);

    m_output_tree -> Branch("spl0", & spl0);
    m_output_tree -> Branch("spl1", & spl1);
    m_output_tree -> Branch("spl2", & spl2);
    m_output_tree -> Branch("spl3", & spl3);
    m_output_tree -> Branch("spl4", & spl4);
    m_output_tree -> Branch("el_z0", & el_z0);
    m_output_tree -> Branch("el_vz", & el_vz);

    //cout<<"--- Trigger map ---"<<endl;
    for (std::map < TString, int > ::iterator itr = triggerMap.begin(); itr != triggerMap.end(); itr++)
        m_output_tree -> Branch(itr -> first, & itr -> second);

    wk() -> addOutput(m_output_tree);
}

EL::StatusCode NPAnalysis::createOutput() {

    // Here you setup the histograms needed for you analysis. This method
    // gets called after the Handlers are initialized, so that the systematic
    // registry is already filled.
    //cout<<"--- Creating MxAOD ---"<<endl;
    //TFile *file = wk()->getOutputFile("MxAOD");
    //cout<<"--- Create output ---"<<endl;
    //  cout<<"--- Trigger done and added to output tree ---"<<endl;
    if (PPT -> initialize().isFailure()) HG::fatal("Couldn't initialize PPT tool");
    //  cout<<"--- Accessed PPT tool ---"<<endl;

    setcutflowname();
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode NPAnalysis::histInitialize() {
    // Here you do everything that needs to be done at the very
    // beginning on each worker node, e.g. create histograms and output
    // trees.  This method gets called before any input files are
    // connected.

    HgammaAnalysis::histInitialize();

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode NPAnalysis::fileExecute() {
    // Here you do everything that needs to be done exactly once for every
    // single file, e.g. collect a list of all lumi-blocks processed

    // We use this function to extract initial and final sum of weights
    // and initial and final events from the metadata (for MC and data derivations)
    // For data AOD, we just sum the number of events in the file
    // For MC AOD, the sum of weights is computed in FillMCInfo

    // this function is called before initialize, unfortunately

    // file name
    Info("fileExecute", "Extracting bookkeeping information from file \"%s\"", wk() -> inputFile() -> GetName());
    Info("fileExecute", "This file has %lli entries", wk() -> xaodEvent() -> getEntries());

    HgammaAnalysis::fileExecute();

    m_newFile = true;

    // read CutBookKeeper information from MetaData
    // recipe from https://twiki.cern.ch/twiki/bin/view/AtlasProtected/AnalysisMetadata#Examples_MC15
    // get the MetaData tree once a new file is opened, with
    TTree * MetaData = dynamic_cast < TTree * > (wk() -> inputFile() -> Get("MetaData"));
    if (!MetaData) {
        Error("fileExecute()", "MetaData not found! Exiting.");
        return EL::StatusCode::FAILURE;
    }

    MetaData -> LoadTree(0);
    nEventsProcessed = 0;
    sumOfWeights = 0;
    sumOfWeightsSquared = 0;
    nEventsDxAOD = 0;
    sumOfWeightsDxAOD = 0;
    sumOfWeightsSquaredDxAOD = 0;

    bool m_bookkeeper = false;
    if (config() -> getBool("EventHandler.bookkeep", false)) m_bookkeeper = true;

    nEventsProcessed = 0;
    sumOfWeights = 0;
    sumOfWeightsSquared = 0;
    nEventsDxAOD = 0;
    sumOfWeightsDxAOD = 0;
    sumOfWeightsSquaredDxAOD = 0;

    if (m_bookkeeper) {
        // check for corruption
        const xAOD::CutBookkeeperContainer * incompleteCBC = nullptr;
        if (!wk() -> xaodEvent() -> retrieveMetaInput(incompleteCBC, "IncompleteCutBookkeepers").isSuccess()) {
            Error("fileExecute()", "Failed to retrieve IncompleteCutBookkeepers from MetaData! Exiting.");
            return EL::StatusCode::FAILURE;
        }
        if (incompleteCBC -> size() != 0) {
            //Error("fileExecute()","Found incomplete Bookkeepers! Check file for corruption.");
            //return EL::StatusCode::FAILURE;
        }

        // Now, let's find the actual information
        const xAOD::CutBookkeeperContainer * completeCBC = 0;
        if (!wk() -> xaodEvent() -> retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess()) {
            Error("fileExecute()", "Failed to retrieve CutBookkeepers from MetaData! Exiting.");
            return EL::StatusCode::FAILURE;
        }

        // First, let's find the smallest cycle number,
        // i.e., the original first processing step/cycle
        int minCycle = 10000;
        for (auto cbk: * completeCBC) {
            if (!cbk -> name().empty() && minCycle > cbk -> cycle()) {
                minCycle = cbk -> cycle();
            }
        }
        // Now, let's actually find the right one that contains all the needed info...
        const xAOD::CutBookkeeper * allEventsCBK = 0;
        const xAOD::CutBookkeeper * DxAODEventsCBK = 0;
        std::string derivationName = "STDM4Kernel";
        int maxCycle = -1;
        for (const auto & cbk: * completeCBC) {
            if (cbk -> cycle() > maxCycle && cbk -> name() == "AllExecutedEvents" && cbk -> inputStream() == "StreamAOD") {
                allEventsCBK = cbk;
                maxCycle = cbk -> cycle();
            }
            if (cbk -> name() == derivationName) {
                DxAODEventsCBK = cbk;
            }
        }
        if (!allEventsCBK) {
            Error("fileExecute()", "Failed to find AllExecutedEvents CutBookkeeper in MetaData! Exiting.");
            return EL::StatusCode::FAILURE;
        }
        if (!DxAODEventsCBK) {
            Error("fileExecute()", Form("Failed to find %s CutBookkeeper in MetaData! Exiting.", derivationName.c_str()));
            return EL::StatusCode::FAILURE;
        }

        nEventsProcessed = allEventsCBK -> nAcceptedEvents();
        sumOfWeights = allEventsCBK -> sumOfEventWeights();
        sumOfWeightsSquared = allEventsCBK -> sumOfEventWeightsSquared();

        nEventsDxAOD = DxAODEventsCBK -> nAcceptedEvents();
        sumOfWeightsDxAOD = DxAODEventsCBK -> sumOfEventWeights();
        sumOfWeightsSquaredDxAOD = DxAODEventsCBK -> sumOfEventWeightsSquared();
        cout << nEventsProcessed << " " << sumOfWeights << " " << sumOfWeightsSquared << endl;
        cout << nEventsDxAOD << " " << sumOfWeightsDxAOD << " " << sumOfWeightsSquaredDxAOD << endl;
    }

    return EL::StatusCode::SUCCESS;
}

EL::StatusCode NPAnalysis::execute() {
    // Here you do everything that needs to be done on every single
    // events, e.g. read input variables, apply cuts, and fill
    // histograms and trees.  This is where most of your actual analysis
    // code will go.

    //Clear and initialize all variables
    //  cout<<"--- Cleared all vectors ---"<<endl;
    // Important to keep this, so that internal tools / event variables
    // are filled properly.
    HgammaAnalysis::execute();

    tru_ph_type.clear();
    tru_ph_origin.clear();
    tru_ph_pt.clear();
    tru_ph_eta.clear();
    tru_ph_phi.clear();
    tru_ph_E.clear();
    tru_mom_pt.clear();
    tru_mom_eta.clear();
    tru_mom_phi.clear();
    tru_mom_E.clear();
    tru_mom_pdgid.clear();
    tru_dau_pt.clear();
    tru_dau_eta.clear();
    tru_dau_phi.clear();
    tru_dau_E.clear();
    tru_dau_pdgid.clear();
    tru_lep_pt.clear();
    tru_lep_eta.clear();
    tru_lep_phi.clear();
    tru_lep_E.clear();
    tru_jets_pt.clear();
    tru_jets_eta.clear();
    tru_jets_phi.clear();
    tru_jets_E.clear();
    tru_lep_pdgid.clear();
    tru_phprod_vx.clear();
    tru_phprod_vy.clear();
    tru_phprod_vz.clear();
    tru_phprod_vt.clear();
    tru_momprod_vx.clear();
    tru_momprod_vy.clear();
    tru_momprod_vz.clear();
    tru_momprod_vt.clear();
    tru_ph_t.clear();
    tru_ph_z.clear();
    tru_ph_tof.clear();
    tru_mom_tof.clear();
    tru_promptph_t.clear();
    tru_n_ph = 0;
    tru_n_lep = 0;
    tru_n_jets = 0;
    tru_MET = -1.0;

    n_ph = 0;
    n_mu = 0;
    n_el = 0;
    n_jet = 0;
    m_njet = 0;
    m_sumet = -1.0;
    m_met = -1.0;
    m_mpx = -1.0;
    m_mpy = -1.0;
    mu_isTight.clear();
    mu_isMedium.clear();
    mu_SF.clear();
    mu_trigSF.clear();
    mu_pt.clear();
    mu_eta.clear();
    mu_phi.clear();
    mu_E.clear();
    jet_pt.clear();
    jet_eta.clear();
    jet_phi.clear();
    jet_E.clear();
    m_jet_btag60.clear();
    m_jet_btag70.clear();
    m_jet_btag77.clear();
    m_jet_btag85.clear();
    el_isTight.clear();
    el_isMedium.clear();
    el_isLoose.clear();
    el_istrigmatch.clear();
    mu_istrigmatch.clear();

    el_istrigmatch_HLT_e24_lhmedium_L1EM20VH.clear();
    el_istrigmatch_HLT_e60_lhmedium.clear();
    el_istrigmatch_HLT_e120_lhloose.clear();
    el_istrigmatch_HLT_e26_lhtight_nod0_ivarloose.clear();
    el_istrigmatch_HLT_e60_lhmedium_nod0.clear();
    el_istrigmatch_HLT_e140_lhloose_nod0.clear();
    mu_istrigmatch_HLT_mu20_iloose_L1MU15.clear();
    mu_istrigmatch_HLT_mu50.clear();
    mu_istrigmatch_HLT_mu26_ivarmedium.clear();

    el_SF.clear();
    el_trigSF.clear();
    el_pt.clear();
    el_eta.clear();
    el_phi.clear();
    el_E.clear();
    el_t.clear();
    el_calo_z.clear();
    el_calo_z_err.clear();
    el_Reta.clear();
    el_Rphi.clear();
    el_dEs.clear();
    el_Rmax2.clear();
    el_wstot.clear();
    el_ws3.clear();
    el_Fside.clear();
    el_Eratio.clear();
    el_Rhad.clear();
    el_maxEcell_energy.clear();
    el_maxEcell_time.clear();
    el_maxEcell_x.clear();
    el_maxEcell_y.clear();
    el_maxEcell_z.clear();
    el_maxEcell_gain.clear();
    el_maxEcell_onlId.clear();

    ph_SF.clear();
    ph_pt.clear();
    ph_eta.clear();
    ph_phi.clear();
    ph_E.clear();
    ph_cl_E.clear();
    ph_t.clear();
    ph_convType.clear();
    ph_isTight.clear();
    ph_isLoose.clear();
    ph_isMedium.clear();
    ph_isClean.clear();
    ph_passIso.clear();
    ph_conv_z.clear();
    ph_conv_z_err.clear();
    ph_calo_z.clear();
    ph_calo_z_err.clear();
    ph_topoetcone20.clear();
    ph_topoetcone40.clear();
    ph_ptcone20.clear();
    ph_f1.clear();
    ph_f3.clear();
    ph_etas2.clear();
    ph_phis2.clear();
    ph_etas1.clear();
    ph_phis1.clear();
    el_f1.clear();
    el_f3.clear();
    el_etas2.clear();
    el_phis2.clear();
    el_etas1.clear();
    el_phis1.clear();
    ph_maxEcell_energy.clear();
    ph_maxEcell_time.clear();
    ph_maxEcell_x.clear();
    ph_maxEcell_y.clear();
    ph_maxEcell_z.clear();
    ph_maxEcell_gain.clear();
    ph_maxEcell_onlId.clear();
    ph_Reta.clear();
    ph_Rphi.clear();
    ph_dEs.clear();
    ph_Rmax2.clear();
    ph_wstot.clear();
    ph_ws3.clear();
    ph_Fside.clear();
    ph_Eratio.clear();
    ph_Rhad.clear();

    spl0.clear();
    spl1.clear();
    spl2.clear();
    spl3.clear();
    spl4.clear();
    el_z0.clear();
    el_vz.clear();

    //if(eventNumber==16172) cout<<1<<endl;
    unsigned int rN =  eventInfo()->auxdata<unsigned int>("runNumber");
    ULong64_t eN =  eventInfo()->auxdata<ULong64_t>("eventNumber");
    float pvz = eventHandler() ->hardestVertexZ();
    PV_z = pvz;
    mu = eventInfo()->averageInteractionsPerCrossing();
    mu_actual = eventInfo()->actualInteractionsPerCrossing();

    runNumber = rN;
    eventNumber = eN;
    bool disable = true;
    // Shikai: commenting out
    // if ((HG::isMC() && not disable)) {
    //     //std::cout<<"MC sample "<<std::endl;
    //     mcWeight = eventHandler() -> mcWeight();
    //     vertexWeight = eventHandler() -> vertexWeight();
    //     pileupWeight = eventHandler() -> pileupWeight();
    //     wt_SF = 1.0;
    //     wt_trigSF = 1.0;

    //     if (config() -> isDefined(TString::Format("CrossSection.%d", eventInfo() -> mcChannelNumber())))
    //         xs = getCrossSection();
    //     if (config() -> isDefined(TString::Format("kFactor.%d", eventInfo() -> mcChannelNumber())))
    //         kf = getKFactor();
    //     if (config() -> isDefined(TString::Format("GeneratorEfficiency.%d", eventInfo() -> mcChannelNumber())))
    //         ge = getGeneratorEfficiency();
    //     if (config() -> isDefined(TString::Format("TotalNEvents.%i", eventInfo() -> mcChannelNumber())))
    //         nEvents = getNtotalEvents();
    // }

    // wt_xs = xs;
    // wt_kf = kf;
    // wt_ge = ge;
    // wt_mc = mcWeight;
    // wt_nEvents = nEvents;
    // wt_pu = pileupWeight;
    // wt_vtx = vertexWeight;

    //----- ALL ----
    m_cutFlow = ALLEVTS;
    //------ GRL -----
    bool m_checkGRL = true;
    if (!m_checkGRL || HG::isMC() || eventHandler() -> passGRL(eventInfo())) {
        m_cutFlow = GRL;
    }

    //if(eventNumber==16172) cout<<2<<endl;

    //Look at the reconstructed variables
    //  cout<<"---xAOD::PhotonContainer allPhotons---"<<endl;
    xAOD::PhotonContainer allPhotons = photonHandler() -> getCorrectedContainer();

    // Now apply the photon selection
    // HW - for photon performance study, we take out ID and isolation cut; for standard photon selection, we keep loose cut and isolation requirement
    // HW - see how this is done in applySelectionWithoutIDandIso 
    //  cout<<"xAOD::PhotonContainer photons ;  "<<endl; 
    xAOD::PhotonContainer photons;
    if (runPhotonnoIDIsoCleaning) {
        photons = photonHandler() -> applySelectionNoPID(allPhotons);
    }
    //  if( doUnbiasedPhotonPerformanceStudy){
    //photons =  applySelectionWithoutIDandIso(allPhotons);}
    else photons = photonHandler() -> applySelection(allPhotons);

    //Trigger selection
    //cout<<"---for (std::map<TString, int>::iterator---"<<endl;
    for (std::map < TString, int > ::iterator itr = triggerMap.begin(); itr != triggerMap.end(); itr++)
        itr -> second = 0.;

    // See README.md file for comments on saving trigger decisions                                                                                        
    if (eventHandler() -> passDQ()) {
        HG::StrV passedtrigs = eventHandler() -> getPassedTriggers(); // check if any triggers are specified in getPassedTriggers
        // turns out getPassedTriggers only check "m_requiredTriggers"
        for (auto trig: passedtrigs) {
            //      std:://cout << trig << std::endl;
            triggerMap[trig] = 1.;
        }
        m_cutFlow = DATA_QUALITY;
    } else {
        return EL::StatusCode::SUCCESS;
    }

    //cout<<"--- setSelectedObjects(&photons); ---"<<endl;
    //if(eventNumber==16172) cout<<3<<endl;

    //Check weights
    setSelectedObjects( & photons);
    //  wt_wt = (HG::isData()) ? 1.0 :weight();
    //  m_weight = (HG::isData()) ? 1.0 : wt_wt * norm;
    // wt_wt=0.0;
    // m_weight=0.0;

    //Other objects
    //cout<<"---xAOD::JetContainer alljets---"<<endl;

    //h025: add taus, needed for overlap removal and MET. Followed HGamCore/HGamTools/Root/HGamExample.cxx
    xAOD::TauJetContainer taus = tauHandler() -> getCorrectedContainer();
    xAOD::TauJetContainer seltaus = tauHandler() -> applySelection(taus);

    xAOD::JetContainer alljets;
    xAOD::JetContainer jets;
    //h025: add PFFlow jets. Followed HGamCore/HGamTools/Root/HGamExample.cxx
    xAOD::JetContainer jets_hv;
    if (runJets) {
        alljets = jetHandler() -> getCorrectedContainer();
        jets = jetHandler() -> applySelection(alljets);
        jets_hv = jetHandlerPFlow() -> getCorrectedContainer();
    }
    //  cout<<"---xAOD::ElectronContainer--- "<<endl;
    xAOD::ElectronContainer all_elecs;
    xAOD::ElectronContainer electrons;
    if (runElectrons) {
        all_elecs = electronHandler() -> getCorrectedContainer();
        //cout<<"---xAOD::ElectronContainer electrons--- "<<endl;
        electrons = electronHandler() -> applySelection(all_elecs);
        //cout<<"---Accessed electron containers--- "<<endl;
    }

    //if(eventNumber==16172) cout<<4<<endl;

    xAOD::MuonContainer muons_all;
    xAOD::MuonContainer dirtyMuons;
    xAOD::MuonContainer muons;
    if (runMuons) {
        muons_all = muonHandler() -> getCorrectedContainer();
        //cout<<"---xAOD::MuonContainer muons_all---"<<endl;
        // the next few lines come from  HGamCutflowAndMxAOD.cxx
        dirtyMuons = muonHandler() -> applySelection(muons_all);
        if (runJets && runElectrons) {
            //h025: Changed the overlapremoval code to include taus
            overlapHandler() -> removeOverlap(photons, jets, electrons, dirtyMuons, seltaus);
        }
        muons = muonHandler() -> applyCleaningSelection(dirtyMuons);
        muonHandler() -> decorateDeltaRJet(muons, jets);
    }

    // cout<<"//MET container"<<endl;
    //MET container
    xAOD::MissingETContainer all_met;
    xAOD::MissingETContainer met_corrected;
    if (runMet) {
        //h025: Had to add PFflow jets and taus here. Followed HGamCore/HGamTools/Root/HGamExample.cxx
        all_met = etmissHandler() -> getCorrectedContainer( & photons, & alljets, & jets_hv, & electrons, & muons, & seltaus);
        met_corrected = etmissHandler() -> applySelection(all_met);
    }

    //if(eventNumber==16172) cout<<5<<endl;

    // scale factor
    static SG::AuxElement::Accessor < float > scaleFactor("scaleFactor");

    //cout<<"Accessed all containers"<<endl;
    //Fill jet 4 vectors
    if (runJets) {
        n_jet = (int) jets.size();
        for (int nj = 0; nj < (int) jets.size(); nj++) {
            TLorentzVector jet = jets[nj] -> p4();
            jet *= HG::invGeV;
            jet_pt.push_back(jet.Pt());
            jet_eta.push_back(jet.Eta());
            jet_phi.push_back(jet.Phi());
            jet_E.push_back(jet.E());
            m_jet_btag60.push_back((bool)(jets[nj] -> auxdata < char > ("MV2c10_FixedCutBEff_60")));
            m_jet_btag70.push_back((bool)(jets[nj] -> auxdata < char > ("MV2c10_FixedCutBEff_70")));
            m_jet_btag77.push_back((bool)(jets[nj] -> auxdata < char > ("MV2c10_FixedCutBEff_77")));
            m_jet_btag85.push_back((bool)(jets[nj] -> auxdata < char > ("MV2c10_FixedCutBEff_85")));
        }
    }

    //if(eventNumber==16172) cout<<6<<endl;

    if (runJets & runMet) {
        m_met = met_corrected["TST"] -> met() / 1000.0;
        m_sumet = (met_corrected)["TST"] -> sumet() * HG::invGeV;
        m_mpx = (met_corrected)["TST"] -> mpx() * HG::invGeV;
        m_mpy = (met_corrected)["TST"] -> mpy() * HG::invGeV;
    }
    //Fill muon vector

    if (runMuons) {
        for (int nmu = 0; nmu < (int) muons.size(); nmu++) {

            double _effMuonTrigSF = 1.0;
            xAOD::MuonContainer Muons(SG::VIEW_ELEMENTS);
            Muons.push_back(muons[nmu]);
            eventHandler() -> getMuonTriggerScaleFactor(Muons, _effMuonTrigSF);
            //wt_trigSF *= _effMuonTrigSF;
            mu_trigSF.push_back(_effMuonTrigSF);

            mu_SF.push_back(scaleFactor( * muons[nmu]));
            wt_SF *= scaleFactor( * muons[nmu]);

            TLorentzVector trumu = muons[nmu] -> p4();
            trumu *= HG::invGeV;
            mu_isTight.push_back((bool) muons[nmu] -> auxdataConst < char > ("isTight"));
            mu_isMedium.push_back((bool) muons[nmu] -> auxdataConst < char > ("isMedium"));

            //Muon trigger matching
            int mutrigmatched = 0, mutrigmatched1 = 0, mutrigmatched2 = 0, mutrigmatched3 = 0;
            if ((bool) eventHandler() -> passTriggerMatch_SingleMuon("HLT_mu20_iloose_L1MU15", * muons[nmu])) {
                mutrigmatched++;
                mutrigmatched1++;
            }
            if ((bool) eventHandler() -> passTriggerMatch_SingleMuon("HLT_mu50", * muons[nmu])) {
                mutrigmatched++;
                mutrigmatched2++;
            }
            if ((bool) eventHandler() -> passTriggerMatch_SingleMuon("HLT_mu26_ivarmedium", * muons[nmu])) {
                mutrigmatched++;
                mutrigmatched3++;
            }

            /*//For all triggers matching use this block of code
              for (auto trig: eventHandler()->getPassedTriggers()) {
              if(eventHandler()->passTrigger("HLT_mu20_iloose_L1MU15") || eventHandler()->passTrigger("HLT_mu50") || eventHandler()->passTrigger("HLT_mu26_ivarmedium")){
              if((bool)eventHandler()->passTriggerMatch_SingleMuon(trig,*muons[nmu])) mutrigmatched++;}}*/

            if (mutrigmatched == 0) mu_istrigmatch.push_back(0);
            else mu_istrigmatch.push_back(1);

            mu_istrigmatch_HLT_mu20_iloose_L1MU15.push_back(mutrigmatched1);
            mu_istrigmatch_HLT_mu50.push_back(mutrigmatched2);
            mu_istrigmatch_HLT_mu26_ivarmedium.push_back(mutrigmatched3);

            mu_pt.push_back(trumu.Pt());
            mu_eta.push_back(trumu.Eta());
            mu_phi.push_back(trumu.Phi());
            mu_E.push_back(trumu.E());
        }
    }
    n_mu = (int) muons.size();

    //if(eventNumber==16172) cout<<7<<endl;

    //Reco pointing calc variables
    std::pair < float, float > convPointing;
    std::pair < float, float > caloPointing;

    // This part below should not go to photon loop. These are event level information
    double t_coll = -999;
    bool corr = false;
    /*
      unsigned int t_bcid = eventInfo()->auxdata<unsigned int>("bcid");
      //  int t_dis_from_front=0;
      int bunch_pos=0;
      int gapbtrain = 0;
      if(HG::isData()){
      gapbtrain=m_bcTool->gapBeforeTrain( t_bcid );
      if(gapbtrain>500)
      bunch_pos = m_bcTool->distanceFromFront( t_bcid );
      else
      bunch_pos = m_bcTool->distanceFromFront( t_bcid ) * 2 + m_bcTool->distanceFromTail( t_bcid ) + m_bcTool->gapBeforeTrain( t_bcid );
      }  
    */
    int photon_present = 0;
    //The correlations between different e/gamma is correctly taken into account in the below funtion
    for (int i = 0; i < (int) photons.size(); i++) {
        if (photons[i] -> pt() * HG::invGeV < 10) continue;
        photon_present = 1;

        ph_SF.push_back(scaleFactor( * photons[i]));
        wt_SF *= scaleFactor( * photons[i]);
        ph_pt.push_back(photons[i] -> pt() * HG::invGeV);
        ph_eta.push_back(photons[i] -> eta());
        ph_phi.push_back(photons[i] -> phi());
        ph_E.push_back(photons[i] -> e() * HG::invGeV);
        ph_cl_E.push_back(photons[i] -> auxdata < float > ("cl_E") * HG::invGeV);
        double time = -999;
        ph_t.push_back(time);
        ph_convType.push_back(photons[i] -> auxdataConst < int > ("conversionType"));
        ph_isTight.push_back((bool) photons[i] -> auxdataConst < char > ("isTight"));
        ph_isLoose.push_back(photonHandler() -> passPIDCut(photons[i], egammaPID::PhotonIDLoose));
        ph_passIso.push_back(photonHandler() -> passIsoCut(photons[i]));
        ph_isClean.push_back(photonHandler() -> passCleaningCut(photons[i]));

        ph_topoetcone40.push_back(photons[i] -> auxdataConst < float > ("topoetcone40"));
        ph_topoetcone20.push_back(photons[i] -> auxdataConst < float > ("topoetcone20"));
        ph_ptcone20.push_back(photons[i] -> auxdataConst < float > ("ptcone20"));

        ph_maxEcell_gain.push_back(photons[i] -> auxdata < int > ("maxEcell_gain"));
        ph_maxEcell_onlId.push_back(photons[i] -> auxdata < unsigned long > ("maxEcell_onlId"));
        ph_maxEcell_energy.push_back(photons[i] -> auxdata < float > ("maxEcell_energy"));
        ph_maxEcell_time.push_back(photons[i] -> auxdata < float > ("maxEcell_time"));
        ph_maxEcell_x.push_back(photons[i] -> auxdata < float > ("maxEcell_x"));
        ph_maxEcell_y.push_back(photons[i] -> auxdata < float > ("maxEcell_y"));
        ph_maxEcell_z.push_back(photons[i] -> auxdata < float > ("maxEcell_z"));
        ph_f1.push_back(photons[i] -> auxdata < float > ("f1"));
        ph_f3.push_back(photons[i] -> auxdata < float > ("f3"));
        ph_etas2.push_back(photons[i] -> caloCluster() -> etaBE(2));
        ph_phis2.push_back(photons[i] -> caloCluster() -> phiBE(2));
        ph_etas1.push_back(photons[i] -> caloCluster() -> etaBE(1));
        ph_phis1.push_back(photons[i] -> caloCluster() -> phiBE(1));

        // Shikai: (eta, phi, E) per sampler
        spl0.push_back(photons[i] -> caloCluster() -> etaBE(0));
        spl0.push_back(photons[i] -> caloCluster() -> phiBE(0));
        spl0.push_back(photons[i] -> caloCluster() -> energyBE(0));

        spl1.push_back(photons[i] -> caloCluster() -> etaBE(1));
        spl1.push_back(photons[i] -> caloCluster() -> phiBE(1));
        spl1.push_back(photons[i] -> caloCluster() -> energyBE(1));

        spl2.push_back(photons[i] -> caloCluster() -> etaBE(2));
        spl2.push_back(photons[i] -> caloCluster() -> phiBE(2));
        spl2.push_back(photons[i] -> caloCluster() -> energyBE(2));

        spl3.push_back(photons[i] -> caloCluster() -> etaBE(3));
        spl3.push_back(photons[i] -> caloCluster() -> phiBE(3));
        spl3.push_back(photons[i] -> caloCluster() -> energyBE(3));

        // cout << "--------------Start PH--------------" << endl;
        try {spl0.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e011)); } catch (...) {spl0.push_back(-888);}
        try {spl0.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e033)); } catch (...) {spl0.push_back(-888);}
        // 1:
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e132)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e1152)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::ethad1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::ehad1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::f1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::f1core)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::weta1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::widths1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::poscs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::asy1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::barys1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::wtots1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::emins1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::emaxs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::fracs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e2tsts1)); } catch (...) {spl1.push_back(-888);}
        // 2:
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e233)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e233)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e255)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e237)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e277)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::weta2)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::widths2)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::poscs2)); } catch (...) {spl2.push_back(-888);}
        // 3:
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e333)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e335)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e337)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::e377)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::f3)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::f3core)); } catch (...) {spl3.push_back(-888);}
        // Global or not clear
        try {spl4.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::ethad)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::pos)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::pos7)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::r33over37allcalo)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(photons[i] -> showerShapeValue(xAOD::EgammaParameters::ecore)); } catch (...) {spl4.push_back(-888);}
        // cout << "--------------END--------------" << endl;
        //Photon shower shape variables in GeV
        ph_Reta.push_back(photons[i] -> auxdata < float > ("Reta"));
        ph_Rphi.push_back(photons[i] -> auxdata < float > ("Rphi"));
        ph_Eratio.push_back(photons[i] -> auxdata < float > ("Eratio"));
        ph_dEs.push_back(photons[i] -> auxdata < float > ("DeltaE") / 1000.0);
        ph_ws3.push_back(photons[i] -> auxdata < float > ("weta1"));
        ph_wstot.push_back(photons[i] -> auxdata < float > ("wtots1"));
        ph_Rmax2.push_back(photons[i] -> auxdata < float > ("e2tsts1") / (ph_E[i] * photons[i] -> auxdata < float > ("f1") * 1000.0));
        ph_Fside.push_back(photons[i] -> auxdata < float > ("r33over37allcalo"));
        ph_Rhad.push_back(photons[i] -> auxdata < float > ("Rhad"));

        //Pointing calculation
        if (xAOD::PVHelpers::passConvSelection(photons[i]))
            convPointing = PPT -> getConvPointing(photons[i]);
        caloPointing = PPT -> getCaloPointing(photons[i]);

        ph_conv_z.push_back(convPointing.first);
        ph_conv_z_err.push_back(convPointing.second);
        ph_calo_z.push_back(caloPointing.first);
        ph_calo_z_err.push_back(caloPointing.second);

    }

    n_ph = (int) photons.size();

    for (int i = 0; i < (int) electrons.size(); i++) {

        if (electrons[i] -> pt() * HG::invGeV < 10) continue;

        double trigSF = 1;
        eventHandler() -> getElectronTriggerScaleFactorOneLepton(electrons[i], trigSF);

        el_trigSF.push_back(trigSF);
        //wt_trigSF*= trigSF;

        el_SF.push_back(scaleFactor( * electrons[i]));
        wt_SF *= scaleFactor( * electrons[i]);

        el_pt.push_back(electrons[i] -> pt() * HG::invGeV);
        el_eta.push_back(electrons[i] -> eta());
        el_phi.push_back(electrons[i] -> phi());
        el_E.push_back(electrons[i] -> e() * HG::invGeV);

        //Remaining timing calculation
        //cout<<"--- runElectronTiming ---"<<endl;
        /*    double time = -999;
        if(runElectronTiming){
        cout<<"--- if((int)photons.size()>0) ---"<<endl;

        if((int)photons.size()>0){
        t_coll1 = ph_t[0];
        } else if(i>0){
        t_coll1 = el_t[0];
        }

        cout<<"--- bool t_valid; ---"<<endl;
        bool t_valid;
        double el_t_corr;
        if( HG::isMC() ){
        time = m_ott->getSmearedTime<const xAOD::Electron>(*electrons[i], pvz, t_coll1, false);}
        else{
        cout<<"--- tie(t_valid, el_t_corr )= ---"<<endl;
        tie(t_valid, el_t_corr )= m_ott->getCorrectedTime<const xAOD::Electron>(*electrons[i], rN, pvz, t_dis_from_front );
        cout<<"--- time = el_t_corr; ---"<<endl;
        time = el_t_corr;}
        }

        el_t.push_back(time);*/

        //Remaining timing calculation     
        double time = -999;
        //if(eventNumber==16172) cout<<9<<endl;

        // t_coll
        //    if( i > 0 ) t_coll = el_t[0] ; 
        // in events of multiple egamma object, the timing between objects are correlated.     
        bool t_valid = false;
        double el_t_corr = -99999;
        el_t.push_back(time);

        //Electron shower shape variables in GeV
        el_isTight.push_back((bool) electrons[i] -> auxdataConst < char > ("isTight"));
        el_isMedium.push_back((bool) electrons[i] -> auxdataConst < char > ("isMedium"));

        //if(eventHandler()->passDQ() )
        //HG::StrV passedtrigs = eventHandler()->getPassedTriggers();

        //Electron trigger matching
        int eltrigmatched = 0, eltrigmatched1 = 0, eltrigmatched2 = 0, eltrigmatched3 = 0, eltrigmatched4 = 0, eltrigmatched5 = 0, eltrigmatched6 = 0;
        /*    //For all trigger matching, use this code
        for (auto trig: eventHandler()->getPassedTriggers()) {
        if(eventHandler()->passTrigger("HLT_e24_lhmedium_L1EM20VH") || eventHandler()->passTrigger("HLT_e60_lhmedium") || eventHandler()->passTrigger("HLT_e120_lhloose") || eventHandler()->passTrigger("HLT_e26_lhtight_nod0_ivarloose") || eventHandler()->passTrigger("HLT_e60_lhmedium_nod0") || eventHandler()->passTrigger("HLT_e140_lhloose_nod0")){
        if((bool)eventHandler()->passTriggerMatch_SingleElectron(trig,*electrons[i])) eltrigmatched++;}}*/
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e24_lhmedium_L1EM20VH", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched1++;
        }
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e60_lhmedium", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched2++;
        }
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e120_lhloose", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched3++;
        }
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e26_lhtight_nod0_ivarloose", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched4++;
        }
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e60_lhmedium_nod0", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched5++;
        }
        if ((bool) eventHandler() -> passTriggerMatch_SingleElectron("HLT_e140_lhloose_nod0", * electrons[i])) {
            eltrigmatched++;
            eltrigmatched6++;
        }

        if (eltrigmatched == 0) el_istrigmatch.push_back(0);
        else el_istrigmatch.push_back(1);

        el_istrigmatch_HLT_e24_lhmedium_L1EM20VH.push_back(eltrigmatched1++);
        el_istrigmatch_HLT_e60_lhmedium.push_back(eltrigmatched2++);
        el_istrigmatch_HLT_e120_lhloose.push_back(eltrigmatched3++);
        el_istrigmatch_HLT_e26_lhtight_nod0_ivarloose.push_back(eltrigmatched4++);
        el_istrigmatch_HLT_e60_lhmedium_nod0.push_back(eltrigmatched5++);
        el_istrigmatch_HLT_e140_lhloose_nod0.push_back(eltrigmatched6++);

        el_f1.push_back(electrons[i] -> auxdata < float > ("f1"));
        el_f3.push_back(electrons[i] -> auxdata < float > ("f3"));
        el_etas2.push_back(electrons[i] -> caloCluster() -> etaBE(2));
        el_phis2.push_back(electrons[i] -> caloCluster() -> phiBE(2));
        el_etas1.push_back(electrons[i] -> caloCluster() -> etaBE(1));
        el_phis1.push_back(electrons[i] -> caloCluster() -> phiBE(1));

        el_Reta.push_back(electrons[i] -> auxdata < float > ("Reta"));
        el_Rphi.push_back(electrons[i] -> auxdata < float > ("Rphi"));
        el_Eratio.push_back(electrons[i] -> auxdata < float > ("Eratio"));
        el_dEs.push_back(electrons[i] -> auxdata < float > ("DeltaE") / 1000.0);
        el_ws3.push_back(electrons[i] -> auxdata < float > ("weta1"));
        el_wstot.push_back(electrons[i] -> auxdata < float > ("wtots1"));
        el_Rmax2.push_back(electrons[i] -> auxdata < float > ("e2tsts1") / (el_E[i] * electrons[i] -> auxdata < float > ("f1") * 1000.0));
        el_Fside.push_back(electrons[i] -> auxdata < float > ("r33over37allcalo"));
        el_Rhad.push_back(electrons[i] -> auxdata < float > ("Rhad"));

        // Shikai: (eta, phi, E) per sampler
        spl0.push_back(electrons[i] -> caloCluster() -> etaBE(0));
        spl0.push_back(electrons[i] -> caloCluster() -> phiBE(0));
        spl0.push_back(electrons[i] -> caloCluster() -> energyBE(0));

        spl1.push_back(electrons[i] -> caloCluster() -> etaBE(1));
        spl1.push_back(electrons[i] -> caloCluster() -> phiBE(1));
        spl1.push_back(electrons[i] -> caloCluster() -> energyBE(1));

        spl2.push_back(electrons[i] -> caloCluster() -> etaBE(2));
        spl2.push_back(electrons[i] -> caloCluster() -> phiBE(2));
        spl2.push_back(electrons[i] -> caloCluster() -> energyBE(2));

        spl3.push_back(electrons[i] -> caloCluster() -> etaBE(3));
        spl3.push_back(electrons[i] -> caloCluster() -> phiBE(3));
        spl3.push_back(electrons[i] -> caloCluster() -> energyBE(3));

        // cout << "--------------Start EL--------------" << endl;
        try {spl0.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e011)); } catch (...) {spl0.push_back(-888);}
        try {spl0.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e033)); } catch (...) {spl0.push_back(-888);}
        // 1:
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e132)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e1152)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::ethad1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::ehad1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::f1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::f1core)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::weta1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::widths1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::poscs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::asy1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::barys1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::wtots1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::emins1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::emaxs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::fracs1)); } catch (...) {spl1.push_back(-888);}
        try {spl1.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e2tsts1)); } catch (...) {spl1.push_back(-888);}
        // 2:
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e233)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e233)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e255)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e237)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e277)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::weta2)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::widths2)); } catch (...) {spl2.push_back(-888);}
        try {spl2.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::poscs2)); } catch (...) {spl2.push_back(-888);}
        // 3:
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e333)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e335)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e337)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::e377)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::f3)); } catch (...) {spl3.push_back(-888);}
        try {spl3.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::f3core)); } catch (...) {spl3.push_back(-888);}
        // Global or not clear
        try {spl4.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::ethad)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::pos)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::pos7)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::r33over37allcalo)); } catch (...) {spl4.push_back(-888);}
        try {spl4.push_back(electrons[i] -> showerShapeValue(xAOD::EgammaParameters::ecore)); } catch (...) {spl4.push_back(-888);}
        // cout << "--------------END--------------" << endl;

        //Pointing calculation
        caloPointing = PPT -> getCaloPointing(electrons[i]);
        el_calo_z.push_back(caloPointing.first);
        el_calo_z_err.push_back(caloPointing.second);

        //Shikai: get z0 and beamspot
        const xAOD::TrackParticle* tp  = electrons[i]->trackParticle();
        if ( !tp ) {
            el_z0.push_back(-999);
            el_vz.push_back(-999);
        } else {
            el_z0.push_back(tp->z0());
            el_vz.push_back(tp->vz());
        }
    }
    n_el = electrons.size();

    //cout<<"---Finished electron loop---"<<endl;
    //  cout<<"nchi20="<<nchi20<<"  nphdec="<<nphdec<<endl;

    // Shuo: compute scale factors (SF) and trigger SF at event level
    wt_wt = 1.0;
    m_weight = 1.0;
    //wt_trigSF = 1.0;

    if ((HG::isMC() && not disable)) {

        // lumi of mc16a/d/e
        if (HG::mcType() == "mc16a") intlumi = 36.2;
        else if (HG::mcType() == "mc16d") intlumi = 44.3;
        else intlumi = 58.5;
        // in fb-1

        if (electrons.size() == 2 || muons.size() >= 1) wt_trigSF = eventHandler() -> triggerScaleFactor( & electrons, & muons); // Shikai: maybe comment out?
        if (electrons.size() == 1) eventHandler() -> getElectronTriggerScaleFactorOneLepton(electrons[0], wt_trigSF);

        wt_wt = wt_mc * wt_pu * wt_vtx * wt_SF * wt_trigSF;
        wt_xs = wt_xs * wt_kf * wt_ge * 1000; // in fb
        m_weight = wt_wt;

        // note: finally, yield = m_weight * intlumi * wt_xs / (sum of m_weight wo any selection);
        // where sum of m_weight cannot be directly taken from the output Tree, but another cutflow histogram.

    }

    // for (int cut = ALLEVTS; cut <= m_cutFlow; ++cut) {
    //     fillCutFlow(CutEnum(cut), sysname, wt_wt);
    // }

    map_outputTree[""]->Fill();

    //store()->clear();
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode NPAnalysis::histFinalize() {
    for (std::map < TString, TH1F * > ::iterator it_map = m_cutflowhistoTH1F.begin(); it_map != m_cutflowhistoTH1F.end(); ++it_map) {
        (it_map -> second) -> SetFillStyle(3013);
        (it_map -> second) -> SetFillColor(13);
        ResetCounterName(m_event_cutflow_name, it_map -> second);
    }
    return EL::StatusCode::SUCCESS;
}

EL::StatusCode NPAnalysis::finalize() {
    HgammaAnalysis::finalize();
    delete PPT;
    delete m_ott;
    return EL::StatusCode::SUCCESS;
}

// HW - this funciton below is one I created based on the applySelection() in PhotonHandler

xAOD::PhotonContainer NPAnalysis::applySelectionWithoutIDandIso(xAOD::PhotonContainer & container) {
    xAOD::PhotonContainer selected(SG::VIEW_ELEMENTS);

    for (auto photon: container) {

        // require photon away from bad calorimeter region
        if (!photonHandler() -> passOQCut(photon)) {
            continue;
        }

        // require photon to pass cleaning cuts
        if (!photonHandler() -> passCleaningCut(photon)) {
            continue;
        }

        // apply pT and eta selection cuts
        if (!photonHandler() -> passPtEtaCuts(photon)) {
            continue;
        }

        // require ambiguity cut for electron fakes
        if (!photonHandler() -> passAmbCut(photon)) {
            continue;
        }

        // require photon away from bad calorimeter region
        if (!photonHandler() -> passHVCut(photon)) {
            continue;
        }
        /*
        // require ID
        if (!photonHandler()->passPIDCut(photon))
        { continue; }

        // require Isolation
        if (!photonHandler()->passIsoCut(photon))
        { continue; }
        */

        selected.push_back(photon);
    }

    return selected;
}

void NPAnalysis::openCutFlow() {
    // Shikai: comment out
    if (HG::isMC()) {
        hist_cutflow_name = Form("cutflow_%d", (int) m_mcChannelNumber);
    } else {
        hist_cutflow_name = Form("cutflow_%d", (int) eventInfo() -> runNumber());
    }

    if (m_cutflowhistoTH1F.count(hist_cutflow_name) == 0) {
        // the histogram doesn't exist - create it and add it to the output file!
        for (auto sys: getSystematics()) {
            //std::cout<<"sysname="<<sys.name()<<std::endl;

            TString sysname = sys.name();
            if (!HG::isMC() && sysname != "") continue;
            if (!m_applySystematicLoop && sysname != "") continue;
            //      if(m_applySystematicLoop && sysname!="" && !sysname.Contains("EFF")) continue; // this is to only take EG error as example
            if (sysname != "") sysname = "_" + sysname;

            m_cutflowhistoTH1F[Form("%s%s", hist_cutflow_name.Data(), sysname.Data())] = new TH1F(Form("%s%s", hist_cutflow_name.Data(), sysname.Data()), Form("%s%s", hist_cutflow_name.Data(), sysname.Data()), 30, 0, 30.);
            wk() -> addOutput(m_cutflowhistoTH1F[Form("%s%s", hist_cutflow_name.Data(), sysname.Data())]);
            m_cutflowhistoTH1F[Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data())] = new TH1F(Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data()), Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data()), 30, 0, 30.);
            wk() -> addOutput(m_cutflowhistoTH1F[Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data())]);
            m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())] = new TH1F(Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data()), Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data()), 30, 0, 30.);
            wk() -> addOutput(m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())]);
        }
    }
}

void NPAnalysis::fillCutFlow(CutEnum cut, TString sysname, double w) {
    if (sysname == "") {
        m_cutflowhistoTH1F[Form("%s", hist_cutflow_name.Data())] -> Fill(cut);
        m_cutflowhistoTH1F[Form("%s_w", hist_cutflow_name.Data())] -> Fill(cut, w);
        m_cutflowhistoTH1F[Form("%s_w2", hist_cutflow_name.Data())] -> Fill(cut, w * w);
    } else {
        m_cutflowhistoTH1F[Form("%s_%s", hist_cutflow_name.Data(), sysname.Data())] -> Fill(cut);
        m_cutflowhistoTH1F[Form("%s_%s_w", hist_cutflow_name.Data(), sysname.Data())] -> Fill(cut, w);
        m_cutflowhistoTH1F[Form("%s_%s_w2", hist_cutflow_name.Data(), sysname.Data())] -> Fill(cut, w * w);
    }
}

void NPAnalysis::addBookKeeping() {
    for (auto sys: getSystematics()) {

        TString sysname = sys.name();
        if (!HG::isMC() && sysname != "") continue;
        if (!m_applySystematicLoop && sysname != "") continue;
        //    if(m_applySystematicLoop && sysname!="" && !sysname.Contains("EFF")) continue; // this is to only take EG error as example
        if (sysname != "") sysname = "_" + sysname;

        cout << "adding book keeper" << endl;

        m_cutflowhistoTH1F[Form("%s%s", hist_cutflow_name.Data(), sysname.Data())] -> AddBinContent(1, nEventsProcessed);
        m_cutflowhistoTH1F[Form("%s%s", hist_cutflow_name.Data(), sysname.Data())] -> AddBinContent(2, nEventsDxAOD);

        m_cutflowhistoTH1F[Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data())] -> AddBinContent(1, sumOfWeights);
        m_cutflowhistoTH1F[Form("%s%s_w", hist_cutflow_name.Data(), sysname.Data())] -> AddBinContent(2, sumOfWeightsDxAOD);

        m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())] -> SetBinContent(1, sqrt(pow(m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())] -> GetBinContent(1), 2) + pow(sumOfWeightsSquared, 2)));
        m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())] -> SetBinContent(2, sqrt(pow(m_cutflowhistoTH1F[Form("%s%s_w2", hist_cutflow_name.Data(), sysname.Data())] -> GetBinContent(2), 2) + pow(sumOfWeightsSquaredDxAOD, 2)));
    }
}

void NPAnalysis::setcutflowname() {
    m_event_cutflow_name[xAOD] = "xAOD";
    m_event_cutflow_name[DxAOD] = "DxAOD";
    m_event_cutflow_name[ALLEVTS] = "ALLEVTS";
    m_event_cutflow_name[GRL] = "GRL";
    m_event_cutflow_name[DATA_QUALITY] = "DATA_QUALITY";
}
