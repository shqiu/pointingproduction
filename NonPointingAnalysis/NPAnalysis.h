#ifndef NonPointingAnalysis_NPAnalysis_H
#define NonPointingAnalysis_NPAnalysis_H
#include "OfflineTimingTool/OfflineTimingTool.h"
#include "HGamAnalysisFramework/HgammaAnalysis.h"
#include "HGamAnalysisFramework/HgammaIncludes.h"
#include <TTree.h>
#include <TLorentzVector.h>
#include <TH1F.h>
#include <TH2F.h>
#include <vector>
#include <string>
#include<iostream>
#include<fstream>
#include "xAODTruth/TruthParticle.h"

class NPAnalysis : public HgammaAnalysis
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
private:
  // Tree *myTree; //!
  // TH1 *myHist; //!
  enum CutEnum { xAOD=0, DxAOD=1,ALLEVTS=2,
    GRL=3, DATA_QUALITY=4};
  CutEnum m_cutFlow; //!

  std::map<TString,TH1F*> m_cutflowhistoTH1F;   //!
  std::map<int, TString> m_event_cutflow_name;  //!
  TString hist_cutflow_name; //!
  unsigned int nEventsProcessed ;  //!
  double sumOfWeights           ;  //!
  double sumOfWeightsSquared    ;  //!
  unsigned int nEventsDxAOD       ;  //!
  double sumOfWeightsDxAOD        ;  //!
  double sumOfWeightsSquaredDxAOD ;  //!
  bool m_newFile; //!
  map<TString, bool> m_btreemap; //!
  std::map<TString, TTree*> map_outputTree; //!

  TTree *m_output_tree; //!
  Trig::IBunchCrossingTool* m_bcTool; //!
  //Declare variables that will go into the TTree
  int runNumber; //!                                      
  int m_mcChannelNumber; //!
  ULong64_t eventNumber; //!                              
  double mu; //!
  double mu_actual; //!
  double PV_z; //!
  double wt_xs; //!
  double wt_kf ;//!                                       
  double wt_ge ;//!                                       
  double wt_mc ;//!                                       
  double wt_pu ;//!
  double wt_vtx;//!
  double wt_SF; //!
  double wt_trigSF; //!  
  double wt_nEvents ;//!                                  
  double intlumi ; //!                                    
  double wt_wt ; //!
  double m_weight ; //!

  bool runPhotons ;
  bool runPhotonTiming ;
  bool runElectronTiming ;
  bool runElectrons ;
  bool runJets ;
  bool runPhotonnoIDIsoCleaning;
  bool runMet ;
  bool runMuons ;
  bool doUnbiasedPhotonPerformanceStudy ;



  //Truth variables
  vector<double> tru_ph_pt; //!
  vector<double> tru_ph_eta; //!
  vector<double> tru_ph_phi; //!
  vector<double> tru_ph_E; //!
  vector<int> tru_ph_type; //!
  vector<int> tru_ph_origin; //!
  
  //Photon's mom
  vector<double> tru_mom_pt; //!
  vector<double> tru_mom_eta; //!
  vector<double> tru_mom_phi; //!
  vector<double> tru_mom_E; //!
  vector<double> tru_mom_pdgid; //!
  //Jet four vectors
  vector<double> tru_jets_pt; //!
  vector<double> tru_jets_eta; //!
  vector<double> tru_jets_phi; //!
  vector<double> tru_jets_E; //!
  //Mom's other daughter (if exists)
  vector<double> tru_dau_pt; //!
  vector<double> tru_dau_eta; //!
  vector<double> tru_dau_phi; //!
  vector<double> tru_dau_E; //!
  vector<double> tru_dau_pdgid; //!
  //Photon's production vertex
  vector<double> tru_phprod_vx; //!
  vector<double> tru_phprod_vy; //!
  vector<double> tru_phprod_vz; //!
  vector<double> tru_phprod_vt; //!
  //Photon's mom's production vertex
  vector<double> tru_momprod_vx; //!
  vector<double> tru_momprod_vy; //!
  vector<double> tru_momprod_vz; //!
  vector<double> tru_momprod_vt; //!
  //Photon's timing and pointing variables
  vector<double> tru_ph_t; //!
  vector<double> tru_ph_z; //!
  //Other timing variables of interest
  vector<double> tru_mom_tof; //!
  vector<double> tru_promptph_t; //!
  vector<double> tru_ph_tof; //!
  //Leptons
  vector<double> tru_lep_pt; //!
  vector<double> tru_lep_eta; //!
  vector<double> tru_lep_phi; //!
  vector<double> tru_lep_E; //!
  vector<double> tru_lep_pdgid; //!
  int tru_n_ph; //!
  int tru_n_lep; //!
  int tru_n_jets; //!
  double tru_MET; //!

  //Reco variables
  vector<double>ph_SF;  //!
  vector<double>ph_pt;  //!
  vector<double>ph_eta; //!
  vector<double>ph_phi; //!
  vector<double>ph_E; //!
  vector<double>ph_cl_E; //!
  int n_ph ; //!
  vector<double>ph_t ; //!
  vector<int> ph_convType ; //!
  vector<bool> ph_isTight ; //!
  vector<bool> ph_isLoose ; //!
  vector<bool> ph_isMedium ; //!
  vector<bool> ph_isClean ; //!
  vector<bool> ph_passIso ; //!
  vector<double>ph_conv_z ; //!                                 
  vector<double>ph_conv_z_err ; //!                             
  vector<double>ph_calo_z ; //!                                 
  vector<double>ph_calo_z_err ; //!
  vector<double> ph_topoetcone20 ;//!                             
  vector<double> ph_topoetcone40 ;//!                             
  vector<double> ph_ptcone20 ;//!                                 
  vector<double> ph_f1;//!                                        
  vector<double> ph_f3;//!                                        
  vector<double> ph_etas2;//!                                     
  vector<double> ph_phis2;//!                                     
  vector<double> ph_etas1;//!                                     
  vector<double> ph_phis1;//!                                     
  vector<double> ph_maxEcell_energy;//!                           
  vector<double> ph_maxEcell_time;//!                             
  vector<double> ph_maxEcell_x;//!                                
  vector<double> ph_maxEcell_y;//!                                
  vector<double> ph_maxEcell_z;//!                                
  vector<int> ph_maxEcell_gain;//!                               
  vector<ULong64_t> ph_maxEcell_onlId;//!
  vector<double>ph_Reta; //!
  vector<double>ph_Rphi; //!
  vector<double>ph_dEs; //!
  vector<double>ph_Rmax2; //!
  vector<double>ph_wstot; //!
  vector<double>ph_ws3; //!
  vector<double>ph_Fside; //!
  vector<double>ph_Eratio; //!
  vector<double>ph_Rhad; //!
 
  int m_njet; //!                                         
  double m_sumet ; //!                                    
  double m_met ; //!                                      
  double m_mpx ; //!                                      
  double m_mpy ; //!                                      

  int n_jet ; //!
  vector<double> jet_pt;  //!
  vector<double> jet_eta;  //!
  vector<double> jet_phi;  //!
  vector<double> jet_E;  //!
  vector<bool> m_jet_btag60; //!
  vector<bool> m_jet_btag70; //!
  vector<bool> m_jet_btag77; //!
  vector<bool> m_jet_btag85; //!


  //Save muons as vector
  int n_mu ; //!
  vector<bool> mu_isTight ; //!  
  vector<bool> mu_isMedium ; //!
  vector<double> mu_SF;  //!
  vector<double> mu_trigSF;  //!
  vector<double> mu_pt;  //!
  vector<double> mu_eta;  //!
  vector<double> mu_phi;  //!
  vector<double> mu_E;  //!

  int n_el ; //!  
  vector<bool> el_isTight ; //!
  vector<bool> el_isMedium ; //!
  vector<bool> el_isLoose ; //!
  vector<bool> el_istrigmatch ; //!
  vector<bool> mu_istrigmatch ; //!
  vector<bool> el_istrigmatch_HLT_e24_lhmedium_L1EM20VH ; //!
  vector<bool> el_istrigmatch_HLT_e60_lhmedium ; //!
  vector<bool> el_istrigmatch_HLT_e120_lhloose ; //!
  vector<bool> el_istrigmatch_HLT_e26_lhtight_nod0_ivarloose ; //!
  vector<bool> el_istrigmatch_HLT_e60_lhmedium_nod0 ; //!
  vector<bool> el_istrigmatch_HLT_e140_lhloose_nod0 ; //!
  vector<bool> mu_istrigmatch_HLT_mu20_iloose_L1MU15 ; //!
  vector<bool> mu_istrigmatch_HLT_mu50 ; //!
  vector<bool> mu_istrigmatch_HLT_mu26_ivarmedium ; //!
  vector<double> el_f1;//!                                        
  vector<double> el_f3;//!                                        
  vector<double> el_etas2;//!                                     
  vector<double> el_phis2;//!                                     
  vector<double> el_etas1;//!                                     
  vector<double> el_phis1;//!
  vector<double> el_SF;  //!
  vector<double> el_trigSF; //!
  vector<double> el_pt;  //!
  vector<double> el_eta;  //!
  vector<double> el_phi;  //!
  vector<double> el_E;  //!
  vector<double> el_t;  //!
  vector<double> el_calo_z;  //!
  vector<double> el_calo_z_err;  //!
  vector<double> el_Reta; //!
  vector<double> el_Rphi; //!
  vector<double> el_dEs; //!
  vector<double> el_Rmax2; //!
  vector<double> el_wstot; //!
  vector<double> el_ws3; //!
  vector<double> el_Fside; //!
  vector<double> el_Eratio; //!
  vector<double> el_Rhad; //!
  vector<double> el_maxEcell_energy;//!                           
  vector<double> el_maxEcell_time;//!                             
  vector<double> el_maxEcell_x;//!                                
  vector<double> el_maxEcell_y;//!                                
  vector<double> el_maxEcell_z;//!                                
  vector<int> el_maxEcell_gain;//!                               
  vector<ULong64_t> el_maxEcell_onlId;//!

  // Sampler vectors
  vector<double> spl0; //!
  vector<double> spl1; //!
  vector<double> spl2; //!
  vector<double> spl3; //!
  vector<double> spl4; //!
  vector<double> el_z0; //!
  vector<double> el_vz; //!
  
  int nchi20=0, nphdec=0;

  std::map<TString, int> triggerMap;
  std::map<TString, int> triggerPtLow;
  std::map<TString, int> triggerPtHigh;
  
public:
  // this is a standard constructor
  NPAnalysis() { }
  NPAnalysis(const char *name);
  virtual ~NPAnalysis();

  // these are the functions inherited from HgammaAnalysis
  virtual EL::StatusCode createOutput();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode fileExecute();
  virtual EL::StatusCode execute();
  virtual EL::StatusCode initialize();
  virtual EL::StatusCode histFinalize ();
  virtual EL::StatusCode finalize();
  
  void openCutFlow();
  void fillCutFlow(CutEnum cut, TString sysname, double w); 
  void addBookKeeping();
  void setcutflowname();
  CutEnum initialcutflow();
  void SetupOutputTree(TString sysname = "");
  template <typename T> void ResetCounterName(map<int, TString> &_cutname, T *_h)
   {
    int Ncut = _cutname.size();
    map<int,  TString >::iterator map_iter_i;
    _h->SetBins(Ncut,0,Ncut);
    int ibin=1;
    for(map_iter_i = _cutname.begin(); map_iter_i != _cutname.end(); map_iter_i++)
     {
      _h->GetXaxis()->SetBinLabel(ibin , (map_iter_i->second).Data());
      ibin++;
     }
    _h->SetMinimum(0);
   }

  bool m_applySystematicLoop;

  xAOD::PhotonContainer applySelectionWithoutIDandIso(xAOD::PhotonContainer &container);
  map<string,double> truthphztcalc(TLorentzVector, TLorentzVector, TLorentzVector, TLorentzVector); 

  // this is needed to distribute the algorithm to the workers
  ClassDef(NPAnalysis, 1);
  double m_trkPtCut;

};

#endif // NonPointingAnalysis_NPAnalysis_H

