#include "NonPointingAnalysis/NPAnalysis.h"
#include "HGamAnalysisFramework/RunUtils.h"

#include <xAODRootAccess/Init.h>
int main(int argc, char *argv[])
{
  // Set up the job for xAOD access
  xAOD::Init().ignore();

  // Create our algorithm
  NPAnalysis *alg = new NPAnalysis("NPAnalysis");

  // Use helper to start the job
  HG::runJob(alg, argc, argv);

  return 0;
}
