#!/bin/bash

for i in `cat data16_13TeV.DAOD_HIGG1D1.r9264_p3083_p3576.txt`;
  do
    run=`echo $i | cut -d"." -f 2`;
    datatype=`echo $i | cut -d"." -f 5`;

    runNPAnalysis HIGG1D1.cfg GridDS: ${i} OutputDS: user.hwang43.${run}.${datatype}_v1

    echo $run $datatype;

done
