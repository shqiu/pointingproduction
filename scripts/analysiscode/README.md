##Compile

make <executable name>

for example,

make processMumugamma ;

./bin/processMumugamma *root


processMumugamma: Used to run EGAM4 derivation samples, like Z->mumugamma type events
processEegamma: Used to run EGAM3 derivation samples, like Z->eegamma type events
processEe: Used to run EGAM1 derivation samples, like Z->ee type events
processemu: Used to run STDM4 derivation samples


##
