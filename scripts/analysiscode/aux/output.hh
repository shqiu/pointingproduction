//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jun  3 06:45:57 2019 by ROOT version 6.14/04
// from TTree output/output
// found on file: /projecta/projectdirs/atlas/santpur/NPP_framework/files/output_PID_apr12/STDM4/data/user.ssantpur.00300800.DAOD_STDM4_v11_hist/user.ssantpur.18246493._000001.hist-output.root
//////////////////////////////////////////////////////////

#ifndef output_hh
#define output_hh

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"
#include "vector"
#include "vector"
using std::vector;

class output {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           runNumber;
   ULong64_t       eventNumber;
   Int_t           m_mcChannelNumber;
   Double_t        PV_z;
   vector<double>  *tru_ph_pt;
   vector<double>  *tru_ph_eta;
   vector<double>  *tru_ph_phi;
   vector<double>  *tru_ph_E;
   vector<double>  *tru_mom_pt;
   vector<double>  *tru_mom_eta;
   vector<double>  *tru_mom_phi;
   vector<double>  *tru_mom_E;
   vector<double>  *tru_mom_pdgid;
   vector<double>  *tru_dau_pt;
   vector<double>  *tru_dau_eta;
   vector<double>  *tru_dau_phi;
   vector<double>  *tru_dau_E;
   vector<double>  *tru_lep_pt;
   vector<double>  *tru_lep_eta;
   vector<double>  *tru_lep_phi;
   vector<double>  *tru_lep_E;
   vector<double>  *tru_jets_pt;
   vector<double>  *tru_jets_eta;
   vector<double>  *tru_jets_phi;
   vector<double>  *tru_jets_E;
   vector<double>  *tru_lep_pdgid;
   vector<double>  *tru_dau_pdgid;
   vector<double>  *tru_phprod_vx;
   vector<double>  *tru_phprod_vy;
   vector<double>  *tru_phprod_vz;
   vector<double>  *tru_phprod_vt;
   vector<double>  *tru_momprod_vx;
   vector<double>  *tru_momprod_vy;
   vector<double>  *tru_momprod_vz;
   vector<double>  *tru_momprod_vt;
   Int_t           tru_n_ph;
   Int_t           tru_n_lep;
   Int_t           tru_n_jets;
   Double_t        tru_MET;
   vector<double>  *tru_ph_t;
   vector<double>  *tru_ph_z;
   vector<double>  *tru_mom_tof;
   vector<double>  *tru_ph_tof;
   vector<double>  *tru_promptph_t;
   vector<double>  *ph_pt;
   vector<double>  *ph_eta;
   vector<double>  *ph_phi;
   vector<double>  *ph_E;
   Int_t           n_ph;
   vector<double>  *ph_t;
   vector<int>     *ph_convType;
   vector<bool>    *ph_isTight;
   vector<bool>    *ph_isLoose;
   vector<bool>    *ph_passIso;
   vector<bool>    *ph_isClean;
   vector<double>  *ph_conv_z;
   vector<double>  *ph_conv_z_err;
   vector<double>  *ph_calo_z;
   vector<double>  *ph_calo_z_err;
   Double_t        wt_xs;
   Double_t        wt_kf;
   Double_t        wt_ge;
   Double_t        wt_mc;
   Double_t        wt_nEvents;
   Double_t        wt_wt;
   Double_t        intlumi;
   vector<double>  *ph_topoetcone20;
   vector<double>  *ph_topoetcone40;
   vector<double>  *ph_ptcone20;
   vector<double>  *ph_f1;
   vector<double>  *ph_f3;
   vector<double>  *ph_etas2;
   vector<double>  *ph_phis2;
   vector<ULong64_t> *ph_maxEcell_onlId;
   vector<int>     *ph_maxEcell_gain;
   vector<double>  *ph_maxEcell_E;
   vector<double>  *ph_maxEcell_x;
   vector<double>  *ph_maxEcell_y;
   vector<double>  *ph_maxEcell_z;
   vector<double>  *ph_maxEcell_time;
   vector<double>  *ph_Reta;
   vector<double>  *ph_Rphi;
   vector<double>  *ph_dEs;
   vector<double>  *ph_Rmax2;
   vector<double>  *ph_wstot;
   vector<double>  *ph_ws3;
   vector<double>  *ph_Fside;
   vector<double>  *ph_Eratio;
   vector<double>  *ph_Rhad;
   Double_t        m_met;
   Double_t        m_mpx;
   Double_t        m_mpy;
   Double_t        m_weight;
   Double_t        m_sumet;
   Int_t           n_mu;
   vector<double>  *mu_pt;
   vector<double>  *mu_eta;
   vector<double>  *mu_phi;
   vector<double>  *mu_E;
   Int_t           n_jet;
   vector<double>  *jet_pt;
   vector<double>  *jet_eta;
   vector<double>  *jet_phi;
   vector<double>  *jet_E;
   Int_t           n_el;
   vector<double>  *el_pt;
   vector<double>  *el_eta;
   vector<double>  *el_phi;
   vector<double>  *el_E;
   vector<double>  *el_t;
   vector<double>  *el_calo_z;
   vector<double>  *el_calo_z_err;
   vector<double>  *el_Reta;
   vector<double>  *el_Rphi;
   vector<double>  *el_dEs;
   vector<double>  *el_Rmax2;
   vector<double>  *el_wstot;
   vector<double>  *el_ws3;
   vector<double>  *el_Fside;
   vector<double>  *el_Eratio;
   vector<double>  *el_Rhad;
   vector<ULong64_t> *el_maxEcell_onlId;
   vector<int>     *el_maxEcell_gain;
   vector<double>  *el_maxEcell_E;
   vector<double>  *el_maxEcell_x;
   vector<double>  *el_maxEcell_y;
   vector<double>  *el_maxEcell_z;
   vector<double>  *el_maxEcell_time;
   Int_t           HLT_2e12_lhloose_nod0_mu10;
   Int_t           HLT_2e12_lhlvoose_nod0_L12EM10VH;
   Int_t           HLT_2e15_lhvloose_nod0_L12EM13VH;
   Int_t           HLT_2e17_lhvloose_nod0;
   Int_t           HLT_2g20_tight;
   Int_t           HLT_2g50_loose;
   Int_t           HLT_3mu6_msonly;
   Int_t           HLT_e120_lhloose;
   Int_t           HLT_e120_lhloose_nod0;
   Int_t           HLT_e120_loose;
   Int_t           HLT_e140_lhloose;
   Int_t           HLT_e140_lhloose_nod0;
   Int_t           HLT_e140_loose;
   Int_t           HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH;
   Int_t           HLT_e17_lhloose_nod0_2e9_lhloose_nod0;
   Int_t           HLT_e17_lhloose_nod0_mu14;
   Int_t           HLT_e24_lhmedium_iloose_EM18VH;
   Int_t           HLT_e24_lhmedium_iloose_EM20VH;
   Int_t           HLT_e24_lhmedium_nod0_ivarloose;
   Int_t           HLT_e24_lhtight_iloose;
   Int_t           HLT_e24_lhtight_iloose_EM20VH;
   Int_t           HLT_e24_lhtight_nod0_ivarloose;
   Int_t           HLT_e24_medium_iloose_EM18VH;
   Int_t           HLT_e24_medium_iloose_EM20VH;
   Int_t           HLT_e24_tight_iloose;
   Int_t           HLT_e24_tight_iloose_EM20VH;
   Int_t           HLT_e26_lhtight_iloose;
   Int_t           HLT_e26_lhtight_nod0_ivarloose;
   Int_t           HLT_e26_lhtight_smooth_ivarloose;
   Int_t           HLT_e26_tight_iloose;
   Int_t           HLT_e60_lhmedium;
   Int_t           HLT_e60_lhmedium_nod0;
   Int_t           HLT_e60_medium;
   Int_t           HLT_e70_lhloose_nod0_xe70noL1;
   Int_t           HLT_e7_lhmedium_nod0_mu24;
   Int_t           HLT_g100_loose;
   Int_t           HLT_g10_loose;
   Int_t           HLT_g120_loose;
   Int_t           HLT_g140_loose;
   Int_t           HLT_g15_loose_L1EM7;
   Int_t           HLT_g20_loose_L1EM12;
   Int_t           HLT_g25_loose_L1EM15;
   Int_t           HLT_g25_medium_mu24;
   Int_t           HLT_g35_loose_L1EM15;
   Int_t           HLT_g35_loose_g25_loose;
   Int_t           HLT_g35_medium_g25_medium;
   Int_t           HLT_g40_loose_L1EM15;
   Int_t           HLT_g45_loose_L1EM15;
   Int_t           HLT_g50_loose_L1EM15;
   Int_t           HLT_g60_loose;
   Int_t           HLT_g70_loose;
   Int_t           HLT_g80_loose;
   Int_t           HLT_j80_bmv2c2060_split_xe60_L12J50_XE40;
   Int_t           HLT_mu20_2mu4noL1;
   Int_t           HLT_mu20_iloose_L1MU15;
   Int_t           HLT_mu20_mu8noL1;
   Int_t           HLT_mu22_mu8noL1;
   Int_t           HLT_mu24_imedium;
   Int_t           HLT_mu24_ivarloose;
   Int_t           HLT_mu24_ivarmedium;
   Int_t           HLT_mu26_imedium;
   Int_t           HLT_mu26_ivarmedium;
   Int_t           HLT_mu40;
   Int_t           HLT_mu50;
   Int_t           HLT_xe100_L1XE50;
   Int_t           HLT_xe100_mht_L1XE50;
   Int_t           HLT_xe110_mht_L1XE50;
   Int_t           HLT_xe110_mht_L1XE50_AND_xe70_L1XE50;
   Int_t           HLT_xe130_mht_L1XE50;
   Int_t           HLT_xe80_tc_lcw_L1XE50;
   Int_t           HLT_xe90_mht_L1XE50;

   // List of branches
   TBranch        *b_runNumber;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_m_mcChannelNumber;   //!
   TBranch        *b_PV_z;   //!
   TBranch        *b_tru_ph_pt;   //!
   TBranch        *b_tru_ph_eta;   //!
   TBranch        *b_tru_ph_phi;   //!
   TBranch        *b_tru_ph_E;   //!
   TBranch        *b_tru_mom_pt;   //!
   TBranch        *b_tru_mom_eta;   //!
   TBranch        *b_tru_mom_phi;   //!
   TBranch        *b_tru_mom_E;   //!
   TBranch        *b_tru_mom_pdgid;   //!
   TBranch        *b_tru_dau_pt;   //!
   TBranch        *b_tru_dau_eta;   //!
   TBranch        *b_tru_dau_phi;   //!
   TBranch        *b_tru_dau_E;   //!
   TBranch        *b_tru_lep_pt;   //!
   TBranch        *b_tru_lep_eta;   //!
   TBranch        *b_tru_lep_phi;   //!
   TBranch        *b_tru_lep_E;   //!
   TBranch        *b_tru_jets_pt;   //!
   TBranch        *b_tru_jets_eta;   //!
   TBranch        *b_tru_jets_phi;   //!
   TBranch        *b_tru_jets_E;   //!
   TBranch        *b_tru_lep_pdgid;   //!
   TBranch        *b_tru_dau_pdgid;   //!
   TBranch        *b_tru_phprod_vx;   //!
   TBranch        *b_tru_phprod_vy;   //!
   TBranch        *b_tru_phprod_vz;   //!
   TBranch        *b_tru_phprod_vt;   //!
   TBranch        *b_tru_momprod_vx;   //!
   TBranch        *b_tru_momprod_vy;   //!
   TBranch        *b_tru_momprod_vz;   //!
   TBranch        *b_tru_momprod_vt;   //!
   TBranch        *b_tru_n_ph;   //!
   TBranch        *b_tru_n_lep;   //!
   TBranch        *b_tru_n_jets;   //!
   TBranch        *b_tru_MET;   //!
   TBranch        *b_tru_ph_t;   //!
   TBranch        *b_tru_ph_z;   //!
   TBranch        *b_tru_mom_tof;   //!
   TBranch        *b_tru_ph_tof;   //!
   TBranch        *b_tru_promptph_t;   //!
   TBranch        *b_ph_pt;   //!
   TBranch        *b_ph_eta;   //!
   TBranch        *b_ph_phi;   //!
   TBranch        *b_ph_E;   //!
   TBranch        *b_n_ph;   //!
   TBranch        *b_ph_t;   //!
   TBranch        *b_ph_convType;   //!
   TBranch        *b_ph_isTight;   //!
   TBranch        *b_ph_isLoose;   //!
   TBranch        *b_ph_passIso;   //!
   TBranch        *b_ph_isClean;   //!
   TBranch        *b_ph_conv_z;   //!
   TBranch        *b_ph_conv_z_err;   //!
   TBranch        *b_ph_calo_z;   //!
   TBranch        *b_ph_calo_z_err;   //!
   TBranch        *b_wt_xs;   //!
   TBranch        *b_wt_kf;   //!
   TBranch        *b_wt_ge;   //!
   TBranch        *b_wt_mc;   //!
   TBranch        *b_wt_nEvents;   //!
   TBranch        *b_wt_wt;   //!
   TBranch        *b_intlumi;   //!
   TBranch        *b_ph_topoetcone20;   //!
   TBranch        *b_ph_topoetcone40;   //!
   TBranch        *b_ph_ptcone20;   //!
   TBranch        *b_ph_f1;   //!
   TBranch        *b_ph_f3;   //!
   TBranch        *b_ph_etas2;   //!
   TBranch        *b_ph_phis2;   //!
   TBranch        *b_ph_maxEcell_onlId;   //!
   TBranch        *b_ph_maxEcell_gain;   //!
   TBranch        *b_ph_maxEcell_E;   //!
   TBranch        *b_ph_maxEcell_x;   //!
   TBranch        *b_ph_maxEcell_y;   //!
   TBranch        *b_ph_maxEcell_z;   //!
   TBranch        *b_ph_maxEcell_time;   //!
   TBranch        *b_ph_Reta;   //!
   TBranch        *b_ph_Rphi;   //!
   TBranch        *b_ph_dEs;   //!
   TBranch        *b_ph_Rmax2;   //!
   TBranch        *b_ph_wstot;   //!
   TBranch        *b_ph_ws3;   //!
   TBranch        *b_ph_Fside;   //!
   TBranch        *b_ph_Eratio;   //!
   TBranch        *b_ph_Rhad;   //!
   TBranch        *b_m_met;   //!
   TBranch        *b_m_mpx;   //!
   TBranch        *b_m_mpy;   //!
   TBranch        *b_m_weight;   //!
   TBranch        *b_m_sumet;   //!
   TBranch        *b_n_mu;   //!
   TBranch        *b_mu_pt;   //!
   TBranch        *b_mu_eta;   //!
   TBranch        *b_mu_phi;   //!
   TBranch        *b_mu_E;   //!
   TBranch        *b_n_jet;   //!
   TBranch        *b_jet_pt;   //!
   TBranch        *b_jet_eta;   //!
   TBranch        *b_jet_phi;   //!
   TBranch        *b_jet_E;   //!
   TBranch        *b_n_el;   //!
   TBranch        *b_el_pt;   //!
   TBranch        *b_el_eta;   //!
   TBranch        *b_el_phi;   //!
   TBranch        *b_el_E;   //!
   TBranch        *b_el_t;   //!
   TBranch        *b_el_calo_z;   //!
   TBranch        *b_el_calo_z_err;   //!
   TBranch        *b_el_Reta;   //!
   TBranch        *b_el_Rphi;   //!
   TBranch        *b_el_dEs;   //!
   TBranch        *b_el_Rmax2;   //!
   TBranch        *b_el_wstot;   //!
   TBranch        *b_el_ws3;   //!
   TBranch        *b_el_Fside;   //!
   TBranch        *b_el_Eratio;   //!
   TBranch        *b_el_Rhad;   //!
   TBranch        *b_el_maxEcell_onlId;   //!
   TBranch        *b_el_maxEcell_gain;   //!
   TBranch        *b_el_maxEcell_E;   //!
   TBranch        *b_el_maxEcell_x;   //!
   TBranch        *b_el_maxEcell_y;   //!
   TBranch        *b_el_maxEcell_z;   //!
   TBranch        *b_el_maxEcell_time;   //!
   TBranch        *b_HLT_2e12_lhloose_nod0_mu10;   //!
   TBranch        *b_HLT_2e12_lhlvoose_nod0_L12EM10VH;   //!
   TBranch        *b_HLT_2e15_lhvloose_nod0_L12EM13VH;   //!
   TBranch        *b_HLT_2e17_lhvloose_nod0;   //!
   TBranch        *b_HLT_2g20_tight;   //!
   TBranch        *b_HLT_2g50_loose;   //!
   TBranch        *b_HLT_3mu6_msonly;   //!
   TBranch        *b_HLT_e120_lhloose;   //!
   TBranch        *b_HLT_e120_lhloose_nod0;   //!
   TBranch        *b_HLT_e120_loose;   //!
   TBranch        *b_HLT_e140_lhloose;   //!
   TBranch        *b_HLT_e140_lhloose_nod0;   //!
   TBranch        *b_HLT_e140_loose;   //!
   TBranch        *b_HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH;   //!
   TBranch        *b_HLT_e17_lhloose_nod0_2e9_lhloose_nod0;   //!
   TBranch        *b_HLT_e17_lhloose_nod0_mu14;   //!
   TBranch        *b_HLT_e24_lhmedium_iloose_EM18VH;   //!
   TBranch        *b_HLT_e24_lhmedium_iloose_EM20VH;   //!
   TBranch        *b_HLT_e24_lhmedium_nod0_ivarloose;   //!
   TBranch        *b_HLT_e24_lhtight_iloose;   //!
   TBranch        *b_HLT_e24_lhtight_iloose_EM20VH;   //!
   TBranch        *b_HLT_e24_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e24_medium_iloose_EM18VH;   //!
   TBranch        *b_HLT_e24_medium_iloose_EM20VH;   //!
   TBranch        *b_HLT_e24_tight_iloose;   //!
   TBranch        *b_HLT_e24_tight_iloose_EM20VH;   //!
   TBranch        *b_HLT_e26_lhtight_iloose;   //!
   TBranch        *b_HLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_HLT_e26_lhtight_smooth_ivarloose;   //!
   TBranch        *b_HLT_e26_tight_iloose;   //!
   TBranch        *b_HLT_e60_lhmedium;   //!
   TBranch        *b_HLT_e60_lhmedium_nod0;   //!
   TBranch        *b_HLT_e60_medium;   //!
   TBranch        *b_HLT_e70_lhloose_nod0_xe70noL1;   //!
   TBranch        *b_HLT_e7_lhmedium_nod0_mu24;   //!
   TBranch        *b_HLT_g100_loose;   //!
   TBranch        *b_HLT_g10_loose;   //!
   TBranch        *b_HLT_g120_loose;   //!
   TBranch        *b_HLT_g140_loose;   //!
   TBranch        *b_HLT_g15_loose_L1EM7;   //!
   TBranch        *b_HLT_g20_loose_L1EM12;   //!
   TBranch        *b_HLT_g25_loose_L1EM15;   //!
   TBranch        *b_HLT_g25_medium_mu24;   //!
   TBranch        *b_HLT_g35_loose_L1EM15;   //!
   TBranch        *b_HLT_g35_loose_g25_loose;   //!
   TBranch        *b_HLT_g35_medium_g25_medium;   //!
   TBranch        *b_HLT_g40_loose_L1EM15;   //!
   TBranch        *b_HLT_g45_loose_L1EM15;   //!
   TBranch        *b_HLT_g50_loose_L1EM15;   //!
   TBranch        *b_HLT_g60_loose;   //!
   TBranch        *b_HLT_g70_loose;   //!
   TBranch        *b_HLT_g80_loose;   //!
   TBranch        *b_HLT_j80_bmv2c2060_split_xe60_L12J50_XE40;   //!
   TBranch        *b_HLT_mu20_2mu4noL1;   //!
   TBranch        *b_HLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_HLT_mu20_mu8noL1;   //!
   TBranch        *b_HLT_mu22_mu8noL1;   //!
   TBranch        *b_HLT_mu24_imedium;   //!
   TBranch        *b_HLT_mu24_ivarloose;   //!
   TBranch        *b_HLT_mu24_ivarmedium;   //!
   TBranch        *b_HLT_mu26_imedium;   //!
   TBranch        *b_HLT_mu26_ivarmedium;   //!
   TBranch        *b_HLT_mu40;   //!
   TBranch        *b_HLT_mu50;   //!
   TBranch        *b_HLT_xe100_L1XE50;   //!
   TBranch        *b_HLT_xe100_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50;   //!
   TBranch        *b_HLT_xe110_mht_L1XE50_AND_xe70_L1XE50;   //!
   TBranch        *b_HLT_xe130_mht_L1XE50;   //!
   TBranch        *b_HLT_xe80_tc_lcw_L1XE50;   //!
   TBranch        *b_HLT_xe90_mht_L1XE50;   //!

   output(TTree *tree=0);
   virtual ~output();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef output_cxx
output::output(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("/projecta/projectdirs/atlas/santpur/NPP_framework/files/output_PID_apr12/STDM4/data/user.ssantpur.00300800.DAOD_STDM4_v11_hist/user.ssantpur.18246493._000001.hist-output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("/projecta/projectdirs/atlas/santpur/NPP_framework/files/output_PID_apr12/STDM4/data/user.ssantpur.00300800.DAOD_STDM4_v11_hist/user.ssantpur.18246493._000001.hist-output.root");
      }
      f->GetObject("output",tree);

   }
   Init(tree);
}

output::~output()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t output::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t output::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void output::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   tru_ph_pt = 0;
   tru_ph_eta = 0;
   tru_ph_phi = 0;
   tru_ph_E = 0;
   tru_mom_pt = 0;
   tru_mom_eta = 0;
   tru_mom_phi = 0;
   tru_mom_E = 0;
   tru_mom_pdgid = 0;
   tru_dau_pt = 0;
   tru_dau_eta = 0;
   tru_dau_phi = 0;
   tru_dau_E = 0;
   tru_lep_pt = 0;
   tru_lep_eta = 0;
   tru_lep_phi = 0;
   tru_lep_E = 0;
   tru_jets_pt = 0;
   tru_jets_eta = 0;
   tru_jets_phi = 0;
   tru_jets_E = 0;
   tru_lep_pdgid = 0;
   tru_dau_pdgid = 0;
   tru_phprod_vx = 0;
   tru_phprod_vy = 0;
   tru_phprod_vz = 0;
   tru_phprod_vt = 0;
   tru_momprod_vx = 0;
   tru_momprod_vy = 0;
   tru_momprod_vz = 0;
   tru_momprod_vt = 0;
   tru_ph_t = 0;
   tru_ph_z = 0;
   tru_mom_tof = 0;
   tru_ph_tof = 0;
   tru_promptph_t = 0;
   ph_pt = 0;
   ph_eta = 0;
   ph_phi = 0;
   ph_E = 0;
   ph_t = 0;
   ph_convType = 0;
   ph_isTight = 0;
   ph_isLoose = 0;
   ph_passIso = 0;
   ph_isClean = 0;
   ph_conv_z = 0;
   ph_conv_z_err = 0;
   ph_calo_z = 0;
   ph_calo_z_err = 0;
   ph_topoetcone20 = 0;
   ph_topoetcone40 = 0;
   ph_ptcone20 = 0;
   ph_f1 = 0;
   ph_f3 = 0;
   ph_etas2 = 0;
   ph_phis2 = 0;
   ph_maxEcell_onlId = 0;
   ph_maxEcell_gain = 0;
   ph_maxEcell_E = 0;
   ph_maxEcell_x = 0;
   ph_maxEcell_y = 0;
   ph_maxEcell_z = 0;
   ph_maxEcell_time = 0;
   ph_Reta = 0;
   ph_Rphi = 0;
   ph_dEs = 0;
   ph_Rmax2 = 0;
   ph_wstot = 0;
   ph_ws3 = 0;
   ph_Fside = 0;
   ph_Eratio = 0;
   ph_Rhad = 0;
   mu_pt = 0;
   mu_eta = 0;
   mu_phi = 0;
   mu_E = 0;
   jet_pt = 0;
   jet_eta = 0;
   jet_phi = 0;
   jet_E = 0;
   el_pt = 0;
   el_eta = 0;
   el_phi = 0;
   el_E = 0;
   el_t = 0;
   el_calo_z = 0;
   el_calo_z_err = 0;
   el_Reta = 0;
   el_Rphi = 0;
   el_dEs = 0;
   el_Rmax2 = 0;
   el_wstot = 0;
   el_ws3 = 0;
   el_Fside = 0;
   el_Eratio = 0;
   el_Rhad = 0;
   el_maxEcell_onlId = 0;
   el_maxEcell_gain = 0;
   el_maxEcell_E = 0;
   el_maxEcell_x = 0;
   el_maxEcell_y = 0;
   el_maxEcell_z = 0;
   el_maxEcell_time = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("m_mcChannelNumber", &m_mcChannelNumber, &b_m_mcChannelNumber);
   fChain->SetBranchAddress("PV_z", &PV_z, &b_PV_z);
   fChain->SetBranchAddress("tru_ph_pt", &tru_ph_pt, &b_tru_ph_pt);
   fChain->SetBranchAddress("tru_ph_eta", &tru_ph_eta, &b_tru_ph_eta);
   fChain->SetBranchAddress("tru_ph_phi", &tru_ph_phi, &b_tru_ph_phi);
   fChain->SetBranchAddress("tru_ph_E", &tru_ph_E, &b_tru_ph_E);
   fChain->SetBranchAddress("tru_mom_pt", &tru_mom_pt, &b_tru_mom_pt);
   fChain->SetBranchAddress("tru_mom_eta", &tru_mom_eta, &b_tru_mom_eta);
   fChain->SetBranchAddress("tru_mom_phi", &tru_mom_phi, &b_tru_mom_phi);
   fChain->SetBranchAddress("tru_mom_E", &tru_mom_E, &b_tru_mom_E);
   fChain->SetBranchAddress("tru_mom_pdgid", &tru_mom_pdgid, &b_tru_mom_pdgid);
   fChain->SetBranchAddress("tru_dau_pt", &tru_dau_pt, &b_tru_dau_pt);
   fChain->SetBranchAddress("tru_dau_eta", &tru_dau_eta, &b_tru_dau_eta);
   fChain->SetBranchAddress("tru_dau_phi", &tru_dau_phi, &b_tru_dau_phi);
   fChain->SetBranchAddress("tru_dau_E", &tru_dau_E, &b_tru_dau_E);
   fChain->SetBranchAddress("tru_lep_pt", &tru_lep_pt, &b_tru_lep_pt);
   fChain->SetBranchAddress("tru_lep_eta", &tru_lep_eta, &b_tru_lep_eta);
   fChain->SetBranchAddress("tru_lep_phi", &tru_lep_phi, &b_tru_lep_phi);
   fChain->SetBranchAddress("tru_lep_E", &tru_lep_E, &b_tru_lep_E);
   fChain->SetBranchAddress("tru_jets_pt", &tru_jets_pt, &b_tru_jets_pt);
   fChain->SetBranchAddress("tru_jets_eta", &tru_jets_eta, &b_tru_jets_eta);
   fChain->SetBranchAddress("tru_jets_phi", &tru_jets_phi, &b_tru_jets_phi);
   fChain->SetBranchAddress("tru_jets_E", &tru_jets_E, &b_tru_jets_E);
   fChain->SetBranchAddress("tru_lep_pdgid", &tru_lep_pdgid, &b_tru_lep_pdgid);
   fChain->SetBranchAddress("tru_dau_pdgid", &tru_dau_pdgid, &b_tru_dau_pdgid);
   fChain->SetBranchAddress("tru_phprod_vx", &tru_phprod_vx, &b_tru_phprod_vx);
   fChain->SetBranchAddress("tru_phprod_vy", &tru_phprod_vy, &b_tru_phprod_vy);
   fChain->SetBranchAddress("tru_phprod_vz", &tru_phprod_vz, &b_tru_phprod_vz);
   fChain->SetBranchAddress("tru_phprod_vt", &tru_phprod_vt, &b_tru_phprod_vt);
   fChain->SetBranchAddress("tru_momprod_vx", &tru_momprod_vx, &b_tru_momprod_vx);
   fChain->SetBranchAddress("tru_momprod_vy", &tru_momprod_vy, &b_tru_momprod_vy);
   fChain->SetBranchAddress("tru_momprod_vz", &tru_momprod_vz, &b_tru_momprod_vz);
   fChain->SetBranchAddress("tru_momprod_vt", &tru_momprod_vt, &b_tru_momprod_vt);
   fChain->SetBranchAddress("tru_n_ph", &tru_n_ph, &b_tru_n_ph);
   fChain->SetBranchAddress("tru_n_lep", &tru_n_lep, &b_tru_n_lep);
   fChain->SetBranchAddress("tru_n_jets", &tru_n_jets, &b_tru_n_jets);
   fChain->SetBranchAddress("tru_MET", &tru_MET, &b_tru_MET);
   fChain->SetBranchAddress("tru_ph_t", &tru_ph_t, &b_tru_ph_t);
   fChain->SetBranchAddress("tru_ph_z", &tru_ph_z, &b_tru_ph_z);
   fChain->SetBranchAddress("tru_mom_tof", &tru_mom_tof, &b_tru_mom_tof);
   fChain->SetBranchAddress("tru_ph_tof", &tru_ph_tof, &b_tru_ph_tof);
   fChain->SetBranchAddress("tru_promptph_t", &tru_promptph_t, &b_tru_promptph_t);
   fChain->SetBranchAddress("ph_pt", &ph_pt, &b_ph_pt);
   fChain->SetBranchAddress("ph_eta", &ph_eta, &b_ph_eta);
   fChain->SetBranchAddress("ph_phi", &ph_phi, &b_ph_phi);
   fChain->SetBranchAddress("ph_E", &ph_E, &b_ph_E);
   fChain->SetBranchAddress("n_ph", &n_ph, &b_n_ph);
   fChain->SetBranchAddress("ph_t", &ph_t, &b_ph_t);
   fChain->SetBranchAddress("ph_convType", &ph_convType, &b_ph_convType);
   fChain->SetBranchAddress("ph_isTight", &ph_isTight, &b_ph_isTight);
   fChain->SetBranchAddress("ph_isLoose", &ph_isLoose, &b_ph_isLoose);
   fChain->SetBranchAddress("ph_passIso", &ph_passIso, &b_ph_passIso);
   fChain->SetBranchAddress("ph_isClean", &ph_isClean, &b_ph_isClean);
   fChain->SetBranchAddress("ph_conv_z", &ph_conv_z, &b_ph_conv_z);
   fChain->SetBranchAddress("ph_conv_z_err", &ph_conv_z_err, &b_ph_conv_z_err);
   fChain->SetBranchAddress("ph_calo_z", &ph_calo_z, &b_ph_calo_z);
   fChain->SetBranchAddress("ph_calo_z_err", &ph_calo_z_err, &b_ph_calo_z_err);
   fChain->SetBranchAddress("wt_xs", &wt_xs, &b_wt_xs);
   fChain->SetBranchAddress("wt_kf", &wt_kf, &b_wt_kf);
   fChain->SetBranchAddress("wt_ge", &wt_ge, &b_wt_ge);
   fChain->SetBranchAddress("wt_mc", &wt_mc, &b_wt_mc);
   fChain->SetBranchAddress("wt_nEvents", &wt_nEvents, &b_wt_nEvents);
   fChain->SetBranchAddress("wt_wt", &wt_wt, &b_wt_wt);
   fChain->SetBranchAddress("intlumi", &intlumi, &b_intlumi);
   fChain->SetBranchAddress("ph_topoetcone20", &ph_topoetcone20, &b_ph_topoetcone20);
   fChain->SetBranchAddress("ph_topoetcone40", &ph_topoetcone40, &b_ph_topoetcone40);
   fChain->SetBranchAddress("ph_ptcone20", &ph_ptcone20, &b_ph_ptcone20);
   fChain->SetBranchAddress("ph_f1", &ph_f1, &b_ph_f1);
   fChain->SetBranchAddress("ph_f3", &ph_f3, &b_ph_f3);
   fChain->SetBranchAddress("ph_etas2", &ph_etas2, &b_ph_etas2);
   fChain->SetBranchAddress("ph_phis2", &ph_phis2, &b_ph_phis2);
   fChain->SetBranchAddress("ph_maxEcell_onlId", &ph_maxEcell_onlId, &b_ph_maxEcell_onlId);
   fChain->SetBranchAddress("ph_maxEcell_gain", &ph_maxEcell_gain, &b_ph_maxEcell_gain);
   fChain->SetBranchAddress("ph_maxEcell_E", &ph_maxEcell_E, &b_ph_maxEcell_E);
   fChain->SetBranchAddress("ph_maxEcell_x", &ph_maxEcell_x, &b_ph_maxEcell_x);
   fChain->SetBranchAddress("ph_maxEcell_y", &ph_maxEcell_y, &b_ph_maxEcell_y);
   fChain->SetBranchAddress("ph_maxEcell_z", &ph_maxEcell_z, &b_ph_maxEcell_z);
   fChain->SetBranchAddress("ph_maxEcell_time", &ph_maxEcell_time, &b_ph_maxEcell_time);
   fChain->SetBranchAddress("ph_Reta", &ph_Reta, &b_ph_Reta);
   fChain->SetBranchAddress("ph_Rphi", &ph_Rphi, &b_ph_Rphi);
   fChain->SetBranchAddress("ph_dEs", &ph_dEs, &b_ph_dEs);
   fChain->SetBranchAddress("ph_Rmax2", &ph_Rmax2, &b_ph_Rmax2);
   fChain->SetBranchAddress("ph_wstot", &ph_wstot, &b_ph_wstot);
   fChain->SetBranchAddress("ph_ws3", &ph_ws3, &b_ph_ws3);
   fChain->SetBranchAddress("ph_Fside", &ph_Fside, &b_ph_Fside);
   fChain->SetBranchAddress("ph_Eratio", &ph_Eratio, &b_ph_Eratio);
   fChain->SetBranchAddress("ph_Rhad", &ph_Rhad, &b_ph_Rhad);
   fChain->SetBranchAddress("m_met", &m_met, &b_m_met);
   fChain->SetBranchAddress("m_mpx", &m_mpx, &b_m_mpx);
   fChain->SetBranchAddress("m_mpy", &m_mpy, &b_m_mpy);
   fChain->SetBranchAddress("m_weight", &m_weight, &b_m_weight);
   fChain->SetBranchAddress("m_sumet", &m_sumet, &b_m_sumet);
   fChain->SetBranchAddress("n_mu", &n_mu, &b_n_mu);
   fChain->SetBranchAddress("mu_pt", &mu_pt, &b_mu_pt);
   fChain->SetBranchAddress("mu_eta", &mu_eta, &b_mu_eta);
   fChain->SetBranchAddress("mu_phi", &mu_phi, &b_mu_phi);
   fChain->SetBranchAddress("mu_E", &mu_E, &b_mu_E);
   fChain->SetBranchAddress("n_jet", &n_jet, &b_n_jet);
   fChain->SetBranchAddress("jet_pt", &jet_pt, &b_jet_pt);
   fChain->SetBranchAddress("jet_eta", &jet_eta, &b_jet_eta);
   fChain->SetBranchAddress("jet_phi", &jet_phi, &b_jet_phi);
   fChain->SetBranchAddress("jet_E", &jet_E, &b_jet_E);
   fChain->SetBranchAddress("n_el", &n_el, &b_n_el);
   fChain->SetBranchAddress("el_pt", &el_pt, &b_el_pt);
   fChain->SetBranchAddress("el_eta", &el_eta, &b_el_eta);
   fChain->SetBranchAddress("el_phi", &el_phi, &b_el_phi);
   fChain->SetBranchAddress("el_E", &el_E, &b_el_E);
   fChain->SetBranchAddress("el_t", &el_t, &b_el_t);
   fChain->SetBranchAddress("el_calo_z", &el_calo_z, &b_el_calo_z);
   fChain->SetBranchAddress("el_calo_z_err", &el_calo_z_err, &b_el_calo_z_err);
   fChain->SetBranchAddress("el_Reta", &el_Reta, &b_el_Reta);
   fChain->SetBranchAddress("el_Rphi", &el_Rphi, &b_el_Rphi);
   fChain->SetBranchAddress("el_dEs", &el_dEs, &b_el_dEs);
   fChain->SetBranchAddress("el_Rmax2", &el_Rmax2, &b_el_Rmax2);
   fChain->SetBranchAddress("el_wstot", &el_wstot, &b_el_wstot);
   fChain->SetBranchAddress("el_ws3", &el_ws3, &b_el_ws3);
   fChain->SetBranchAddress("el_Fside", &el_Fside, &b_el_Fside);
   fChain->SetBranchAddress("el_Eratio", &el_Eratio, &b_el_Eratio);
   fChain->SetBranchAddress("el_Rhad", &el_Rhad, &b_el_Rhad);
   fChain->SetBranchAddress("el_maxEcell_onlId", &el_maxEcell_onlId, &b_el_maxEcell_onlId);
   fChain->SetBranchAddress("el_maxEcell_gain", &el_maxEcell_gain, &b_el_maxEcell_gain);
   fChain->SetBranchAddress("el_maxEcell_E", &el_maxEcell_E, &b_el_maxEcell_E);
   fChain->SetBranchAddress("el_maxEcell_x", &el_maxEcell_x, &b_el_maxEcell_x);
   fChain->SetBranchAddress("el_maxEcell_y", &el_maxEcell_y, &b_el_maxEcell_y);
   fChain->SetBranchAddress("el_maxEcell_z", &el_maxEcell_z, &b_el_maxEcell_z);
   fChain->SetBranchAddress("el_maxEcell_time", &el_maxEcell_time, &b_el_maxEcell_time);
   fChain->SetBranchAddress("HLT_2e12_lhloose_nod0_mu10", &HLT_2e12_lhloose_nod0_mu10, &b_HLT_2e12_lhloose_nod0_mu10);
   fChain->SetBranchAddress("HLT_2e12_lhlvoose_nod0_L12EM10VH", &HLT_2e12_lhlvoose_nod0_L12EM10VH, &b_HLT_2e12_lhlvoose_nod0_L12EM10VH);
   fChain->SetBranchAddress("HLT_2e15_lhvloose_nod0_L12EM13VH", &HLT_2e15_lhvloose_nod0_L12EM13VH, &b_HLT_2e15_lhvloose_nod0_L12EM13VH);
   fChain->SetBranchAddress("HLT_2e17_lhvloose_nod0", &HLT_2e17_lhvloose_nod0, &b_HLT_2e17_lhvloose_nod0);
   fChain->SetBranchAddress("HLT_2g20_tight", &HLT_2g20_tight, &b_HLT_2g20_tight);
   fChain->SetBranchAddress("HLT_2g50_loose", &HLT_2g50_loose, &b_HLT_2g50_loose);
   fChain->SetBranchAddress("HLT_3mu6_msonly", &HLT_3mu6_msonly, &b_HLT_3mu6_msonly);
   fChain->SetBranchAddress("HLT_e120_lhloose", &HLT_e120_lhloose, &b_HLT_e120_lhloose);
   fChain->SetBranchAddress("HLT_e120_lhloose_nod0", &HLT_e120_lhloose_nod0, &b_HLT_e120_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e120_loose", &HLT_e120_loose, &b_HLT_e120_loose);
   fChain->SetBranchAddress("HLT_e140_lhloose", &HLT_e140_lhloose, &b_HLT_e140_lhloose);
   fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e140_loose", &HLT_e140_loose, &b_HLT_e140_loose);
   fChain->SetBranchAddress("HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH", &HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH, &b_HLT_e17_lhloose_nod0_2e10_lhloose_nod0_L1EM15VH_3EM8VH);
   fChain->SetBranchAddress("HLT_e17_lhloose_nod0_2e9_lhloose_nod0", &HLT_e17_lhloose_nod0_2e9_lhloose_nod0, &b_HLT_e17_lhloose_nod0_2e9_lhloose_nod0);
   fChain->SetBranchAddress("HLT_e17_lhloose_nod0_mu14", &HLT_e17_lhloose_nod0_mu14, &b_HLT_e17_lhloose_nod0_mu14);
   fChain->SetBranchAddress("HLT_e24_lhmedium_iloose_EM18VH", &HLT_e24_lhmedium_iloose_EM18VH, &b_HLT_e24_lhmedium_iloose_EM18VH);
   fChain->SetBranchAddress("HLT_e24_lhmedium_iloose_EM20VH", &HLT_e24_lhmedium_iloose_EM20VH, &b_HLT_e24_lhmedium_iloose_EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhmedium_nod0_ivarloose", &HLT_e24_lhmedium_nod0_ivarloose, &b_HLT_e24_lhmedium_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e24_lhtight_iloose", &HLT_e24_lhtight_iloose, &b_HLT_e24_lhtight_iloose);
   fChain->SetBranchAddress("HLT_e24_lhtight_iloose_EM20VH", &HLT_e24_lhtight_iloose_EM20VH, &b_HLT_e24_lhtight_iloose_EM20VH);
   fChain->SetBranchAddress("HLT_e24_lhtight_nod0_ivarloose", &HLT_e24_lhtight_nod0_ivarloose, &b_HLT_e24_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e24_medium_iloose_EM18VH", &HLT_e24_medium_iloose_EM18VH, &b_HLT_e24_medium_iloose_EM18VH);
   fChain->SetBranchAddress("HLT_e24_medium_iloose_EM20VH", &HLT_e24_medium_iloose_EM20VH, &b_HLT_e24_medium_iloose_EM20VH);
   fChain->SetBranchAddress("HLT_e24_tight_iloose", &HLT_e24_tight_iloose, &b_HLT_e24_tight_iloose);
   fChain->SetBranchAddress("HLT_e24_tight_iloose_EM20VH", &HLT_e24_tight_iloose_EM20VH, &b_HLT_e24_tight_iloose_EM20VH);
   fChain->SetBranchAddress("HLT_e26_lhtight_iloose", &HLT_e26_lhtight_iloose, &b_HLT_e26_lhtight_iloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("HLT_e26_lhtight_smooth_ivarloose", &HLT_e26_lhtight_smooth_ivarloose, &b_HLT_e26_lhtight_smooth_ivarloose);
   fChain->SetBranchAddress("HLT_e26_tight_iloose", &HLT_e26_tight_iloose, &b_HLT_e26_tight_iloose);
   fChain->SetBranchAddress("HLT_e60_lhmedium", &HLT_e60_lhmedium, &b_HLT_e60_lhmedium);
   fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("HLT_e60_medium", &HLT_e60_medium, &b_HLT_e60_medium);
   fChain->SetBranchAddress("HLT_e70_lhloose_nod0_xe70noL1", &HLT_e70_lhloose_nod0_xe70noL1, &b_HLT_e70_lhloose_nod0_xe70noL1);
   fChain->SetBranchAddress("HLT_e7_lhmedium_nod0_mu24", &HLT_e7_lhmedium_nod0_mu24, &b_HLT_e7_lhmedium_nod0_mu24);
   fChain->SetBranchAddress("HLT_g100_loose", &HLT_g100_loose, &b_HLT_g100_loose);
   fChain->SetBranchAddress("HLT_g10_loose", &HLT_g10_loose, &b_HLT_g10_loose);
   fChain->SetBranchAddress("HLT_g120_loose", &HLT_g120_loose, &b_HLT_g120_loose);
   fChain->SetBranchAddress("HLT_g140_loose", &HLT_g140_loose, &b_HLT_g140_loose);
   fChain->SetBranchAddress("HLT_g15_loose_L1EM7", &HLT_g15_loose_L1EM7, &b_HLT_g15_loose_L1EM7);
   fChain->SetBranchAddress("HLT_g20_loose_L1EM12", &HLT_g20_loose_L1EM12, &b_HLT_g20_loose_L1EM12);
   fChain->SetBranchAddress("HLT_g25_loose_L1EM15", &HLT_g25_loose_L1EM15, &b_HLT_g25_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g25_medium_mu24", &HLT_g25_medium_mu24, &b_HLT_g25_medium_mu24);
   fChain->SetBranchAddress("HLT_g35_loose_L1EM15", &HLT_g35_loose_L1EM15, &b_HLT_g35_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g35_loose_g25_loose", &HLT_g35_loose_g25_loose, &b_HLT_g35_loose_g25_loose);
   fChain->SetBranchAddress("HLT_g35_medium_g25_medium", &HLT_g35_medium_g25_medium, &b_HLT_g35_medium_g25_medium);
   fChain->SetBranchAddress("HLT_g40_loose_L1EM15", &HLT_g40_loose_L1EM15, &b_HLT_g40_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g45_loose_L1EM15", &HLT_g45_loose_L1EM15, &b_HLT_g45_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g50_loose_L1EM15", &HLT_g50_loose_L1EM15, &b_HLT_g50_loose_L1EM15);
   fChain->SetBranchAddress("HLT_g60_loose", &HLT_g60_loose, &b_HLT_g60_loose);
   fChain->SetBranchAddress("HLT_g70_loose", &HLT_g70_loose, &b_HLT_g70_loose);
   fChain->SetBranchAddress("HLT_g80_loose", &HLT_g80_loose, &b_HLT_g80_loose);
   fChain->SetBranchAddress("HLT_j80_bmv2c2060_split_xe60_L12J50_XE40", &HLT_j80_bmv2c2060_split_xe60_L12J50_XE40, &b_HLT_j80_bmv2c2060_split_xe60_L12J50_XE40);
   fChain->SetBranchAddress("HLT_mu20_2mu4noL1", &HLT_mu20_2mu4noL1, &b_HLT_mu20_2mu4noL1);
   fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("HLT_mu20_mu8noL1", &HLT_mu20_mu8noL1, &b_HLT_mu20_mu8noL1);
   fChain->SetBranchAddress("HLT_mu22_mu8noL1", &HLT_mu22_mu8noL1, &b_HLT_mu22_mu8noL1);
   fChain->SetBranchAddress("HLT_mu24_imedium", &HLT_mu24_imedium, &b_HLT_mu24_imedium);
   fChain->SetBranchAddress("HLT_mu24_ivarloose", &HLT_mu24_ivarloose, &b_HLT_mu24_ivarloose);
   fChain->SetBranchAddress("HLT_mu24_ivarmedium", &HLT_mu24_ivarmedium, &b_HLT_mu24_ivarmedium);
   fChain->SetBranchAddress("HLT_mu26_imedium", &HLT_mu26_imedium, &b_HLT_mu26_imedium);
   fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
   fChain->SetBranchAddress("HLT_mu40", &HLT_mu40, &b_HLT_mu40);
   fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
   fChain->SetBranchAddress("HLT_xe100_L1XE50", &HLT_xe100_L1XE50, &b_HLT_xe100_L1XE50);
   fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe110_mht_L1XE50_AND_xe70_L1XE50", &HLT_xe110_mht_L1XE50_AND_xe70_L1XE50, &b_HLT_xe110_mht_L1XE50_AND_xe70_L1XE50);
   fChain->SetBranchAddress("HLT_xe130_mht_L1XE50", &HLT_xe130_mht_L1XE50, &b_HLT_xe130_mht_L1XE50);
   fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
   fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
   Notify();
}

Bool_t output::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void output::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t output::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef output_cxx
