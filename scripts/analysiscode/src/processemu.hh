#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TROOT.h"
#include "THStack.h"
#include "TString.h"
#include "TH1.h"
#include "TH2F.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooPlot.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooAbsData.h"
#include "RooAbsRealLValue.h"
#include "RooAbsPdf.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooDataHist.h"
#include "RooNLLVar.h"
#include "RooSimultaneous.h"
#include "RooExponential.h"
#include "RooGlobalFunc.h"
#include "RooCBShape.h"
#include "RooFormula.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "../aux/output.hh"
#include "header.hh"
using namespace std;
using namespace RooFit;
using std::vector;
#include <sys/stat.h>


output *p;

//Declare all histograms                              
TH1F* cutflow = new TH1F("cutflow","cutflow",15,0,15);

TH1F* lep_pt1 = new TH1F("lep_pt1","lep_pt1",1000,0,1000);                                       
TH1F* lep_eta1 = new TH1F("lep_eta1","lep_eta1",100,-2.5,2.5);                                  
TH1F* lep_phi1 = new TH1F("lep_phi1","lep_phi1",100,-3.5,3.5);                                  
TH1F* lep_E1 = new TH1F("lep_E1","lep_E1",100,0,1000);                                          
TH1F* met = new TH1F("met","met",1000,0,1000);                                          
TH1F* met_bin = new TH1F("met_bin","met_bin",3,0,3);                                          
TH1F* met_20 = new TH1F("met_20","met_20",1000,0,1000);                                          
TH1F* met_50 = new TH1F("met_50","met_50",1000,0,1000);                                          

TH1F* n_lep = new TH1F("n_lep","n_lep",10,0,10);
TH1F* n_ph = new TH1F("n_ph","n_ph",10,0,10);

TH1F* ph_pt1 = new TH1F("ph_pt1","ph_pt1",1000,0,1000);
TH1F* ph_eta1 = new TH1F("ph_eta1","ph_eta1",100,-2.5,2.5);                                  
TH1F* ph_phi1 = new TH1F("ph_phi1","ph_phi1",100,-3.5,3.5);                                  
TH1F* ph_E1 = new TH1F("ph_E1","ph_E1",100,0,1000); 

TH1F* ph_zdca_CR = new TH1F("ph_zdca_CR","ph_zdca_CR",800,-2000,2000);
TH1F* ph_zdca_VR = new TH1F("ph_zdca_VR","ph_zdca_VR",800,-2000,2000);
TH1F* ph_t_CR = new TH1F("ph_t_CR","ph_t_CR",2000,-100,100);
TH1F* ph_t_VR = new TH1F("ph_t_VR","ph_t_VR",2000,-100,100);
TH1F* ph_pt_CR = new TH1F("ph_pt_CR","ph_pt_CR",200,0,1000);
TH1F* ph_pt_VR = new TH1F("ph_pt_VR","ph_pt_VR",200,0,1000);
TH1F* ph_cellE_CR = new TH1F("ph_cellE_CR","ph_cellE_CR",200,0,1000);
TH1F* ph_cellE_VR = new TH1F("ph_cellE_VR","ph_cellE_VR",200,0,1000);

TH1F* met_bin_1lep = new TH1F("met_bin_1lep","met_bin1lep",3,0,3);                                          
TH1F* met_bin_2lepSF = new TH1F("met_bin_2lepSF","met_bin_2lepSF",3,0,3);
TH1F* met_bin_2lepOF = new TH1F("met_bin_2lepOF","met_bin_2lepOF",3,0,3);

TH2F* ph_t_zdca_CR = new TH2F("ph_t_zdca_CR","ph_t_zdca_CR",2000,-100,100,800,-2000,2000);
TH2F* ph_abs_t_zdca_CR = new TH2F("ph_abs_t_zdca_CR","ph_abs_t_zdca_CR",2000,0,100,800,0,2000);
                                         
TH1F* ph_t_full = new TH1F("ph_t_full","ph_t_full",10000,-50,50);
TH1F* el_t_full = new TH1F("el_t_full","el_t_full",10000,-50,50);

TH1F* ph_zdca = new TH1F("ph_zdca","ph_zdca",800,-4000,4000);
TH1F* ph_calo_z = new TH1F("ph_calo_z","ph_calo_z",800,-4000,4000);
TH1F* ph_PV_z = new TH1F("ph_PV_z","ph_PV_z",800,-4000,4000);
TH1F* ph_zdca_bins = new TH1F("ph_zdca_bins","ph_zdca_bins",6,0,6);
TH1F* ph_t_bins = new TH1F("ph_t_bins","ph_t_bins",6,0,6);


   
void get_event(int i) {
  if ( p->LoadTree(i) < 0) { 
    cout<<"\nProblem in LoadTree."
        <<"\nEntry: "<<i<<endl;
    exit(0);
  }
  p->fChain->GetEntry(i);
}


void fill_kinematics(vector<double>* pt1, vector<double>* eta1,vector<double>* phi1, vector<double>* E1,vector<double>* pt2, vector<double>* eta2,vector<double>* phi2, vector<double>* E2,double wt1)
{
  TLorentzVector mu1,mu2,ph1;
  mu1.SetPtEtaPhiE(pt1->at(0),eta1->at(0),phi1->at(0),E1->at(0));
  ph1.SetPtEtaPhiE(pt2->at(0),eta2->at(0),phi2->at(0),E2->at(0));

  lep_pt1->Fill(mu1.Pt(),wt1); lep_eta1->Fill(mu1.Eta(),wt1);
  lep_phi1->Fill(mu1.Phi(),wt1); lep_E1->Fill(mu1.E(),wt1);
  ph_pt1->Fill(ph1.Pt(),wt1); ph_eta1->Fill(ph1.Eta(),wt1);
  ph_phi1->Fill(ph1.Phi(),wt1); ph_E1->Fill(ph1.E(),wt1);
}
