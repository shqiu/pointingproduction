/*
Created by Haichen Wang November 8, 2012
*/

#include "processEegamma.hh"
#include "MCxsec_ee.hh"

int main (int argc, char **argv)
{
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  cout << endl;
		 if (argc < 2)
		 {
		    printf("\nUsage: %s *.root\n\n",argv[0]);
		    exit(0);
		 }
		 
		 int const index = argc - 1;
		
		 TString path[index];
		 
		 for ( int j = 0; j < argc-1; j++)
		 {
			path[j] = (argv[j+1]);
		 }
		 
		 TChain *chain = new TChain("output");
		 
		 for (int k = 0; k < argc-1 ; k++)
		 {
		    chain -> Add(path[k]); // Add the root file to the TChain chain
		    cout << " adding " << path[k] << endl;
		 }
		 
		 p = new output(chain);
		 int numev = p->fChain->GetEntries();
		 p -> fChain ->SetBranchStatus("*",0);

                 p -> fChain ->SetBranchStatus("runNumber",1);
                 p -> fChain ->SetBranchStatus("eventNumber",1);
                 p -> fChain ->SetBranchStatus("m_mcChannelNumber",1);
                 p -> fChain ->SetBranchStatus("m_met",1);
                 p -> fChain ->SetBranchStatus("n_el",1);
                 p -> fChain ->SetBranchStatus("PV_z",1);
                 p -> fChain ->SetBranchStatus("el_pt",1);
                 p -> fChain ->SetBranchStatus("el_eta",1);
                 p -> fChain ->SetBranchStatus("el_phi",1);
                 p -> fChain ->SetBranchStatus("el_E",1);
                 p -> fChain ->SetBranchStatus("el_t",1);
                 p -> fChain ->SetBranchStatus("el_calo_z",1);
                 p -> fChain ->SetBranchStatus("el_calo_z_err",1);
                 p -> fChain ->SetBranchStatus("el_Reta",1);
                 p -> fChain ->SetBranchStatus("el_Rphi",1);
                 p -> fChain ->SetBranchStatus("el_dEs",1);
                 p -> fChain ->SetBranchStatus("el_Rmax2",1);
                 p -> fChain ->SetBranchStatus("el_wstot",1);
                 p -> fChain ->SetBranchStatus("el_ws3",1);
                 p -> fChain ->SetBranchStatus("el_Fside",1);
                 p -> fChain ->SetBranchStatus("el_Eratio",1);
                 p -> fChain ->SetBranchStatus("el_Rhad",1);
		 p -> fChain ->SetBranchStatus("n_ph",1);
		 p -> fChain ->SetBranchStatus("ph_isTight",1);
                 p -> fChain ->SetBranchStatus("ph_isLoose",1);
                 p -> fChain ->SetBranchStatus("ph_passIso",1);
                 p -> fChain ->SetBranchStatus("ph_isClean",1);
		 p -> fChain ->SetBranchStatus("ph_pt",1);
                 p -> fChain ->SetBranchStatus("ph_eta",1);
                 p -> fChain ->SetBranchStatus("ph_phi",1);
                 p -> fChain ->SetBranchStatus("ph_E",1);
                 p -> fChain ->SetBranchStatus("ph_t",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z_err",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z",1);
                 p -> fChain ->SetBranchStatus("ph_convType",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z_err",1);
                 p -> fChain ->SetBranchStatus("ph_Reta",1);
                 p -> fChain ->SetBranchStatus("ph_Rphi",1);
                 p -> fChain ->SetBranchStatus("ph_dEs",1);
                 p -> fChain ->SetBranchStatus("ph_Rmax2",1);
                 p -> fChain ->SetBranchStatus("ph_wstot",1);
                 p -> fChain ->SetBranchStatus("ph_ws3",1);
                 p -> fChain ->SetBranchStatus("ph_Fside",1);
                 p -> fChain ->SetBranchStatus("ph_Eratio",1);
                 p -> fChain ->SetBranchStatus("ph_Rhad",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_onlId",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_gain",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_E",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_x",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_y",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_z",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_time",1);

                 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_nod0_ivarloose",1);
                 p -> fChain ->SetBranchStatus("HLT_2e17_lhvloose_nod0",1);

		 p -> fChain ->SetBranchStatus("HLT_e24_medium_iloose_EM18VH",1);
                 p -> fChain ->SetBranchStatus("HLT_e24_medium_iloose_EM20VH",1);
		
		 //                 p -> fChain ->SetBranchStatus("HLT_e24_tight_iloose_EM20VH",1);
		 //                 p -> fChain ->SetBranchStatus("HLT_e24_tight_iloose",1);
		 //                 p -> fChain ->SetBranchStatus("HLT_e26_tight_iloose",1);
		 //                 p -> fChain ->SetBranchStatus("HLT_e24_lhtight_iloose",1);
		 //                 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_iloose",1);
		 cout << "total number of events to be processed " << numev << endl;
		 double lumi = 33.017; //in fb-1
		 double wt_ge = 1.0;
		 get_event(0) ;
                 int DSID = p->m_mcChannelNumber;
                 bool isData = p->m_mcChannelNumber < 0 ;
                 map<string,double> sample = MCxsec_ee(DSID);
                 double xs = sample["xs"];
                 double Nevents = sample["Nevents"];
		 cout<<"DSID="<<DSID<<endl;
		 define_SS_variables(Reta,Rphi,dEs,Rmax2,wstot,ws3,Fside,Eratio,Rhad);
		 define_pointing_variables(z_res_reco);
		 define_timing_variables(t_res_reco);
		 define_timing_res(ph_t_E_gain, ph_t_cellE_gain);
		 int countmet50=0,countmet20=0;
		 double tbins[7]={-4.0,0.5,1.1,1.3,1.5,1.8,4.0};
		 for( int i = 0 ; i < numev ; i ++ )
		 //for( int i = 0 ; i < 1000000 ; i ++ )
		   {
		     //cout<<"------Event="<<i<<endl;
		     if(i>0 && i%500000==0) cout << "Processed " << i << " events..."<<endl;
			
		     get_event(i) ;

		     //Weight calculation
		     double wt=1.0;
		     if(!isData) wt = lumi*xs*wt_ge/Nevents;
		     trigger_sel->Fill(0.5,wt);
		     //		     if(p->HLT_e26_lhtight_nod0_ivarloose==1) trigger_sel->Fill(1.5,wt);
		     //if(p->HLT_e60_lhmedium_nod0==1) trigger_sel->Fill(2.5,wt);
		     //if(p->HLT_e140_lhloose_nod0==1) trigger_sel->Fill(3.5,wt);
		     //cout<<"Triggers"<<endl;
		     n_e->Fill(p->n_el,wt);
		     //cout<<"filled electron number"<<endl;
		     //cout<<"No. of photons="<<(int)p->ph_pt->size()<<endl;
		     if((int)p->ph_pt->size()>0) fill_kinematics_photon(p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,0,wt);
		     //		     //cout<<"photon 4 vector="<<p->ph_pt->at(0)<<"  "<<p->ph_eta->at(0)<<"  "<<p->ph_phi->at(0)<<"  "<<p->ph_E->at(0)<<"  "<<wt<<endl;
		     //cout<<"Filled all photon kinematics"<<endl;
		     cutflow->Fill(0.5,wt);
		     int trig=0;
		     if(p->HLT_2e17_lhvloose_nod0==1){
		       if((int)p->el_pt->size()>1){
			 if(p->el_pt->at(0) > 18.0 && p->el_pt->at(1) > 18.0) trig=1;}}
		     else if(p->HLT_e26_lhtight_nod0_ivarloose==1){
		       if((int)p->el_pt->size()>1){
			 if(p->el_pt->at(0) > 27.0 && p->el_pt->at(1) > 9.0) trig=2;}}
		     if(trig==0) continue;
		     //if(p->HLT_e26_lhtight_nod0_ivarloose==0) continue;
		     //cout<<"Trigger selected"<<endl;
		     
		     if((int)p->el_pt->size()<2) continue;
		     //cout<<"Two electrons required"<<endl;
		     if(p->el_pt->at(0) < 27.0 || p->el_pt->at(1) < 10.0) continue;
		     cutflow->Fill(1.5,wt);
		     //		     //cout<<"No. of photons="<<(int)(int)p->ph_pt->size()<<endl;
		     if( (int)p->ph_pt->size() < 1) continue;
		     //cout<<"Photon required"<<endl;

		     cutflow->Fill(2.5,wt);
		     //		     fill_kinematics_photon(p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,1,wt);

		     map<string,double> result;
		    
		     fill_2D_hists(result,p->ph_pt,0,wt);

		     if(!p->ph_isClean->at(0) || p->ph_pt->at(0) < 10.0  || ( fabs(p->ph_eta->at(0))>1.37 && fabs(p->ph_eta->at(0))<1.52)) continue;
		     //cout<<"Photon pt cut satisfied"<<endl;

		     result = eegamma(p->el_pt,p->el_eta,p->el_phi,p->el_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E);

		     cutflow->Fill(3.5,wt);
		     //		     if(p->ph_t->at(0) > -1000) continue;
		     cutflow->Fill(4.5,wt);
		     if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 && p->ph_maxEcell_onlId->at(0)!=4057058268516712448 && p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
		       ph_t_all->Fill(p->ph_t->at(0),wt);
		       if(fabs(p->ph_t->at(0))>15.0 && fabs(p->ph_t->at(0))<35.0){
			 m_eegamma_25ns->Fill(result["m_eegamma"],wt);
			 m_ee_eegamma_25ns->Fill(result["m_ee"],result["m_eegamma"],wt);
			 m_ee_25ns->Fill(result["m_ee"],wt);}
		       
		       if(p->ph_isTight->at(0)) ph_t_tight_all->Fill(p->ph_t->at(0),wt);
		     }
		       
		       
		     fill_kinematics(p->el_pt,p->el_eta,p->el_phi,p->el_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,0,wt);
		     
		     fill_hists_minv(result,wt);
		     if(fabs(91.18-result["m_eegamma"])>15.0){
		       ph_t_offZ->Fill(p->ph_t->at(0),wt);
		       for(int k1=0;k1<6;k1++){
			 if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_bins->Fill(k1+0.5,wt);}
		       if(p->ph_maxEcell_E->at(0)/1000.0>5.0 && p->ph_maxEcell_E->at(0)/1000.0<7.0){
                         ph_t_offZ_ematch5_7->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch5_7_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>7.0 && p->ph_maxEcell_E->at(0)/1000.0<10.0){
                         ph_t_offZ_ematch7_10->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch7_10_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>10.0 && p->ph_maxEcell_E->at(0)/1000.0<12.0){
                         ph_t_offZ_ematch10_12->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch10_12_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>12.0 && p->ph_maxEcell_E->at(0)/1000.0<15.0){
                         ph_t_offZ_ematch12_15->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch12_15_bins->Fill(k1+0.5,wt);}
                       }
		       if(!p->ph_isLoose->at(0)){
                         if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 && p->ph_maxEcell_onlId->at(0)!=4057058268516712448 && p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
                           ph_t_notloose->Fill(p->ph_t->at(0),wt);
                           for(int k1=0;k1<6;k1++){
                             if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_notloose_bins->Fill(k1+0.5,wt);}
                         }
                       }
		       if(p->ph_passIso->at(0) && p->ph_isLoose->at(0) && !p->ph_isTight->at(0)){
                         if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 && p->ph_maxEcell_onlId->at(0)!=4057058268516712448 && p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
                           ph_t_loosenottight->Fill(p->ph_t->at(0),wt);
                           for(int k1=0;k1<6;k1++){
                             if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_loosenottight_bins->Fill(k1+0.5,wt);}
                         }
                       }
		     }
		     
		     if(fabs(91.18-result["m_eegamma"])>15.0) continue;
		     ph_t_onZ->Fill(p->ph_t->at(0),wt);
		     ph_pt_onZ->Fill(p->ph_pt->at(0),wt);
		     ph_cellE_onZ->Fill(p->ph_maxEcell_E->at(0)/1000.0,wt);
		     if(fabs(p->ph_t->at(0))>15.0 && fabs(p->ph_t->at(0))<35.0){
		       ph_25ns_pt->Fill(p->ph_pt->at(0),wt);
		       ph_t_maxEcellt_onZ->Fill(p->ph_t->at(0),p->ph_maxEcell_time->at(0),wt);
		     }
		     for(int k1=0;k1<6;k1++){
		       if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_bins->Fill(k1+0.5,wt);}
		     if(p->ph_maxEcell_E->at(0)/1000.0>5.0 && p->ph_maxEcell_E->at(0)/1000.0<7.0){
		       ph_t_onZ_ematch5_7->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch5_7_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>7.0 && p->ph_maxEcell_E->at(0)/1000.0<10.0){
                         ph_t_onZ_ematch7_10->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch7_10_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>10.0 && p->ph_maxEcell_E->at(0)/1000.0<12.0){
                         ph_t_onZ_ematch10_12->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch10_12_bins->Fill(k1+0.5,wt);}
                       }
                       if(p->ph_maxEcell_E->at(0)/1000.0>12.0 && p->ph_maxEcell_E->at(0)/1000.0<15.0){
                         ph_t_onZ_ematch12_15->Fill(p->ph_t->at(0),wt);
                         for(int k1=0;k1<6;k1++){
                           if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch12_15_bins->Fill(k1+0.5,wt);}
                       }	
		       met->Fill(p->m_met,wt);
		       if(p->m_met>=20.0 && p->m_met<=50.0){
			 met_bin->Fill(1.5,wt);}
		       if(p->m_met>50.0){
			 countmet50++;
			 met_bin->Fill(2.5,wt);
			 met50->Fill(p->m_met,wt);}
		       if(p->m_met<20.0){
			 countmet20++;
			 met_bin->Fill(0.5,wt);
			 met20->Fill(p->m_met,wt);}
		     //cout<<"Mass cut"<<endl;
		     //Fill timing distributions
		     timing_distr_bkgyield(p->ph_eta,p->PV_z,p->ph_t,wt);
		     fill_timing_hists(p->ph_maxEcell_gain,p->ph_t,wt);
		     //maxEcell_E variable is in MeV
		     timing_resolution_hists(p->ph_maxEcell_gain,p->ph_E,p->ph_maxEcell_E,p->ph_t,wt);
		     cutflow->Fill(5.5,wt);
		     
		     fill_2D_hists(result,p->ph_pt,1,wt);
		     ph_XPVz_Yphz->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		     if(p->ph_convType->at(0)==1){
		       ph_XPVz_Yphconvz1->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type1->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type1->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==3){
		       ph_XPVz_Yphconvz3->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type3->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type3->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==5){
		       ph_XPVz_Yphconvz5->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type5->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type5->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==1 || p->ph_convType->at(0)==3 || p->ph_convType->at(0)==5){
		       ph_XPVz_Yphconvz->Fill(p->PV_z,p->ph_conv_z->at(0),wt);}
		     //Fill kinematics after
		     fill_kinematics(p->el_pt,p->el_eta,p->el_phi,p->el_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,1,wt);
		     //Fill shower shape variables
                     SS_variables(p->ph_eta,p->PV_z,p->ph_Reta,p->ph_Rphi,p->ph_dEs,p->ph_Rmax2,p->ph_wstot,p->ph_ws3,p->ph_Fside,p->ph_Eratio,p->ph_Rhad,wt);
		     //Fill pointing distributions
		     pointing_distributions(p->ph_eta,p->PV_z,p->ph_calo_z,wt);
		     timing_distributions(p->ph_eta,p->PV_z,p->ph_t,wt);
		   }//numev loop

		 // Histograms need to be written into a TFile
                 TFile f("NPP_EGAM3_hist_output.root","recreate");
		 met50->Write();
		 m_eegamma_25ns->Write();
		 m_ee_eegamma_25ns->Write();
		 m_ee_25ns->Write();
		 met20->Write();
		 met_bin->Write();
		 met->Write();
		 ph_t_onZ->Write();
		 ph_pt_onZ->Write();
                 ph_cellE_onZ->Write();
		 ph_25ns_pt->Write();
		 ph_t_maxEcellt_onZ->Write();
                 ph_t_offZ->Write();
		 ph_t_offZ_ematch5_7->Write();
                 ph_t_onZ_ematch5_7->Write();
		 ph_t_offZ_ematch7_10->Write();
                 ph_t_onZ_ematch7_10->Write();
		 ph_t_offZ_ematch10_12->Write();
                 ph_t_onZ_ematch10_12->Write();
		 ph_t_offZ_ematch12_15->Write();
                 ph_t_onZ_ematch12_15->Write();
		 ph_t_notloose->Write();
                 ph_t_loosenottight->Write();
                 ph_t_notloose_bins->Write();
                 ph_t_loosenottight_bins->Write();
                 ph_t_onZ_bins->Write();
                 ph_t_offZ_bins->Write();
		 ph_t_offZ_ematch5_7_bins->Write();
		 ph_t_onZ_ematch5_7_bins->Write();
		 ph_t_offZ_ematch7_10_bins->Write();
                 ph_t_onZ_ematch7_10_bins->Write();
		 ph_t_offZ_ematch10_12_bins->Write();
                 ph_t_onZ_ematch10_12_bins->Write();
		 ph_t_offZ_ematch12_15_bins->Write();
                 ph_t_onZ_ematch12_15_bins->Write();

		 ph_beforetrig_pt1->Write();
		 ph_beforetrig_eta1->Write();
		 ph_beforetrig_phi1->Write();
		 ph_beforetrig_E1->Write();
		 ph_aftertrig_pt1->Write();
		 ph_aftertrig_eta1->Write();
		 ph_aftertrig_phi1->Write();
		 ph_aftertrig_E1->Write();
		 ph_XPVz_Yphz->Write();
		 ph_XPVz_Yphconvz->Write();
		 ph_XPVz_Yphconvz1->Write();
		 ph_XPVz_Yphconvz3->Write();
		 ph_XPVz_Yphconvz5->Write();
		 ph_XPVz_Yphz_type1->Write();
		 ph_XPVz_Yphz_type3->Write();
		 ph_XPVz_Yphz_type5->Write();
		 ph_Xphconvz_Yphz_type1->Write();
		 ph_Xphconvz_Yphz_type3->Write();
		 ph_Xphconvz_Yphz_type5->Write();
		 trigger_sel->Write();
		 n_e->Write();
		 e_pt1->Write();
		 e_eta1->Write();
		 e_phi1->Write();
		 e_E1->Write();
		 e_pt2->Write();
		 e_eta2->Write();
		 e_phi2->Write();
		 e_E2->Write();
		 n_ph->Write();
		 ph_pt1->Write();
		 ph_eta1->Write();
		 ph_phi1->Write();
		 ph_E1->Write();
		 m_ee->Write();
		 m_eegamma->Write();
		 m_diff->Write();
		 e_sel_pt1->Write();
		 e_sel_eta1->Write();
		 e_sel_phi1->Write();
		 e_sel_E1->Write();
		 e_sel_pt2->Write();
		 e_sel_eta2->Write();
		 e_sel_phi2->Write();
		 e_sel_E2->Write();
		 ph_sel_pt1->Write();
		 ph_sel_eta1->Write();
		 ph_sel_phi1->Write();
		 ph_sel_E1->Write();
		 X_ph_pt_Y_eegamma_minv_sel->Write();
		 X_ph_pt_Y_ee_minv_sel->Write();
		 X_ph_pt_Y_eegamma_minv->Write();
		 X_ph_pt_Y_ee_minv->Write();
		 ph_t_bins->Write();
		 
		 for(int i=0;i<7;i++){
		   for(int j=0;j<3;j++){
		     Reta[i][j]->Write();
		     Rphi[i][j]->Write();
		     dEs[i][j]->Write();
		     Rmax2[i][j]->Write();
		     wstot[i][j]->Write();
		     ws3[i][j]->Write();
		     Fside[i][j]->Write();
		     Eratio[i][j]->Write();
		     Rhad[i][j]->Write();}}
		 for(int i=0;i<11;i++){
		   z_res_reco[i]->Write();}
		 for(int i=0;i<11;i++){
		   t_res_reco[i]->Write();}
		 cutflow->Write();
		 ph_gain->Write();
		 ph_t_gain2->Write();
		 ph_t_gain1->Write();
		 ph_t_gain0->Write();
		 for(int i=0;i<58;i++){
		   for(int j=0;j<3;j++){
		     ph_t_E_gain[i][j]->Write();
		     ph_t_cellE_gain[i][j]->Write();}}
		 ph_cellE->Write();
		 ph_ratio_cellE_E->Write();
		 ph_cellE_gain0->Write();
		 ph_cellE_gain1->Write();
		 ph_cellE_gain2->Write();
		 ph_E_gain0->Write();
		 ph_E_gain1->Write();
		 ph_E_gain2->Write();
		 ph_t_all->Write();
		 ph_t_tight_all->Write();
                 f.Close();


		 return 0 ;
}
