/*
Created by Haichen Wang November 8, 2012
*/

#include "processMumugamma.hh"
#include "MCxsec.hh"

int main (int argc, char **argv)
{
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  cout << endl;
		 if (argc < 2)
		 {
		    printf("\nUsage: %s *.root\n\n",argv[0]);
		    exit(0);
		 }
		 
		 int const index = argc - 1;
		
		 TString path[index];
		 
		 for ( int j = 0; j < argc-1; j++)
		 {
			path[j] = (argv[j+1]);
		 }
		 
		 TChain *chain = new TChain("output");
		 
		 for (int k = 0; k < argc-1 ; k++)
		 {
		    chain -> Add(path[k]); // Add the root file to the TChain chain
		    cout << " adding " << path[k] << endl;
		 }
		 
		 p = new output(chain);		 
		 int numev = p->fChain->GetEntries();
		 p -> fChain ->SetBranchStatus("*",0);

		 p -> fChain ->SetBranchStatus("runNumber",1);
                 p -> fChain ->SetBranchStatus("eventNumber",1);
                 p -> fChain ->SetBranchStatus("m_mcChannelNumber",1);
                 p -> fChain ->SetBranchStatus("m_met",1);
                 p -> fChain ->SetBranchStatus("PV_z",1);
                 p -> fChain ->SetBranchStatus("n_mu",1);
                 p -> fChain ->SetBranchStatus("mu_pt",1);
                 p -> fChain ->SetBranchStatus("mu_eta",1);
                 p -> fChain ->SetBranchStatus("mu_phi",1);
                 p -> fChain ->SetBranchStatus("mu_E",1);
		 p -> fChain ->SetBranchStatus("n_ph",1);
                 p -> fChain ->SetBranchStatus("ph_pt",1);
                 p -> fChain ->SetBranchStatus("ph_eta",1);
                 p -> fChain ->SetBranchStatus("ph_phi",1);
                 p -> fChain ->SetBranchStatus("ph_E",1);
                 p -> fChain ->SetBranchStatus("ph_t",1);
                 p -> fChain ->SetBranchStatus("ph_isTight",1);
                 p -> fChain ->SetBranchStatus("ph_isLoose",1);
                 p -> fChain ->SetBranchStatus("ph_passIso",1);
                 p -> fChain ->SetBranchStatus("ph_topoetcone40",1);
                 p -> fChain ->SetBranchStatus("ph_ptcone20",1);
                 p -> fChain ->SetBranchStatus("ph_isClean",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z_err",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z",1);
                 p -> fChain ->SetBranchStatus("ph_convType",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z_err",1);
                 p -> fChain ->SetBranchStatus("ph_Reta",1);
                 p -> fChain ->SetBranchStatus("ph_Rphi",1);
                 p -> fChain ->SetBranchStatus("ph_dEs",1);
                 p -> fChain ->SetBranchStatus("ph_Rmax2",1);
                 p -> fChain ->SetBranchStatus("ph_wstot",1);
                 p -> fChain ->SetBranchStatus("ph_ws3",1);
                 p -> fChain ->SetBranchStatus("ph_Fside",1);
                 p -> fChain ->SetBranchStatus("ph_Eratio",1);
                 p -> fChain ->SetBranchStatus("ph_Rhad",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_onlId",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_gain",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_E",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_x",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_y",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_z",1);
                 p -> fChain ->SetBranchStatus("ph_maxEcell_time",1);
		 
                 p -> fChain ->SetBranchStatus("HLT_mu26_ivarmedium",1);
                 p -> fChain ->SetBranchStatus("HLT_mu22_mu8noL1",1);
                 p -> fChain ->SetBranchStatus("HLT_mu20_iloose_L1MU15",1);
                 p -> fChain ->SetBranchStatus("HLT_mu24_imedium",1);

		 //p-> || p->

		 cout << "total number of events to be processed " << numev << endl;
		 double lumi = 33.017; //in fb-1
		 double wt_ge = 1.0;
		 get_event(0) ;
                 int DSID = p->m_mcChannelNumber;
                 bool isData = p->m_mcChannelNumber < 0 ;
                 map<string,double> sample = MCxsec(DSID);
                 double xs = sample["xs"];
                 double Nevents = sample["Nevents"];
		 cout<<"DSID="<<DSID<<endl;
		 define_SS_variables(Reta,Rphi,dEs,Rmax2,wstot,ws3,Fside,Eratio,Rhad);
		 define_pointing_variables(z_res_reco);
		 define_timing_variables(t_res_reco);
		 define_timing_res(ph_t_E_gain, ph_t_cellE_gain);
		 int countmet50=0,countmet20=0;
		 double tbins[7]={-4.0,0.5,1.1,1.3,1.5,1.8,4.0};
		 double ptbins[8]={10,15,20,25,30,40,50,1000};
		 double total_onZ[7]={ };
		 double total_offZ[7]={ };

		 double loose_onZ[7]={ }, notloose_onZ[7]={ }, tight_onZ[7]={ }, loosenottight_onZ[7]={ };
		 double loose_offZ[7]={ }, notloose_offZ[7]={ }, tight_offZ[7]={ }, loosenottight_offZ[7]={ };
		 for( int i = 0 ; i < numev ; i ++ )
		 //for( int i = 0 ; i < 1000000 ; i ++ )
		   {
		     if(i>0 && i%500000==0) cout << "Processed " << i << " events..."<<endl;
			
		     get_event(i) ;

		     //Weight calculation
		     double wt=1.0;
		     if(!isData) wt = lumi*xs*wt_ge/Nevents;
		     trigger_sel->Fill(0.5,wt);
		     
		    
		     
		     /*		     if(p->HLT_mu26_ivarmedium==1) trigger_sel->Fill(1.5,wt);
		     if(p->HLT_mu26_imedium==1) trigger_sel->Fill(2.5,wt);
		     if(p->HLT_mu24_ivarmedium==1) trigger_sel->Fill(3.5,wt);
		     if(p->HLT_mu24_imedium==1) trigger_sel->Fill(4.5,wt);
		     if(p->HLT_mu24_ivarloose==1) trigger_sel->Fill(5.5,wt);
		     if(p->HLT_mu50==1) trigger_sel->Fill(6.5,wt);
		     if(p->HLT_mu40==1) trigger_sel->Fill(7.5,wt);
		     if(p->HLT_mu22_mu8noL1==1) trigger_sel->Fill(8.5,wt);
		     if(p->HLT_mu20_mu8noL1==1) trigger_sel->Fill(9.5,wt);
		     if(p->HLT_mu20_2mu4noL1==1) trigger_sel->Fill(10.5,wt);
		     if(p->HLT_3mu6_msonly==1) trigger_sel->Fill(11.5,wt);*/
		     
		     n_mu->Fill((int)p->ph_pt->size(),wt);
		     //Single muon or a dimuon trigger wiht muon pt cuts
		     if((int)p->ph_pt->size()>0){
		       fill_kinematics_photon(p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,0,wt);
		       if(p->HLT_mu26_ivarmedium==1) trig_25ns->Fill(5.5,wt);
		       if(p->HLT_mu24_imedium==1) trig_25ns->Fill(6.5,wt);
		       if(p->HLT_mu22_mu8noL1==1) trig_25ns->Fill(7.5,wt);
		       if(p->HLT_mu20_iloose_L1MU15==1) trig_25ns->Fill(8.5,wt);
		       if(fabs(p->ph_t->at(0))>15.0 && fabs(p->ph_t->at(0))<35.0){
			 if(p->HLT_mu26_ivarmedium==1) trig_25ns->Fill(0.5,wt);
			 if(p->HLT_mu24_imedium==1) trig_25ns->Fill(1.5,wt);
			 if(p->HLT_mu22_mu8noL1==1) trig_25ns->Fill(2.5,wt);
			 if(p->HLT_mu20_iloose_L1MU15==1) trig_25ns->Fill(3.5,wt);}}
		     
		     cutflow->Fill(0.5,wt);
		     int trig=0;
		     if(p->HLT_mu22_mu8noL1==1){
		       if((int)p->mu_pt->size()>1){
			 if(p->mu_pt->at(0) > 23.0 && p->mu_pt->at(1) > 9.0) trig=1;}}
		     else if(p->HLT_mu26_ivarmedium==1 /*|| p->HLT_mu20_iloose_L1MU15*/ || p->HLT_mu24_imedium){
		       if((int)p->mu_pt->size()>1){
			 if(p->mu_pt->at(0) > 27.0 && p->mu_pt->at(1) > 9.0) trig=2;}}
		     if(trig==0) continue;
		     //cout<<"Checkpoint 1"<<endl;
		     //		     if(p->HLT_mu26_ivarmedium==0) continue;
		     if((int)p->mu_pt->size()<2) continue;
		     if(p->mu_pt->at(0) < 27.0 || p->mu_pt->at(1) < 10.0) continue;
		     //cout<<"Checkpoint 2"<<endl;
		     cutflow->Fill(1.5,wt);
		     if((int)p->ph_pt->size()<1) continue;
		     cutflow->Fill(2.5,wt);
		     //cout<<"Checkpoint 3"<<endl;
		     if((int)p->ph_pt->size()>0) fill_kinematics_photon(p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,1,wt);

		     map<string,double> result;
		     //cout<<"Checkpoint 4"<<endl;

		     fill_2D_hists(result,p->ph_pt,0,wt);

		     if(!p->ph_isClean->at(0) || p->ph_pt->at(0) < 10.0 || ( fabs(p->ph_eta->at(0))>1.37 && fabs(p->ph_eta->at(0))<1.52)) continue;
		     if(p->ph_maxEcell_E->at(0)<5.0) continue;
		     result = mumugamma(p->mu_pt,p->mu_eta,p->mu_phi,p->mu_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E);
		     //cout<<"Checkpoint 5"<<endl;

		     cutflow->Fill(3.5,wt);
		     //		     if(p->ph_t->at(0) > -1000) continue;
		     cutflow->Fill(4.5,wt);
		     fill_kinematics(p->mu_pt,p->mu_eta,p->mu_phi,p->mu_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,0,wt);
		     fill_hists_minv(result,wt);
		     
		     //Fill all timing here
		     ph_t_all->Fill(p->ph_t->at(0),wt);
		     if(fabs(p->ph_t->at(0))>15.0 && fabs(p->ph_t->at(0))<35.0){
		       m_mumugamma_25ns->Fill(result["m_mumugamma"],wt);
		       m_mumu_mumugamma_25ns->Fill(result["m_mumu"],result["m_mumugamma"],wt);
		     }
		     
		     if(p->ph_isTight->at(0)) ph_t_alltight->Fill(p->ph_t->at(0),wt);
		     

		     if(fabs(91.18-result["m_mumugamma"])>15.0){
		       ph_t_offZ->Fill(p->ph_t->at(0),wt);
		       //Fill photon id fraction
		       ph_id_offZ->Fill(0.5,wt);
		       if(p->ph_isTight->at(0)) ph_id_offZ->Fill(1.5,wt);
		       if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) ph_id_offZ->Fill(2.5,wt);
		       if(p->ph_isLoose->at(0)) ph_id_offZ->Fill(3.5,wt);
		       if(!p->ph_isLoose->at(0)) ph_id_offZ->Fill(4.5,wt);

		       //Loose efficiency
		       for(int b=0;b<7;b++){
                         if(p->ph_pt->at(0)>ptbins[b] && p->ph_pt->at(0)<ptbins[b+1]){
			   total_offZ[b]+=1.0;
			   if(p->ph_isTight->at(0)) tight_offZ[b]+=1.0;
			   if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) loosenottight_offZ[b]+=1.0;
			   if(p->ph_isLoose->at(0)) loose_offZ[b]+=1.0;
			   if(!p->ph_isLoose->at(0)) notloose_offZ[b]+=1.0;}}
			   
		       //Isolated
		       if(p->ph_passIso->at(0)){
			 ph_id_offZ->Fill(5.5,wt);
			 if(p->ph_isTight->at(0)) ph_id_offZ->Fill(6.5,wt);
			 if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) ph_id_offZ->Fill(7.5,wt);
			 if(p->ph_isLoose->at(0)) ph_id_offZ->Fill(8.5,wt);
			 if(!p->ph_isLoose->at(0)) ph_id_offZ->Fill(9.5,wt);}

		       for(int k1=0;k1<6;k1++){
                         if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_bins->Fill(k1+0.5,wt);}
		       if(p->ph_maxEcell_E->at(0)/1000.0>5.0 && p->ph_maxEcell_E->at(0)/1000.0<7.0){
			 ph_t_offZ_ematch5_7->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch5_7_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>7.0 && p->ph_maxEcell_E->at(0)/1000.0<10.0){
			 ph_t_offZ_ematch7_10->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch7_10_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>10.0 && p->ph_maxEcell_E->at(0)/1000.0<12.0){
			 ph_t_offZ_ematch10_12->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch10_12_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>12.0 && p->ph_maxEcell_E->at(0)/1000.0<15.0){
			 ph_t_offZ_ematch12_15->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch12_15_bins->Fill(k1+0.5,wt);}
		       }
		       ph_t_offZ_iso->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       ph_t_offZ_Rhad->Fill(p->ph_Rhad->at(0),wt);
		       if(p->ph_Rhad->at(0)<0.1) ph_t_offZ_iso_Rhadl0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<0.3) ph_t_offZ_iso_Rhadl0p3->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<0.5) ph_t_offZ_iso_Rhadl0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<0.8) ph_t_offZ_iso_Rhadl0p8->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<1.0) ph_t_offZ_iso_Rhadl1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<5.0) ph_t_offZ_iso_Rhadl5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       if(p->ph_Rhad->at(0)<10.0) ph_t_offZ_iso_Rhadl10->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		       

		       if(!p->ph_isLoose->at(0)){
			 if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 && p->ph_maxEcell_onlId->at(0)!=4057058268516712448 && p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
			   ph_t_notloose->Fill(p->ph_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_notloose_bins->Fill(k1+0.5,wt);}
			   ph_t_notloose_iso->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   ph_t_notloose_trackiso->Fill(p->ph_ptcone20->at(0)/1000.0,wt);
			   ph_t_notloose_Rhad->Fill(p->ph_Rhad->at(0),wt);


			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.01) ph_t_notloose_iso_tisol0p01->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.1) ph_t_notloose_iso_tisol0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.5) ph_t_notloose_iso_tisol0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<1.0) ph_t_notloose_iso_tisol1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);


			   if(p->ph_Rhad->at(0)<0.001) ph_t_notloose_iso_Rhadl0p001->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.0001) ph_t_notloose_iso_Rhadl0p0001->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.002) ph_t_notloose_iso_Rhadl0p002->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.005) ph_t_notloose_iso_Rhadl0p005->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.008) ph_t_notloose_iso_Rhadl0p008->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);      
			   if(p->ph_Rhad->at(0)<0.01) ph_t_notloose_iso_Rhadl0p01->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.1) ph_t_notloose_iso_Rhadl0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.5) ph_t_notloose_iso_Rhadl0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<1.0) ph_t_notloose_iso_Rhadl1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			 }
		       }
		       if(p->ph_passIso->at(0) && p->ph_isLoose->at(0) && !p->ph_isTight->at(0)){
			 if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 && p->ph_maxEcell_onlId->at(0)!=4057058268516712448 && p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
			   ph_t_loosenottight->Fill(p->ph_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_loosenottight_bins->Fill(k1+0.5,wt);}
			   ph_t_loosenottight_iso->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   ph_t_loosenottight_Rhad->Fill(p->ph_Rhad->at(0),wt);
			   ph_t_loosenottight_trackiso->Fill(p->ph_ptcone20->at(0)/1000.0,wt);

			   
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.01) ph_t_loosenottight_iso_tisol0p01->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.1) ph_t_loosenottight_iso_tisol0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<0.5) ph_t_loosenottight_iso_tisol0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_ptcone20->at(0)*0.001/p->ph_pt->at(0)<1.0) ph_t_loosenottight_iso_tisol1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);


			   if(p->ph_Rhad->at(0)<0.0001) ph_t_loosenottight_iso_Rhadl0p0001->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.001) ph_t_loosenottight_iso_Rhadl0p001->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.002) ph_t_loosenottight_iso_Rhadl0p002->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.005) ph_t_loosenottight_iso_Rhadl0p005->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.008) ph_t_loosenottight_iso_Rhadl0p008->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.01) ph_t_loosenottight_iso_Rhadl0p01->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.1) ph_t_loosenottight_iso_Rhadl0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<0.5) ph_t_loosenottight_iso_Rhadl0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			   if(p->ph_Rhad->at(0)<1.0) ph_t_loosenottight_iso_Rhadl1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
			 }
		       }
		     }
		     
		     if(fabs(91.18-result["m_mumugamma"])>15.0) continue;
		     //Fill photon id fraction
		     ph_id_onZ->Fill(0.5,wt);
		     if(p->ph_isTight->at(0)) ph_id_onZ->Fill(1.5,wt);
		     if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) ph_id_onZ->Fill(2.5,wt);
		     if(p->ph_isLoose->at(0)) ph_id_onZ->Fill(3.5,wt);
		     if(!p->ph_isLoose->at(0)) ph_id_onZ->Fill(4.5,wt);

		     //Loose efficiency
		     for(int b=0;b<7;b++){
		       if(p->ph_pt->at(0)>ptbins[b] && p->ph_pt->at(0)<ptbins[b+1]){
			 total_onZ[b]+=1.0;
			 if(p->ph_isTight->at(0)) tight_onZ[b]+=1.0;
			 if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) loosenottight_onZ[b]+=1.0;
			 if(p->ph_isLoose->at(0)) loose_onZ[b]+=1.0;
			 if(!p->ph_isLoose->at(0)) notloose_onZ[b]+=1.0;}}


		     //Isolated
		     if(p->ph_passIso->at(0)){
		       ph_id_onZ->Fill(5.5,wt);
		       if(p->ph_isTight->at(0)) ph_id_onZ->Fill(6.5,wt);
		       if(p->ph_isLoose->at(0) && !p->ph_isTight->at(0)) ph_id_onZ->Fill(7.5,wt);
		       if(p->ph_isLoose->at(0)) ph_id_onZ->Fill(8.5,wt);
		       if(!p->ph_isLoose->at(0)) ph_id_onZ->Fill(9.5,wt);}
		     
		     ph_t_onZ_iso->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     ph_t_onZ_Rhad->Fill(p->ph_Rhad->at(0),wt);
		     if(p->ph_Rhad->at(0)<0.1) ph_t_onZ_iso_Rhadl0p1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<0.3) ph_t_onZ_iso_Rhadl0p3->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<0.5) ph_t_onZ_iso_Rhadl0p5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<0.8) ph_t_onZ_iso_Rhadl0p8->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<1.0) ph_t_onZ_iso_Rhadl1->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<5.0) ph_t_onZ_iso_Rhadl5->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);
		     if(p->ph_Rhad->at(0)<10.0) ph_t_onZ_iso_Rhadl10->Fill(p->ph_topoetcone40->at(0)/1000.0,wt);


		     if(!p->ph_isLoose->at(0))ph_t_onZfake_Rhad->Fill(p->ph_Rhad->at(0),wt);
		     if(p->ph_maxEcell_onlId->at(0)!=4185790189408354304 && p->ph_maxEcell_onlId->at(0)!=4057048372912062464 && p->ph_maxEcell_onlId->at(0)!=4057050571935318016 && p->ph_maxEcell_onlId->at(0)!=4057042875353923584 &&p->ph_maxEcell_onlId->at(0)!=4057058268516712448 &&   p->ph_maxEcell_onlId->at(0)!=4057065965098106880 && p->ph_maxEcell_onlId->at(0)!=4057060467539968000 && p->ph_maxEcell_onlId->at(0)!=4057040676330668032 && p->ph_maxEcell_onlId->at(0)!=4057068164121362432 && p->ph_maxEcell_onlId->at(0)!=4054704214121644032){
		       if(fabs(p->ph_maxEcell_time->at(0))>15.0 && fabs(p->ph_maxEcell_time->at(0))<35.0){
			 ph_maxEcell25ns_eta_phi->Fill(p->ph_eta->at(0),p->ph_phi->at(0),wt);
			 ph_maxEcell25ns_onlId->Fill(p->ph_maxEcell_onlId->at(0)/1000000000000000,wt);}
		       if(fabs(p->ph_t->at(0))>15.0 && fabs(p->ph_t->at(0))<35.0){
			 ph_25ns_eta_phi->Fill(p->ph_eta->at(0),p->ph_phi->at(0),wt);
			 ph_t_maxEcellt_onZ->Fill(p->ph_t->at(0),p->ph_maxEcell_time->at(0),wt);
			 if(fabs(p->ph_eta->at(0))>1.37 && fabs(p->ph_eta->at(0))<1.52) cout<< "Photon in crack region"<<endl;
			 ph_25ns_onlId->Fill(p->ph_maxEcell_onlId->at(0)/1000000000000000,wt);
			 ph_25ns_mueta->Fill(p->mu_eta->at(0),wt);
			 ph_25ns_pt->Fill(p->ph_pt->at(0),wt);
			 ph_25ns_mmumugamma->Fill(result["m_mumugamma"],wt);
			 //			 cout<<"ph_onlId= "<<p->ph_maxEcell_onlId->at(0)<<endl;
		       }
		       ph_t_onZ->Fill(p->ph_t->at(0),wt);
		       ph_pt_onZ->Fill(p->ph_pt->at(0),wt);
		       ph_cellE_onZ->Fill(p->ph_maxEcell_E->at(0)/1000.0,wt);
		       for(int k1=0;k1<6;k1++){
                         if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_bins->Fill(k1+0.5,wt);}

		       if(p->ph_maxEcell_E->at(0)/1000.0>5.0 && p->ph_maxEcell_E->at(0)/1000.0<7.0){
			 ph_t_onZ_ematch5_7->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch5_7_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>7.0 && p->ph_maxEcell_E->at(0)/1000.0<10.0){
			 ph_t_onZ_ematch7_10->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch7_10_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>10.0 && p->ph_maxEcell_E->at(0)/1000.0<12.0){
			 ph_t_onZ_ematch10_12->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch10_12_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->ph_maxEcell_E->at(0)/1000.0>12.0 && p->ph_maxEcell_E->at(0)/1000.0<15.0){
			 ph_t_onZ_ematch12_15->Fill(p->ph_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->ph_t->at(0)>tbins[k1] && p->ph_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch12_15_bins->Fill(k1+0.5,wt);}
		       }
		       ph_maxEcellt_onZ->Fill(p->ph_maxEcell_time->at(0),wt);
		     }
		     timing_distr_bkgyield(p->ph_eta,p->PV_z,p->ph_t,wt);
		     met->Fill(p->m_met,wt);
		     
		     if(p->m_met>=20.0 && p->m_met<=50.0){
		       met_bin->Fill(1.5,wt);}

		     if(p->m_met>50.0){
                       countmet50++;
		       met_bin->Fill(2.5,wt);
                       met50->Fill(p->m_met,wt);}
                     if(p->m_met<20.0){
                       countmet20++;
		       met_bin->Fill(0.5,wt);
                       met20->Fill(p->m_met,wt);}

		     //Fill timing distributions
		     //cout<<"Checkpoint 6"<<endl;
		     
		     fill_timing_hists(p->ph_maxEcell_gain,p->ph_t,wt);
		     //maxEcell_E variable is in MeV
		     timing_resolution_hists(p->ph_maxEcell_gain,p->ph_E,p->ph_maxEcell_E,p->ph_t,wt);

		     cutflow->Fill(5.5,wt);

		     fill_2D_hists(result,p->ph_pt,1,wt);
		     //cout<<"Checkpoint 7"<<endl;

		     ph_XPVz_Yphz->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		     if(p->ph_convType->at(0)==1){
		       ph_XPVz_Yphconvz1->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type1->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type1->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==3){
		       ph_XPVz_Yphconvz3->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type3->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type3->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==5){
		       ph_XPVz_Yphconvz5->Fill(p->PV_z,p->ph_conv_z->at(0),wt);
		       ph_XPVz_Yphz_type5->Fill(p->PV_z,p->ph_calo_z->at(0),wt);
		       ph_Xphconvz_Yphz_type5->Fill(p->ph_conv_z->at(0),p->ph_calo_z->at(0),wt);}
		     if(p->ph_convType->at(0)==1 || p->ph_convType->at(0)==3 || p->ph_convType->at(0)==5){
		       ph_XPVz_Yphconvz->Fill(p->PV_z,p->ph_conv_z->at(0),wt);}
		     //cout<<"Checkpoint 8"<<endl;

		     //Fill kinematics after
		     fill_kinematics(p->mu_pt,p->mu_eta,p->mu_phi,p->mu_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,1,wt);
		     //Fill shower shape variables
                     SS_variables(p->ph_eta,p->PV_z,p->ph_Reta,p->ph_Rphi,p->ph_dEs,p->ph_Rmax2,p->ph_wstot,p->ph_ws3,p->ph_Fside,p->ph_Eratio,p->ph_Rhad,wt);
		     //cout<<"Checkpoint 9"<<endl;

		     //Fill pointing distributions
		     pointing_distributions(p->ph_eta,p->PV_z,p->ph_calo_z,wt);
		     timing_distributions(p->ph_eta,p->PV_z,p->ph_t,wt);
		     //cout<<"Checkpoint 10"<<endl;

		   }//numev loop
		 
		 for(int i=0;i<7;i++){
		   ph_ideff_loose_onZ->SetBinContent(i+1,loose_onZ[i]/total_onZ[i]);
		   ph_ideff_loose_onZ->SetBinError(i+1,(loose_onZ[i]/total_onZ[i])*( sqrt(1.0/loose_onZ[i]) + sqrt(1.0/total_onZ[i]) ));
		   ph_ideff_notloose_onZ->SetBinContent(i+1,notloose_onZ[i]/total_onZ[i]);
		   ph_ideff_notloose_onZ->SetBinError(i+1,(notloose_onZ[i]/total_onZ[i])*( sqrt(1.0/notloose_onZ[i]) + sqrt(1.0/total_onZ[i]) ));
		   ph_ideff_loosenottight_onZ->SetBinContent(i+1,loosenottight_onZ[i]/total_onZ[i]);
		   ph_ideff_loosenottight_onZ->SetBinError(i+1,(loosenottight_onZ[i]/total_onZ[i])*( sqrt(1.0/loosenottight_onZ[i]) + sqrt(1.0/total_onZ[i]) ));
		   ph_ideff_tight_onZ->SetBinContent(i+1,tight_onZ[i]/total_onZ[i]);
		   ph_ideff_tight_onZ->SetBinError(i+1,(tight_onZ[i]/total_onZ[i])*( sqrt(1.0/tight_onZ[i]) + sqrt(1.0/total_onZ[i]) ));

		   ph_ideff_loose_offZ->SetBinContent(i+1,loose_offZ[i]/total_offZ[i]);
		   ph_ideff_loose_offZ->SetBinError(i+1,(loose_offZ[i]/total_offZ[i])*( sqrt(1.0/loose_offZ[i]) + sqrt(1.0/total_offZ[i]) ));
		   ph_ideff_notloose_offZ->SetBinContent(i+1,notloose_offZ[i]/total_offZ[i]);
		   ph_ideff_notloose_offZ->SetBinError(i+1,(notloose_offZ[i]/total_offZ[i])*( sqrt(1.0/notloose_offZ[i]) + sqrt(1.0/total_offZ[i]) ));
		   ph_ideff_loosenottight_offZ->SetBinContent(i+1,loosenottight_offZ[i]/total_offZ[i]);
		   ph_ideff_loosenottight_offZ->SetBinError(i+1,(loosenottight_offZ[i]/total_offZ[i])*( sqrt(1.0/loosenottight_offZ[i]) + sqrt(1.0/total_offZ[i]) ));
		   ph_ideff_tight_offZ->SetBinContent(i+1,tight_offZ[i]/total_offZ[i]);
		   ph_ideff_tight_offZ->SetBinError(i+1,(tight_offZ[i]/total_offZ[i])*( sqrt(1.0/tight_offZ[i]) + sqrt(1.0/total_offZ[i]) ));

		   
	
		 }

		 // Histograms need to be written into a TFile
                 TFile f("NPP_EGAM4_hist_output.root","recreate");
		 trig_25ns->Write();
		 ph_t_all->Write();
		 ph_t_alltight->Write();
		 m_mumugamma_25ns->Write();
		 m_mumu_mumugamma_25ns->Write();
		 met_bin->Write();
		 met50->Write();
                 met20->Write();
		 ph_ideff_loose_onZ->Write();
		 ph_ideff_loose_offZ->Write();
		 ph_ideff_tight_onZ->Write();
		 ph_ideff_tight_offZ->Write();
		 ph_ideff_loosenottight_onZ->Write();
		 ph_ideff_loosenottight_offZ->Write();
		 ph_ideff_notloose_onZ->Write();
		 ph_ideff_notloose_offZ->Write();
		 

		 ph_id_offZ->Write();
		 ph_id_onZ->Write();		 
		 ph_t_onZ->Write();
		 ph_pt_onZ->Write();
		 ph_cellE_onZ->Write();
		 
		 ph_t_offZ->Write();
		 ph_t_onZ_iso->Write();
		 ph_t_offZ_iso->Write();
		 ph_t_onZ_Rhad->Write();
		 ph_t_onZfake_Rhad->Write();
		 ph_t_offZ_Rhad->Write();
		 ph_t_loosenottight_iso->Write();
		 ph_t_notloose_iso->Write();
		 ph_t_loosenottight_Rhad->Write();
       		 ph_t_notloose_trackiso->Write();
		 ph_t_loosenottight_trackiso->Write();
		 ph_t_notloose_Rhad->Write();
		 ph_t_loosenottight_iso_Rhadl0p0001->Write();
		 ph_t_loosenottight_iso_Rhadl0p001->Write();
		 ph_t_loosenottight_iso_Rhadl0p002->Write();
		 ph_t_loosenottight_iso_Rhadl0p005->Write();
		 ph_t_loosenottight_iso_Rhadl0p008->Write();
		 ph_t_loosenottight_iso_Rhadl0p01->Write();
		 ph_t_loosenottight_iso_Rhadl0p1->Write();
		 ph_t_loosenottight_iso_Rhadl0p5->Write();
		 ph_t_loosenottight_iso_Rhadl1->Write();
		 
		 ph_t_loosenottight_iso_tisol0p01->Write();
		 ph_t_loosenottight_iso_tisol0p1->Write();
		 ph_t_loosenottight_iso_tisol0p5->Write();
		 ph_t_loosenottight_iso_tisol1->Write();
		 ph_t_notloose_iso_tisol0p01->Write();
		 ph_t_notloose_iso_tisol0p1->Write();
		 ph_t_notloose_iso_tisol0p5->Write();
		 ph_t_notloose_iso_tisol1->Write();

		 ph_t_notloose_iso_Rhadl0p0001->Write();
		 ph_t_notloose_iso_Rhadl0p001->Write();
		 ph_t_notloose_iso_Rhadl0p002->Write();
		 ph_t_notloose_iso_Rhadl0p005->Write();
		 ph_t_notloose_iso_Rhadl0p008->Write();

		 ph_t_notloose_iso_Rhadl0p01->Write();
		 ph_t_notloose_iso_Rhadl0p1->Write();
		 ph_t_notloose_iso_Rhadl0p5->Write();
		 ph_t_notloose_iso_Rhadl1->Write();
		 ph_t_offZ_iso_Rhadl0p1->Write();
		 ph_t_offZ_iso_Rhadl0p3->Write();
		 ph_t_offZ_iso_Rhadl0p5->Write();
		 ph_t_offZ_iso_Rhadl0p8->Write();
		 ph_t_offZ_iso_Rhadl1->Write();
		 ph_t_offZ_iso_Rhadl5->Write();
		 ph_t_offZ_iso_Rhadl10->Write();
		 ph_t_onZ_iso_Rhadl0p1->Write();
		 ph_t_onZ_iso_Rhadl0p3->Write();
		 ph_t_onZ_iso_Rhadl0p5->Write();
		 ph_t_onZ_iso_Rhadl0p8->Write();
		 ph_t_onZ_iso_Rhadl1->Write();
		 ph_t_onZ_iso_Rhadl5->Write();
		 ph_t_onZ_iso_Rhadl10->Write();

		 ph_t_offZ_ematch5_7->Write();
                 ph_t_onZ_ematch5_7->Write();
                 ph_t_offZ_ematch7_10->Write();
                 ph_t_onZ_ematch7_10->Write();
                 ph_t_offZ_ematch10_12->Write();
                 ph_t_onZ_ematch10_12->Write();
                 ph_t_offZ_ematch12_15->Write();
                 ph_t_onZ_ematch12_15->Write();
		 ph_25ns_eta_phi->Write();
		 ph_25ns_mueta->Write();
		 ph_25ns_onlId->Write();
		 ph_25ns_pt->Write();
		 ph_25ns_mmumugamma->Write();
		 ph_t_offZ_ematch5_7_bins->Write();
                 ph_t_onZ_ematch5_7_bins->Write();
                 ph_t_offZ_ematch7_10_bins->Write();
                 ph_t_onZ_ematch7_10_bins->Write();
                 ph_t_offZ_ematch10_12_bins->Write();
                 ph_t_onZ_ematch10_12_bins->Write();
                 ph_t_offZ_ematch12_15_bins->Write();
                 ph_t_onZ_ematch12_15_bins->Write();
		 ph_t_notloose->Write();
		 ph_t_loosenottight->Write();
		 ph_t_notloose_bins->Write();
		 ph_t_loosenottight_bins->Write();
		 ph_t_onZ_bins->Write();
		 ph_t_offZ_bins->Write();
		 
		 ph_maxEcellt_onZ->Write();
		 ph_t_maxEcellt_onZ->Write();
		 ph_maxEcell25ns_eta_phi->Write();
		 ph_maxEcell25ns_onlId->Write();
		 ph_t_notloose->Write();
		 ph_t_loosenottight->Write();
		 ph_beforetrig_pt1->Write();
		 ph_beforetrig_eta1->Write();
		 ph_beforetrig_phi1->Write();
		 ph_beforetrig_E1->Write();
		 ph_aftertrig_pt1->Write();
		 ph_aftertrig_eta1->Write();
		 ph_aftertrig_phi1->Write();
		 ph_aftertrig_E1->Write();
		 ph_XPVz_Yphz->Write();
		 ph_XPVz_Yphconvz->Write();
		 ph_XPVz_Yphconvz1->Write();
		 ph_XPVz_Yphconvz3->Write();
		 ph_XPVz_Yphconvz5->Write();
		 ph_XPVz_Yphz_type1->Write();
		 ph_XPVz_Yphz_type3->Write();
		 ph_XPVz_Yphz_type5->Write();
		 ph_Xphconvz_Yphz_type1->Write();
		 ph_Xphconvz_Yphz_type3->Write();
		 ph_Xphconvz_Yphz_type5->Write();
		 trigger_sel->Write();
		 met->Write();
		 n_mu->Write();
		 mu_pt1->Write();
		 mu_eta1->Write();
		 mu_phi1->Write();
		 mu_E1->Write();
		 mu_pt2->Write();
		 mu_eta2->Write();
		 mu_phi2->Write();
		 mu_E2->Write();
		 n_ph->Write();
		 ph_pt1->Write();
		 ph_eta1->Write();
		 ph_phi1->Write();
		 ph_E1->Write();
		 m_mumu->Write();
		 m_mumugamma->Write();
		 m_diff->Write();
		 mu_sel_pt1->Write();
		 mu_sel_eta1->Write();
		 mu_sel_phi1->Write();
		 mu_sel_E1->Write();
		 mu_sel_pt2->Write();
		 mu_sel_eta2->Write();
		 mu_sel_phi2->Write();
		 mu_sel_E2->Write();
		 ph_sel_pt1->Write();
		 ph_sel_eta1->Write();
		 ph_sel_phi1->Write();
		 ph_sel_E1->Write();
		 X_ph_pt_Y_mumugamma_minv_sel->Write();
		 X_ph_pt_Y_mumu_minv_sel->Write();
		 X_ph_pt_Y_mumugamma_minv->Write();
		 X_ph_pt_Y_mumu_minv->Write();
		 ph_t_bins->Write();

		 for(int i=0;i<7;i++){
		   for(int j=0;j<3;j++){
		     Reta[i][j]->Write();
		     Rphi[i][j]->Write();
		     dEs[i][j]->Write();
		     Rmax2[i][j]->Write();
		     wstot[i][j]->Write();
		     ws3[i][j]->Write();
		     Fside[i][j]->Write();
		     Eratio[i][j]->Write();
		     Rhad[i][j]->Write();}}
		 for(int i=0;i<11;i++){
		   z_res_reco[i]->Write();}
		 for(int i=0;i<11;i++){
		   t_res_reco[i]->Write();}
		 cutflow->Write();
		 ph_gain->Write();
		 ph_t_gain2->Write();
		 ph_t_gain1->Write();
		 ph_t_gain0->Write();
		 for(int i=0;i<58;i++){
		   for(int j=0;j<3;j++){
		     ph_t_E_gain[i][j]->Write();
		     ph_t_cellE_gain[i][j]->Write();}}
		 ph_cellE->Write();
		 ph_ratio_cellE_E->Write();
		 ph_cellE_gain0->Write();
		 ph_cellE_gain1->Write();
		 ph_cellE_gain2->Write();
		 ph_E_gain0->Write();
		 ph_E_gain1->Write();
		 ph_E_gain2->Write();
		 
                 f.Close();


		 return 0 ;
}
