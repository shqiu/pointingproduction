map<string,double>MCxsec(int DSID ){

  double xs = -9 , Nevents = -9;
  //Cross section in femtobarn
  //if( DSID == 364505 ) { xs =  60933.2  ; Nevents = 4000000.0   ; }
  //if( DSID == 364506 ) { xs =  35220.9  ; Nevents = 2956000.0   ; }
  //if( DSID == 364507 ) { xs =  6167.22  ; Nevents = 1000000.0   ; }
  //if( DSID == 364508 ) { xs =  543.268  ; Nevents =  498500.0  ; }
  //if( DSID == 364509 ) { xs =  63.4562  ; Nevents =  100000.0  ; }

//Nevents from DAODs are
//364505 : 3999932
//365506 : 2955957
//365507 : 999990
//365508 : 498498
//365509 : 100000

//cross-section from logfiles (generator level) are (in fb)
//364505 : 60933.2
//365506 : 35220.9
//365507 : 6167.22
//365508 : 543.268
//365509 : 63.4562

//cross-section from ami page (in fb)
if( DSID == 364505 ) { xs =  57701.0  ; Nevents = 4000000.0   ; }
if( DSID == 364506 ) { xs =  34588.0  ; Nevents = 2956000.0   ; }
if( DSID == 364507 ) { xs =  6285.3  ; Nevents = 1000000.0   ; }
if( DSID == 364508 ) { xs =  493.920  ; Nevents =  498500.0  ; }
if( DSID == 364509 ) { xs =  63.08  ; Nevents =  100000.0  ; }

 map<string,double> xsec;
 xsec["xs"] = xs;
 xsec["Nevents"] = Nevents;

 return xsec;
}
