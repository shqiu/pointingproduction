#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TROOT.h"
#include "THStack.h"
#include "TString.h"
#include "TH1.h"
#include "TH2F.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooPlot.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooAbsData.h"
#include "RooAbsRealLValue.h"
#include "RooAbsPdf.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooDataHist.h"
#include "RooNLLVar.h"
#include "RooSimultaneous.h"
#include "RooExponential.h"
#include "RooGlobalFunc.h"
#include "RooCBShape.h"
#include "RooFormula.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "../aux/output.hh"
#include "header.hh"
using namespace std;
using namespace RooFit;
using std::vector;
#include <sys/stat.h>


output *p;

//Declare all histograms                              
TH1F* trigger_sel = new TH1F("trigger_sel","trigger_sel",15,0,15);
TH1F* ph_beforetrig_pt1 = new TH1F("ph_beforetrig_pt1","ph_beforetrig_pt1",1000,0,1000);                                       
TH1F* ph_beforetrig_eta1 = new TH1F("ph_beforetrig_eta1","ph_beforetrig_eta1",100,-2.5,2.5);                                  
TH1F* ph_beforetrig_phi1 = new TH1F("ph_beforetrig_phi1","ph_beforetrig_phi1",100,-3.5,3.5);                                  
TH1F* ph_beforetrig_E1 = new TH1F("ph_beforetrig_E1","ph_beforetrig_E1",1000,0,1000);                                          
TH1F* ph_aftertrig_pt1 = new TH1F("ph_aftertrig_pt1","ph_aftertrig_pt1",1000,0,1000);                                       
TH1F* ph_aftertrig_eta1 = new TH1F("ph_aftertrig_eta1","ph_aftertrig_eta1",100,-2.5,2.5);                                  
TH1F* ph_aftertrig_phi1 = new TH1F("ph_aftertrig_phi1","ph_aftertrig_phi1",100,-3.5,3.5);                                  
TH1F* ph_aftertrig_E1 = new TH1F("ph_aftertrig_E1","ph_aftertrig_E1",1000,0,1000);                                          


TH1F* mu_pt1 = new TH1F("mu_pt1","mu_pt1",1000,0,1000);                                       
TH1F* met = new TH1F("met","met",1000,0,1000);                                       
TH1F* met_bin = new TH1F("met_bin","met_bin",3,0,3);                                       
TH1F* mu_eta1 = new TH1F("mu_eta1","mu_eta1",100,-2.5,2.5);                                  
TH1F* mu_phi1 = new TH1F("mu_phi1","mu_phi1",100,-3.5,3.5);                                  
TH1F* mu_E1 = new TH1F("mu_E1","mu_E1",100,0,1000);                                          
TH1F* mu_pt2 = new TH1F("mu_pt2","mu_pt2",1000,0,1000);                                       
TH1F* mu_eta2 = new TH1F("mu_eta2","mu_eta2",100,-2.5,2.5);                                  
TH1F* mu_phi2 = new TH1F("mu_phi2","mu_phi2",100,-3.5,3.5);                                  
TH1F* mu_E2 = new TH1F("mu_E2","mu_E2",1000,0,1000);                                          
TH1F* ph_pt1 = new TH1F("ph_pt1","ph_pt1",100,0,1000);                                       
TH1F* ph_eta1 = new TH1F("ph_eta1","ph_eta1",100,-2.5,2.5);                                  
TH1F* ph_phi1 = new TH1F("ph_phi1","ph_phi1",100,-3.5,3.5);                                  
TH1F* ph_E1 = new TH1F("ph_E1","ph_E1",1000,0,1000);                                          
TH1F* met50 = new TH1F("met50","met50",1000,0,1000);
TH1F* met20 = new TH1F("met20","met20",1000,0,1000);

TH2F* ph_XPVz_Yphz = new TH2F("ph_XPVz_Yphz","ph_XPVz_Yphz",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphconvz = new TH2F("ph_XPVz_Yphconvz","ph_XPVz_Yphconvz",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphconvz1 = new TH2F("ph_XPVz_Yphconvz1","ph_XPVz_Yphconvz1",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphconvz3 = new TH2F("ph_XPVz_Yphconvz3","ph_XPVz_Yphconvz3",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphconvz5 = new TH2F("ph_XPVz_Yphconvz5","ph_XPVz_Yphconvz5",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphz_type1 = new TH2F("ph_XPVz_Yphz_type1","ph_XPVz_Yphz_type1",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphz_type3 = new TH2F("ph_XPVz_Yphz_type3","ph_XPVz_Yphz_type3",500,-150,150,500,-150,150);
TH2F* ph_XPVz_Yphz_type5 = new TH2F("ph_XPVz_Yphz_type5","ph_XPVz_Yphz_type5",500,-150,150,500,-150,150);
TH2F* ph_Xphconvz_Yphz_type1 = new TH2F("ph_Xphconvz_Yphz_type1","ph_XPVz_Yphconvz_type1",500,-150,150,500,-150,150);
TH2F* ph_Xphconvz_Yphz_type3 = new TH2F("ph_Xphconvz_Yphz_type3","ph_XPVz_Yphconvz_type3",500,-150,150,500,-150,150);
TH2F* ph_Xphconvz_Yphz_type5 = new TH2F("ph_Xphconvz_Yphz_type5","ph_XPVz_Yphconvz_type5",500,-150,150,500,-150,150);

TH1F* mu_sel_pt1 = new TH1F("mu_sel_pt1","mu_sel_pt1",100,0,1000);                                       
TH1F* mu_sel_eta1 = new TH1F("mu_sel_eta1","mu_sel_eta1",100,-2.5,2.5);                                  
TH1F* mu_sel_phi1 = new TH1F("mu_sel_phi1","mu_sel_phi1",100,-3.5,3.5);                                  
TH1F* mu_sel_E1 = new TH1F("mu_sel_E1","mu_sel_E1",100,0,1000);                                          
TH1F* mu_sel_pt2 = new TH1F("mu_sel_pt2","mu_sel_pt2",100,0,1000);                                       
TH1F* mu_sel_eta2 = new TH1F("mu_sel_eta2","mu_sel_eta2",100,-2.5,2.5);                                  
TH1F* mu_sel_phi2 = new TH1F("mu_sel_phi2","mu_sel_phi2",100,-3.5,3.5);                                  
TH1F* mu_sel_E2 = new TH1F("mu_sel_E2","mu_sel_E2",100,0,1000);                                          
TH1F* ph_sel_pt1 = new TH1F("ph_sel_pt1","ph_sel_pt1",1000,0,1000);                                       
TH1F* ph_sel_eta1 = new TH1F("ph_sel_eta1","ph_sel_eta1",100,-2.5,2.5);                                  
TH1F* ph_sel_phi1 = new TH1F("ph_sel_phi1","ph_sel_phi1",100,-3.5,3.5);                                  
TH1F* ph_sel_E1 = new TH1F("ph_sel_E1","ph_sel_E1",1000,0,1000);                                          

TH1F* m_mumugamma = new TH1F("m_mumugamma","m_mumugamma",1000,0,1000);                       
TH1F* m_mumu = new TH1F("m_mumu","m_mumu",1000,0,1000);
TH1F* m_diff = new TH1F("m_diff","m_diff",1000,-1000,1000);

TH2F* X_ph_pt_Y_mumugamma_minv_sel = new TH2F("X_ph_pt_Y_mumugamma_minv_sel","X_ph_pt_Y_mumugamma_minv_sel",1000,0,1000,1000,0,1000);
TH2F* X_ph_pt_Y_mumu_minv_sel = new TH2F("X_ph_pt_Y_mumu_minv_sel","X_ph_pt_Y_mumu_minv_sel",1000,0,1000,1000,0,1000);

TH2F* X_ph_pt_Y_mumugamma_minv = new TH2F("X_ph_pt_Y_mumugamma_minv","X_ph_pt_Y_mumugamma_minv",1000,0,1000,1000,0,1000);
TH2F* X_ph_pt_Y_mumu_minv = new TH2F("X_ph_pt_Y_mumu_minv","X_ph_pt_Y_mumu_minv",1000,0,1000,1000,0,1000);

TH1F* n_mu = new TH1F("n_mu","n_mu",10,0,10);                                                
TH1F* n_ph = new TH1F("n_ph","n_ph",10,0,10);
TH2F* ph_25ns_eta_phi = new TH2F("ph_25ns_eta_phi","ph_25ns_eta_phi",500,-5.0,5.0,100,-3.5,3.5);
TH1D* ph_25ns_onlId = new TH1D("ph_25ns_onlId","ph_25ns_onlId",2300,2500,4800);
TH1D* ph_25ns_mmumugamma = new TH1D("ph_25ns_mmumugamma","ph_25ns_mmumugamma",100,0,500);
TH1D* ph_25ns_pt = new TH1D("ph_25ns_pt","ph_25ns_pt",250,0,500);
TH1D* ph_25ns_mueta = new TH1D("ph_25ns_mueta","ph_25ns_mueta",100,-5,5);
TH1F* ph_t_onZ = new TH1F("ph_t_onZ","ph_t_onZ",2000,-100,100);
TH1F* ph_pt_onZ = new TH1F("ph_pt_onZ","ph_pt_onZ",200,0,1000);
TH1F* ph_cellE_onZ = new TH1F("ph_cellE_onZ","ph_cellE_onZ",200,0,1000);

TH1F* ph_t_all = new TH1F("ph_t_all","ph_t_all",5000,-100,100);
TH1F* ph_t_alltight = new TH1F("ph_t_alltight","ph_t_alltight",5000,-100,100);
TH1F* m_mumugamma_25ns = new TH1F("m_mumugamma_25ns","m_mumugamma_25ns",2000,0,500);
TH2F* m_mumu_mumugamma_25ns = new TH2F("m_mumu_mumugamma_25ns","m_mumu_mumugamma_25ns",200,0,200,200,0,200);

TH2F* ph_maxEcell25ns_eta_phi = new TH2F("ph_maxEcell25ns_eta_phi","ph_maxEcell25ns_eta_phi",100,-5.0,5.0,100,-3.5,3.5);
TH1D* ph_maxEcell25ns_onlId = new TH1D("ph_maxEcell25ns_onlId","ph_maxEcell25ns_onlId",2300,2500,4800);
TH1F* ph_maxEcellt_onZ = new TH1F("ph_maxEcellt_onZ","ph_maxEcellt_onZ",5000,-100,100);
TH2F* ph_t_maxEcellt_onZ = new TH2F("ph_t_maxEcellt_onZ","ph_t_maxEcellt_onZ",200,-40,40,200,-40,40);

TH1F* ph_t_offZ = new TH1F("ph_t_offZ","ph_t_offZ",5000,-100,100);
TH1F* ph_t_onZ_ematch5_7 = new TH1F("ph_t_onZ_ematch5_7","ph_t_onZ_ematch5_7",5000,-100,100);
TH1F* ph_t_offZ_ematch5_7 = new TH1F("ph_t_offZ_ematch5_7","ph_t_offZ_ematch5_7",5000,-100,100);
TH1F* ph_t_onZ_ematch7_10 = new TH1F("ph_t_onZ_ematch7_10","ph_t_onZ_ematch7_10",5000,-100,100);
TH1F* ph_t_offZ_ematch7_10 = new TH1F("ph_t_offZ_ematch7_10","ph_t_offZ_ematch7_10",5000,-100,100);
TH1F* ph_t_onZ_ematch10_12 = new TH1F("ph_t_onZ_ematch10_12","ph_t_onZ_ematch10_12",5000,-100,100);
TH1F* ph_t_offZ_ematch10_12 = new TH1F("ph_t_offZ_ematch10_12","ph_t_offZ_ematch10_12",5000,-100,100);
TH1F* ph_t_onZ_ematch12_15 = new TH1F("ph_t_onZ_ematch12_15","ph_t_onZ_ematch12_15",5000,-100,100);
TH1F* ph_t_offZ_ematch12_15 = new TH1F("ph_t_offZ_ematch12_15","ph_t_offZ_ematch12_15",5000,-100,100);
TH1F* ph_t_onZ_bins = new TH1F("ph_t_onZ_bins","ph_t_onZ_bins",6,0,6);
TH1F* ph_t_offZ_bins = new TH1F("ph_t_offZ_bins","ph_t_offZ_bins",6,0,6);
TH1F* ph_t_onZ_ematch5_7_bins = new TH1F("ph_t_onZ_ematch5_7_bins","ph_t_onZ_ematch5_7_bins",6,0,6);
TH1F* ph_t_offZ_ematch5_7_bins = new TH1F("ph_t_offZ_ematch5_7_bins","ph_t_offZ_ematch5_7_bins",6,0,6);
TH1F* ph_t_onZ_ematch7_10_bins = new TH1F("ph_t_onZ_ematch7_10_bins","ph_t_onZ_ematch7_10_bins",6,0,6);
TH1F* ph_t_offZ_ematch7_10_bins = new TH1F("ph_t_offZ_ematch7_10_bins","ph_t_offZ_ematch7_10_bins",6,0,6);
TH1F* ph_t_onZ_ematch10_12_bins = new TH1F("ph_t_onZ_ematch10_12_bins","ph_t_onZ_ematch10_12_bins",6,0,6);
TH1F* ph_t_offZ_ematch10_12_bins = new TH1F("ph_t_offZ_ematch10_12_bins","ph_t_offZ_ematch10_12_bins",6,0,6);
TH1F* ph_t_onZ_ematch12_15_bins = new TH1F("ph_t_onZ_ematch12_15_bins","ph_t_onZ_ematch12_15_bins",6,0,6);
TH1F* ph_t_offZ_ematch12_15_bins = new TH1F("ph_t_offZ_ematch12_15_bins","ph_t_offZ_ematch12_15_bins",6,0,6);
TH1F* ph_t_notloose = new TH1F("ph_t_notloose","ph_t_notloose",5000,-100,100);
TH1F* ph_t_loosenottight = new TH1F("ph_t_loosenottight","ph_t_loosenottight",5000,-100,100);
TH1F* ph_t_notloose_bins = new TH1F("ph_t_notloose_bins","ph_t_notloose_bins",6,0,6);
TH1F* ph_t_loosenottight_bins = new TH1F("ph_t_loosenottight_bins","ph_t_loosenottight_bins",6,0,6);

TH1F* ph_t_offZ_iso = new TH1F("ph_t_offZ_iso","ph_t_offZ_iso",500,-1000,1000.0);
TH1F* ph_t_loosenottight_iso = new TH1F("ph_t_loosenottight_iso","ph_t_loosenottight_iso",1000,-100,100);
TH1F* ph_t_notloose_iso = new TH1F("ph_t_notloose_iso","ph_t_notloose_iso",1000,-100,100);
TH1F* ph_t_loosenottight_Rhad = new TH1F("ph_t_loosenottight_Rhad","ph_t_loosenottight_Rhad",500,-10,10.0);
TH1F* ph_t_notloose_Rhad = new TH1F("ph_t_notloose_Rhad","ph_t_notloose_Rhad",500,-10,10.0);
TH1F* ph_t_loosenottight_trackiso = new TH1F("ph_t_loosenottight_trackiso","ph_t_loosenottight_trackiso",1000,-100,100);
TH1F* ph_t_notloose_trackiso = new TH1F("ph_t_notloose_trackiso","ph_t_notloose_trackiso",1000,-100,100);

TH1F* ph_t_loosenottight_iso_tisol0p1 = new TH1F("ph_t_loosenottight_iso_tisol0p1","ph_t_loosenottight_iso_tisol0p1",1000,-100,100);
TH1F* ph_t_notloose_iso_tisol0p1 = new TH1F("ph_t_notloose_iso_tisol0p1","ph_t_notloose_iso_tisol0p1",1000,-100,100);
TH1F* ph_t_loosenottight_iso_tisol0p5 = new TH1F("ph_t_loosenottight_iso_tisol0p5","ph_t_loosenottight_iso_tisol0p5",1000,-100,100);
TH1F* ph_t_notloose_iso_tisol0p5 = new TH1F("ph_t_notloose_iso_tisol0p5","ph_t_notloose_iso_tisol0p5",1000,-100,100);
TH1F* ph_t_loosenottight_iso_tisol1 = new TH1F("ph_t_loosenottight_iso_tisol1","ph_t_loosenottight_iso_tisol1",1000,-100,100);
TH1F* ph_t_notloose_iso_tisol1 = new TH1F("ph_t_notloose_iso_tisol1","ph_t_notloose_iso_tisol1",1000,-100,100);
TH1F* ph_t_loosenottight_iso_tisol0p01 = new TH1F("ph_t_loosenottight_iso_tisol0p01","ph_t_loosenottight_iso_tisol0p01",1000,-100,100);
TH1F* ph_t_notloose_iso_tisol0p01 = new TH1F("ph_t_notloose_iso_tisol0p01","ph_t_notloose_iso_tisol0p01",1000,-100,100);


TH1F* ph_t_loosenottight_iso_Rhadl0p1 = new TH1F("ph_t_loosenottight_iso_Rhadl0p1","ph_t_loosenottight_iso_Rhadl0p1",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p1 = new TH1F("ph_t_notloose_iso_Rhadl0p1","ph_t_notloose_iso_Rhadl0p1",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p5 = new TH1F("ph_t_loosenottight_iso_Rhadl0p5","ph_t_loosenottight_iso_Rhadl0p5",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p5 = new TH1F("ph_t_notloose_iso_Rhadl0p5","ph_t_notloose_iso_Rhadl0p5",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl1 = new TH1F("ph_t_loosenottight_iso_Rhadl1","ph_t_loosenottight_iso_Rhadl1",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl1 = new TH1F("ph_t_notloose_iso_Rhadl1","ph_t_notloose_iso_Rhadl1",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p01 = new TH1F("ph_t_loosenottight_iso_Rhadl0p01","ph_t_loosenottight_iso_Rhadl0p01",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p01 = new TH1F("ph_t_notloose_iso_Rhadl0p01","ph_t_notloose_iso_Rhadl0p01",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p008 = new TH1F("ph_t_loosenottight_iso_Rhadl0p008","ph_t_loosenottight_iso_Rhadl0p008",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p008 = new TH1F("ph_t_notloose_iso_Rhadl0p008","ph_t_notloose_iso_Rhadl0p008",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p005 = new TH1F("ph_t_loosenottight_iso_Rhadl0p005","ph_t_loosenottight_iso_Rhadl0p005",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p005 = new TH1F("ph_t_notloose_iso_Rhadl0p005","ph_t_notloose_iso_Rhadl0p005",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p002 = new TH1F("ph_t_loosenottight_iso_Rhadl0p002","ph_t_loosenottight_iso_Rhadl0p002",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p002 = new TH1F("ph_t_notloose_iso_Rhadl0p002","ph_t_notloose_iso_Rhadl0p002",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p001 = new TH1F("ph_t_loosenottight_iso_Rhadl0p001","ph_t_loosenottight_iso_Rhadl0p001",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p001 = new TH1F("ph_t_notloose_iso_Rhadl0p001","ph_t_notloose_iso_Rhadl0p001",1000,-100,100);
TH1F* ph_t_loosenottight_iso_Rhadl0p0001 = new TH1F("ph_t_loosenottight_iso_Rhadl0p0001","ph_t_loosenottight_iso_Rhadl0p0001",1000,-100,100);
TH1F* ph_t_notloose_iso_Rhadl0p0001 = new TH1F("ph_t_notloose_iso_Rhadl0p0001","ph_t_notloose_iso_Rhadl0p0001",1000,-100,100);


TH1F* ph_t_offZ_iso_Rhadl0p1 = new TH1F("ph_t_offZ_iso_Rhadl0p1","ph_t_offZ_iso_Rhadl0p1",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl0p3 = new TH1F("ph_t_offZ_iso_Rhadl0p3","ph_t_offZ_iso_Rhadl0p3",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl0p5 = new TH1F("ph_t_offZ_iso_Rhadl0p5","ph_t_offZ_iso_Rhadl0p5",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl0p8 = new TH1F("ph_t_offZ_iso_Rhadl0p8","ph_t_offZ_iso_Rhadl0p8",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl1 = new TH1F("ph_t_offZ_iso_Rhadl1","ph_t_offZ_iso_Rhadl1",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl5 = new TH1F("ph_t_offZ_iso_Rhadl5","ph_t_offZ_iso_Rhadl5",1000,-100,100);
TH1F* ph_t_offZ_iso_Rhadl10 = new TH1F("ph_t_offZ_iso_Rhadl10","ph_t_offZ_iso_Rhadl10",1000,-100,100);
TH1F* ph_t_onZfake_Rhad = new TH1F("ph_t_onZfake_Rhad","ph_t_onZfake_Rhad",1000,-10,10);
TH1F* ph_t_offZ_Rhad = new TH1F("ph_t_offZ_Rhad","ph_t_offZ_Rhad",1000,-10,10);
TH1F* ph_t_onZ_iso = new TH1F("ph_t_onZ_iso","ph_t_onZ_iso",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl0p1 = new TH1F("ph_t_onZ_iso_Rhadl0p1","ph_t_onZ_iso_Rhadl0p1",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl0p3 = new TH1F("ph_t_onZ_iso_Rhadl0p3","ph_t_onZ_iso_Rhadl0p3",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl0p5 = new TH1F("ph_t_onZ_iso_Rhadl0p5","ph_t_onZ_iso_Rhadl0p5",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl0p8 = new TH1F("ph_t_onZ_iso_Rhadl0p8","ph_t_onZ_iso_Rhadl0p8",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl1 = new TH1F("ph_t_onZ_iso_Rhadl1","ph_t_onZ_iso_Rhadl1",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl5 = new TH1F("ph_t_onZ_iso_Rhadl5","ph_t_onZ_iso_Rhadl5",1000,-100,100);
TH1F* ph_t_onZ_iso_Rhadl10 = new TH1F("ph_t_onZ_iso_Rhadl10","ph_t_onZ_iso_Rhadl10",1000,-100,100);
TH1F* ph_t_onZ_Rhad = new TH1F("ph_t_onZ_Rhad","ph_t_onZ_Rhad",1000,-10,10);

//Timing variables
TH1F* ph_t_gain2 = new TH1F("ph_t_gain2","ph_t_gain2",2000,-20,20);
TH1F* ph_t_gain1 = new TH1F("ph_t_gain1","ph_t_gain1",2000,-20,20);
TH1F* ph_t_gain0 = new TH1F("ph_t_gain0","ph_t_gain0",2000,-20,20);
TH1F* ph_gain = new TH1F("ph_gain","ph_gain",4,0,4);
TH1F* cutflow = new TH1F("cutflow","cutflow",10,0,10);
TH1F* trig_25ns = new TH1F("trig_25ns","trig_25ns",10,0,10);
TH1F* ph_t_E_gain[58][3];
TH1F* ph_t_cellE_gain[58][3];
TH1F* ph_cellE = new TH1F("ph_cellE","ph_cellE",1000,0,1000);
TH1F* ph_ratio_cellE_E = new TH1F("ph_ratio_cellE_E","ph_ratio_cellE_E",2000,0,10);
TH1F* ph_cellE_gain0 = new TH1F("ph_cellE_gain0","ph_cellE_gain0",1000,0,1000);
TH1F* ph_cellE_gain1 = new TH1F("ph_cellE_gain1","ph_cellE_gain1",1000,0,1000);
TH1F* ph_cellE_gain2 = new TH1F("ph_cellE_gain2","ph_cellE_gain2",1000,0,1000);
TH1F* ph_E_gain0 = new TH1F("ph_E_gain0","ph_E_gain0",1000,0,1000);
TH1F* ph_E_gain1 = new TH1F("ph_E_gain1","ph_E_gain1",1000,0,1000);
TH1F* ph_E_gain2 = new TH1F("ph_E_gain2","ph_E_gain2",1000,0,1000);
TH1F* ph_id_onZ = new TH1F("ph_id_onZ","ph_id_onZ",10,0,10);
TH1F* ph_id_offZ = new TH1F("ph_id_offZ","ph_id_offZ",10,0,10);
TH1F* ph_ideff_loose_onZ = new TH1F("ph_ideff_loose_onZ","ph_ideff_loose_onZ",10,0,10);
TH1F* ph_ideff_loose_offZ = new TH1F("ph_ideff_loose_offZ","ph_ideff_loose_offZ",10,0,10);
TH1F* ph_ideff_notloose_onZ = new TH1F("ph_ideff_notloose_onZ","ph_ideff_notloose_onZ",10,0,10);
TH1F* ph_ideff_notloose_offZ = new TH1F("ph_ideff_notloose_offZ","ph_ideff_notloose_offZ",10,0,10);
TH1F* ph_ideff_tight_onZ = new TH1F("ph_ideff_tight_onZ","ph_ideff_tight_onZ",10,0,10);
TH1F* ph_ideff_tight_offZ = new TH1F("ph_ideff_tight_offZ","ph_ideff_tight_offZ",10,0,10);
TH1F* ph_ideff_loosenottight_onZ = new TH1F("ph_ideff_loosenottight_onZ","ph_ideff_loosenottight_onZ",10,0,10);
TH1F* ph_ideff_loosenottight_offZ = new TH1F("ph_ideff_loosenottight_offZ","ph_ideff_loosenottight_offZ",10,0,10);


//Showershape variables
TH1F* Reta[7][3]; TH1F* Rphi[7][3]; TH1F* dEs[7][3];
TH1F* Rmax2[7][3]; TH1F* wstot[7][3]; TH1F* ws3[7][3];
TH1F* Fside[7][3]; TH1F* Rhad[7][3]; TH1F* Eratio[7][3];
TH1F* ph_t_bins = new TH1F("ph_t_bins","ph_t_bins",6,0,6);
//Pointing distributions
TH1F* z_res_reco[11];
TH1F* t_res_reco[11];
   
void get_event(int i) {
  if ( p->LoadTree(i) < 0) { 
    cout<<"\nProblem in LoadTree."
        <<"\nEntry: "<<i<<endl;
    exit(0);
  }
  p->fChain->GetEntry(i);
}



map<string,double> mumugamma(vector<double>* pt1, vector<double>* eta1,vector<double>* phi1, vector<double>* E1,vector<double>* pt2, vector<double>* eta2,vector<double>* phi2, vector<double>* E2)
{
  map<string,double> result;
  TLorentzVector mu1,mu2,ph1;
  mu1.SetPtEtaPhiE(pt1->at(0),eta1->at(0),phi1->at(0),E1->at(0));
  mu2.SetPtEtaPhiE(pt1->at(1),eta1->at(1),phi1->at(1),E1->at(1));
  ph1.SetPtEtaPhiE(pt2->at(0),eta2->at(0),phi2->at(0),E2->at(0));

  result["m_mumu"] = (mu1+mu2).M();
  result["m_mumugamma"] = (mu1+mu2+ph1).M();
  result["m_diff"] = (double)(((mu1+mu2).M()) - ((mu1+mu2+ph1).M()));
  return result;
}

void fill_kinematics(vector<double>* pt1, vector<double>* eta1,vector<double>* phi1, vector<double>* E1,vector<double>* pt2, vector<double>* eta2,vector<double>* phi2, vector<double>* E2,int selection,double wt1)
{
  TLorentzVector mu1,mu2,ph1;
  mu1.SetPtEtaPhiE(pt1->at(0),eta1->at(0),phi1->at(0),E1->at(0));
  mu2.SetPtEtaPhiE(pt1->at(1),eta1->at(1),phi1->at(1),E1->at(1));
  ph1.SetPtEtaPhiE(pt2->at(0),eta2->at(0),phi2->at(0),E2->at(0));

  if(selection==0){
    mu_pt1->Fill(mu1.Pt(),wt1); mu_eta1->Fill(mu1.Eta(),wt1);
    mu_phi1->Fill(mu1.Phi(),wt1); mu_E1->Fill(mu1.E(),wt1);
    mu_pt2->Fill(mu2.Pt(),wt1); mu_eta2->Fill(mu2.Eta(),wt1);
    mu_phi2->Fill(mu2.Phi(),wt1); mu_E2->Fill(mu2.E(),wt1);
    ph_pt1->Fill(ph1.Pt(),wt1); ph_eta1->Fill(ph1.Eta(),wt1);
    ph_phi1->Fill(ph1.Phi(),wt1); ph_E1->Fill(ph1.E(),wt1);}
  else{
    mu_sel_pt1->Fill(mu1.Pt(),wt1); mu_sel_eta1->Fill(mu1.Eta(),wt1);
    mu_sel_phi1->Fill(mu1.Phi(),wt1); mu_sel_E1->Fill(mu1.E(),wt1);
    mu_sel_pt2->Fill(mu2.Pt(),wt1); mu_sel_eta2->Fill(mu2.Eta(),wt1);
    mu_sel_phi2->Fill(mu2.Phi(),wt1); mu_sel_E2->Fill(mu2.E(),wt1);
    ph_sel_pt1->Fill(ph1.Pt(),wt1); ph_sel_eta1->Fill(ph1.Eta(),wt1);
    ph_sel_phi1->Fill(ph1.Phi(),wt1); ph_sel_E1->Fill(ph1.E(),wt1);}
}

void timing_distr_bkgyield(vector<double>* eta, double PV_z, vector<double>* timing,double wt1)
{
  double time = timing->at(0);
  double t[7] = { -4, 0.5 , 1.1, 1.3, 1.5, 1.8, 4.0};
  //barrel only
  if(fabs(eta->at(0))<1.37){
    for( int k = 0 ; k < 6 ; k ++ ){
      if(time>= t[k] &&  time < t[k+1]){
	ph_t_bins -> Fill( k+0.5,wt1); }}}
}

void fill_kinematics_photon(vector<double>* pt2,vector<double>* eta2,vector<double>* phi2, vector<double>* E2,int selection,double wt1)
{
  TLorentzVector ph1;
  ph1.SetPtEtaPhiE(pt2->at(0),eta2->at(0),phi2->at(0),E2->at(0));

  if(selection==0){
    ph_beforetrig_pt1->Fill(ph1.Pt(),wt1); ph_beforetrig_eta1->Fill(ph1.Eta(),wt1);
    ph_beforetrig_phi1->Fill(ph1.Phi(),wt1); ph_beforetrig_E1->Fill(ph1.E(),wt1);}
  else{
    ph_aftertrig_pt1->Fill(ph1.Pt(),wt1); ph_aftertrig_eta1->Fill(ph1.Eta(),wt1);
    ph_aftertrig_phi1->Fill(ph1.Phi(),wt1); ph_aftertrig_E1->Fill(ph1.E(),wt1);}
}


void fill_hists_minv(map<string,double> value,double wt1)
{
  m_mumu->Fill(value["m_mumu"],wt1);
  m_mumugamma->Fill(value["m_mumugamma"],wt1);
  m_diff->Fill(value["m_diff"],wt1);
  
}

void fill_timing_hists(vector<int>* gain, vector<double>* time, double wt1)
{
  ph_gain->Fill(gain->at(0),wt1);
  //high gain : 0
  if(gain->at(0)==0) ph_t_gain0->Fill(time->at(0),wt1);
  //med gain : 1
  if(gain->at(0)==1) ph_t_gain1->Fill(time->at(0),wt1);
  //low gain : 2
  if(gain->at(0)==2) ph_t_gain2->Fill(time->at(0),wt1);
}

void define_SS_variables(TH1F *Reta1[7][3],TH1F *Rphi1[7][3],TH1F *dEs1[7][3],TH1F *Rmax21[7][3],TH1F *wstot1[7][3],TH1F *ws31[7][3],TH1F *Fside1[7][3],TH1F *Eratio1[7][3],TH1F *Rhad1[7][3])
{
  for(int i=0; i<7; i++){                                                                      
    char name0[10];                                                                            
    sprintf(name0,"%d",i);                                                                     
    TString hname0 = "Reta_";TString hname1 = "Rphi_";                                         
    TString hname2 = "dEs_";TString hname3 = "Rmax2_";                                         
    TString hname4 = "wstot_";TString hname5 = "ws3_";                                         
    TString hname6 = "Fside_";TString hname7 = "Eratio_";                                      
    TString hname8 = "Rhad_";                                                                  
    hname0.Append(name0);hname1.Append(name0);hname2.Append(name0);                            
    hname3.Append(name0);hname4.Append(name0);hname5.Append(name0);                            
    hname6.Append(name0);hname7.Append(name0);hname8.Append(name0);                            
    for(int j=0; j<3; j++){                                                                    
      char name1[10];                                                                          
      TString hname_new0 = hname0;TString hname_new1 = hname1;                                 
      TString hname_new2 = hname2;TString hname_new3 = hname3;                                 
      TString hname_new4 = hname4;TString hname_new5 = hname5;                                 
      TString hname_new6 = hname6;TString hname_new7 = hname7;                                 
      TString hname_new8 = hname8;                                                             
      sprintf(name1,"_%d",j);                                                                  
      hname_new0.Append(name1);hname_new1.Append(name1);hname_new2.Append(name1);              
      hname_new3.Append(name1);hname_new4.Append(name1);hname_new5.Append(name1);              
      hname_new6.Append(name1);hname_new7.Append(name1);hname_new8.Append(name1);              
      Reta1[i][j]= new TH1F(hname_new0,hname_new0,150,0,1.5);                                   
      Rphi1[i][j]= new TH1F(hname_new1,hname_new1,150,0,1.5);                                   
      dEs1[i][j]= new TH1F(hname_new2,hname_new2,100,-1,1);                                     
      Rmax21[i][j]= new TH1F(hname_new3,hname_new3,100,0,1);                                    
      wstot1[i][j]= new TH1F(hname_new4,hname_new4,100,0,10);                                   
      ws31[i][j]= new TH1F(hname_new5,hname_new5,100,0,10);                                     
      Fside1[i][j]= new TH1F(hname_new6,hname_new6,100,0,1);                                    
      Eratio1[i][j]= new TH1F(hname_new7,hname_new7,150,0,1.5);                                 
      Rhad1[i][j]= new TH1F(hname_new8,hname_new8,1000,-1,1);}}
}


void define_pointing_variables(TH1F *z_res_reco[11])
{
  for(int i=0; i<11; i++){                                                                      
    char name0[10];                                                                            
    sprintf(name0,"%d",i);                                                                     
    TString hname0 = "z_res_reco_";                                                            
    hname0.Append(name0);                            
    z_res_reco[i]= new TH1F(hname0,hname0,1000,-2000,2000);}
}

void define_timing_res(TH1F *ph_t_E_gain[58][3], TH1F *ph_t_cellE_gain[58][3])
{
  for(int i=0; i<58; i++){
    char name0[10];
    sprintf(name0,"%d",i);
    TString hname0 = "ph_t_E_gain_";
    TString hname1 = "ph_t_cellE_gain_";
    hname0.Append(name0);
    hname1.Append(name0);
    for(int j=0; j<3; j++){
      char name1[10];
      TString hname_new0 = hname0;
      TString hname_new1 = hname1;
      sprintf(name1,"_%d",j); 
      hname_new0.Append(name1);
      hname_new1.Append(name1);
      ph_t_E_gain[i][j]= new TH1F(hname_new0,hname_new0,2000,-10,10);
      ph_t_cellE_gain[i][j]= new TH1F(hname_new1,hname_new1,2000,-10,10);}}
}


void define_timing_variables(TH1F *t_res_reco[11])
{
  for(int i=0; i<11; i++){                                                                      
    char name0[10];                                                                            
    sprintf(name0,"%d",i);                                                                     
    TString hname0 = "t_res_reco_";                                                            
    hname0.Append(name0);                            
    t_res_reco[i]= new TH1F(hname0,hname0,1000,-1000,1000);}
}


void SS_variables(vector<double>* eta,double PV_z, vector<double>* Reta1,vector<double>* Rphi1, vector<double>* dEs1,vector<double>* Rmax21, vector<double>* wstot1, vector<double>* ws31, vector<double>* FSide1, vector<double>* Eratio1, vector<double>* Rhad1,double wt1)
{

  for(int i=0; i<(int)eta->size();i++){
    int count1=0, count2=0;
    //Inclusive in both PV_z and barrel/endcap
    Reta[count2][0] -> Fill(Reta1->at(i),wt1);
    Rphi[count2][0] -> Fill(Rphi1->at(i),wt1);
    dEs[count2][0] -> Fill(dEs1->at(i),wt1);
    Rmax2[count2][0] -> Fill(Rmax21->at(i),wt1);
    wstot[count2][0] -> Fill(wstot1->at(i),wt1);
    ws3[count2][0] -> Fill(ws31->at(i),wt1);
    Fside[count2][0] -> Fill(FSide1->at(i),wt1);
    Eratio[count2][0] -> Fill(Eratio1->at(i),wt1);
    Rhad[count2][0] -> Fill(Rhad1->at(i),wt1);
    //Barrel or endcap
    if(fabs(eta->at(i))<1.37) count1=1;
    if(fabs(eta->at(i))>1.52 && fabs(eta->at(i))<2.37) count1=2;
    if(count1==0) continue;
    //Inclusive in PV_z but divide in barrel/endcap
    Reta[count2][count1] -> Fill(Reta1->at(i),wt1);
    Rphi[count2][count1] -> Fill(Rphi1->at(i),wt1);
    dEs[count2][count1] -> Fill(dEs1->at(i),wt1);
    Rmax2[count2][count1] -> Fill(Rmax21->at(i),wt1);
    wstot[count2][count1] -> Fill(wstot1->at(i),wt1);
    ws3[count2][count1] -> Fill(ws31->at(i),wt1);
    Fside[count2][count1] -> Fill(FSide1->at(i),wt1);
    Eratio[count2][count1] -> Fill(Eratio1->at(i),wt1);
    Rhad[count2][count1] -> Fill(Rhad1->at(i),wt1);
    //Bin in PV_z (proxy for pointing in data)
    if(fabs(PV_z)<=25.0) count2=1;
    if(fabs(PV_z)>25.0 && fabs(PV_z)<=50.0) count2=2;
    if(fabs(PV_z)>50.0 && fabs(PV_z)<=75.0) count2=3;
    if(fabs(PV_z)>75.0 && fabs(PV_z)<=100.0) count2=4;
    if(fabs(PV_z)>100.0 && fabs(PV_z)<=125.0) count2=5;
    if(fabs(PV_z)>125.0 && fabs(PV_z)<=150.0) count2=6;
    if(count2==0) continue;
    //Bin in PV_z but inclusive in barrel/end-cap
    Reta[count2][0] -> Fill(Reta1->at(i),wt1);
    Rphi[count2][0] -> Fill(Rphi1->at(i),wt1);
    dEs[count2][0] -> Fill(dEs1->at(i),wt1);
    Rmax2[count2][0] -> Fill(Rmax21->at(i),wt1);
    wstot[count2][0] -> Fill(wstot1->at(i),wt1);
    ws3[count2][0] -> Fill(ws31->at(i),wt1);
    Fside[count2][0] -> Fill(FSide1->at(i),wt1);
    Eratio[count2][0] -> Fill(Eratio1->at(i),wt1);
    Rhad[count2][0] -> Fill(Rhad1->at(i),wt1);
    //Fill remaining histograms binned in PV_z and barrel/end-cap
    Reta[count2][count1] -> Fill(Reta1->at(i),wt1);
    Rphi[count2][count1] -> Fill(Rphi1->at(i),wt1);
    dEs[count2][count1] -> Fill(dEs1->at(i),wt1);
    Rmax2[count2][count1] -> Fill(Rmax21->at(i),wt1);
    wstot[count2][count1] -> Fill(wstot1->at(i),wt1);
    ws3[count2][count1] -> Fill(ws31->at(i),wt1);
    Fside[count2][count1] -> Fill(FSide1->at(i),wt1);
    Eratio[count2][count1] -> Fill(Eratio1->at(i),wt1);
    Rhad[count2][count1] -> Fill(Rhad1->at(i),wt1);}
}

void pointing_distributions(vector<double>* eta, double PV_z, vector<double>* zcalo,double wt1)
{
  for(int i=0;i<(int)eta->size();i++){
    //barrel only
    if(fabs(eta->at(i))>1.37) continue;
    double zdca1 = zcalo->at(i) - PV_z;
    //Fill distributions inclusive in PV_z
    z_res_reco[0]->Fill(zdca1,wt1);
    int count1=0;
    if(fabs(PV_z)<=20) count1=1;
    if(fabs(PV_z)>20 && fabs(PV_z)<=40) count1=2;
    if(fabs(PV_z)>40 && fabs(PV_z)<=60) count1=3;
    if(fabs(PV_z)>60 && fabs(PV_z)<=80) count1=4;
    if(fabs(PV_z)>80 && fabs(PV_z)<=100) count1=5;
    if(fabs(PV_z)>100 && fabs(PV_z)<=120) count1=6;
    if(fabs(PV_z)>120 && fabs(PV_z)<=140) count1=7;
    if(fabs(PV_z)>140 && fabs(PV_z)<=160) count1=8;
    if(fabs(PV_z)>160 && fabs(PV_z)<=180) count1=9;
    if(fabs(PV_z)>180 && fabs(PV_z)<=200) count1=10;
    if(count1==0) continue;
    z_res_reco[count1]->Fill(zdca1,wt1);}
}

void timing_distributions(vector<double>* eta, double PV_z, vector<double>* timing,double wt1)
{
  for(int i=0;i<(int)eta->size();i++){
    //barrel only
    if(fabs(eta->at(i))>1.37) continue;
    double time = timing->at(i);
    //Fill distributions inclusive in PV_z
    t_res_reco[0]->Fill(time,wt1);
    int count1=0;
    if(fabs(PV_z)<=20) count1=1;
    if(fabs(PV_z)>20 && fabs(PV_z)<=40) count1=2;
    if(fabs(PV_z)>40 && fabs(PV_z)<=60) count1=3;
    if(fabs(PV_z)>60 && fabs(PV_z)<=80) count1=4;
    if(fabs(PV_z)>80 && fabs(PV_z)<=100) count1=5;
    if(fabs(PV_z)>100 && fabs(PV_z)<=120) count1=6;
    if(fabs(PV_z)>120 && fabs(PV_z)<=140) count1=7;
    if(fabs(PV_z)>140 && fabs(PV_z)<=160) count1=8;
    if(fabs(PV_z)>160 && fabs(PV_z)<=180) count1=9;
    if(fabs(PV_z)>180 && fabs(PV_z)<=200) count1=10;
    if(count1==0) continue;
    t_res_reco[count1]->Fill(time,wt1);}
}

void fill_2D_hists(map<string,double> value,vector<double>* ph_pt1,int selection,double wt1)
{

  if(selection==0){
    X_ph_pt_Y_mumugamma_minv_sel->Fill(ph_pt1->at(0),value["m_mumugamma"],wt1);
    X_ph_pt_Y_mumu_minv_sel->Fill(ph_pt1->at(0),value["m_mumu"],wt1);}
  else{
    X_ph_pt_Y_mumugamma_minv->Fill(ph_pt1->at(0),value["m_mumugamma"],wt1);
    X_ph_pt_Y_mumu_minv->Fill(ph_pt1->at(0),value["m_mumu"],wt1);}
  
}

void timing_resolution_hists(vector<int>* gain, vector<double>* energy, vector<double>* cellE,vector<double>* time, double wt1)
{
  double cell_energy=cellE->at(0)/1000.0;
  ph_cellE->Fill(cell_energy,wt1);
  ph_ratio_cellE_E->Fill(cell_energy/energy->at(0),wt1);
  if(gain->at(0)==0){
    ph_cellE_gain0->Fill(cell_energy,wt1);
    ph_E_gain0->Fill(energy->at(0),wt1);}
  if(gain->at(0)==1){
    ph_cellE_gain1->Fill(cell_energy,wt1);
    ph_E_gain1->Fill(energy->at(0),wt1);}
  if(gain->at(0)==2){
    ph_cellE_gain2->Fill(cell_energy,wt1);
    ph_E_gain2->Fill(energy->at(0),wt1);}
  
  int quotient, quotient1;
  if(energy->at(0)<20.0) quotient=(int)(energy->at(0)/1.0);
  if(energy->at(0)>=20.0 && energy->at(0)<50.0) quotient=((int)((energy->at(0)-20.0)/2.0))+20;
  if(energy->at(0)>=50.0 && energy->at(0)<100.0) quotient=((int)((energy->at(0)-50.0)/5.0))+35;
  if(energy->at(0)>=100.0 && energy->at(0)<200.0) quotient=((int)((energy->at(0)-100.0)/20.0))+45;
  if(energy->at(0)>=200.0 && energy->at(0)<500.0) quotient=((int)((energy->at(0)-200.0)/50.0))+50;
  if(energy->at(0)>=500.0 && energy->at(0)<1000.0) quotient=((int)((energy->at(0)-500.0)/500.0))+56;
  if(energy->at(0)>=1000.0) quotient=57;
  ph_t_E_gain[quotient][gain->at(0)]->Fill(time->at(0),wt1);
  //  if(energy->at(0)>500.0)  cout<<"E="<<energy->at(0)<<"   bin_no="<<quotient<<endl;  
  if(cell_energy<20.0) quotient1=(int)(cell_energy/1.0);
  if(cell_energy>=20.0 && cell_energy<50.0) quotient1=((int)((cell_energy-20.0)/2.0))+20;
  if(cell_energy>=50.0 && cell_energy<100.0) quotient1=((int)((cell_energy-50.0)/5.0))+35;
  if(cell_energy>=100.0 && cell_energy<200.0) quotient1=((int)((cell_energy-100.0)/20.0))+45;
  if(cell_energy>=200.0 && cell_energy<500.0) quotient1=((int)((cell_energy-200.0)/50.0))+50;
  if(cell_energy>=500.0 && cell_energy<1000.0) quotient1=((int)((cell_energy-500.0)/500.0))+56;
  if(cell_energy>=1000.0) quotient1=57;
  ph_t_cellE_gain[quotient1][gain->at(0)]->Fill(time->at(0),wt1);
  //if(energy->at(0)>500.0)  cout<<"cellE="<<cell_energy<<"   bin_no="<<quotient1<<endl;  

}
