/*
Created by Haichen Wang November 8, 2012
*/

#include "processEe.hh"
#include "MCxsec_ee.hh"

int main (int argc, char **argv)
{
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  cout << endl;
		 if (argc < 2)
		 {
		    printf("\nUsage: %s *.root\n\n",argv[0]);
		    exit(0);
		 }
		 
		 int const index = argc - 1;
		
		 TString path[index];
		 
		 for ( int j = 0; j < argc-1; j++)
		 {
			path[j] = (argv[j+1]);
		 }
		 
		 TChain *chain = new TChain("output");
		 
		 for (int k = 0; k < argc-1 ; k++)
		 {
		    chain -> Add(path[k]); // Add the root file to the TChain chain
		    cout << " adding " << path[k] << endl;
		 }
		 
		 p = new output(chain);
		 int numev = p->fChain->GetEntries();
		 
		 //Turn off the branches not used
		 cout<<"Turning off all branches"<<endl;
		 p -> fChain ->SetBranchStatus("*",0);
                 p -> fChain ->SetBranchStatus("runNumber",1);
                 p -> fChain ->SetBranchStatus("eventNumber",1);
                 p -> fChain ->SetBranchStatus("m_mcChannelNumber",1);
                 p -> fChain ->SetBranchStatus("n_el",1);
                 p -> fChain ->SetBranchStatus("el_pt",1);
                 p -> fChain ->SetBranchStatus("el_eta",1);
                 p -> fChain ->SetBranchStatus("el_phi",1);
                 p -> fChain ->SetBranchStatus("el_E",1);
                 p -> fChain ->SetBranchStatus("el_t",1);
                 p -> fChain ->SetBranchStatus("el_calo_z",1);
                 p -> fChain ->SetBranchStatus("el_calo_z_err",1);
                 p -> fChain ->SetBranchStatus("el_Reta",1);
                 p -> fChain ->SetBranchStatus("el_Rphi",1);
                 p -> fChain ->SetBranchStatus("el_dEs",1);
                 p -> fChain ->SetBranchStatus("el_Rmax2",1);
                 p -> fChain ->SetBranchStatus("el_wstot",1);
                 p -> fChain ->SetBranchStatus("el_ws3",1);
                 p -> fChain ->SetBranchStatus("el_Fside",1);
                 p -> fChain ->SetBranchStatus("el_Eratio",1);
                 p -> fChain ->SetBranchStatus("el_Rhad",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_onlId",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_gain",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_E",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_x",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_y",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_z",1);
                 p -> fChain ->SetBranchStatus("el_maxEcell_time",1);
                 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_nod0_ivarloose",1);
                 p -> fChain ->SetBranchStatus("HLT_2e17_lhvloose_nod0",1);
		 cout<<"Needed branches are added"<<endl;
		 
		 cout << "total number of events to be processed " << numev << endl;
		 double lumi = 33.017; //in fb-1
		 double wt_ge = 1.0;
		 get_event(0) ;
                 int DSID = p->m_mcChannelNumber;
                 bool isData = p->m_mcChannelNumber < 0 ;
                 map<string,double> sample = MCxsec_ee(DSID);
                 double xs = sample["xs"];
                 double Nevents = sample["Nevents"];
		 //cout<<"DSID="<<DSID<<endl;
		 define_SS_variables(Reta,Rphi,dEs,Rmax2,wstot,ws3,Fside,Eratio,Rhad);
		 define_pointing_variables(z_res_reco);
		 define_timing_variables(t_res_reco);
		 define_timing_res(ph_t_E_gain, ph_t_cellE_gain, ph_t_cellE_gain_eta1p4, ph_t_cellE_gain_eta1p5_2p4);
		 double tbins[7]={-4.0,0.5,1.1,1.3,1.5,1.8,4.0};		
		 for( int i = 0 ; i < numev ; i ++ )
		   //for( int i = 0 ; i < 1000000 ; i ++ )
		   {
		     if(i>0 && i%500000==0) cout << "Processed " << i << " events..."<<endl;
		     
		     get_event(i) ;
		     //Weight calculation
		     double wt=1.0;
		     if(!isData) wt = lumi*xs*wt_ge/Nevents;
		     trigger_sel->Fill(0.5,wt);
		     //		     if(p->HLT_e26_lhtight_nod0_ivarloose==1) trigger_sel->Fill(1.5,wt);
		     //if(p->HLT_e60_lhmedium_nod0==1) trigger_sel->Fill(2.5,wt);
		     //if(p->HLT_e140_lhloose_nod0==1) trigger_sel->Fill(3.5,wt);
		     if(p->HLT_2e17_lhvloose_nod0==1) trigger_sel->Fill(4.5,wt);

		     n_e->Fill((int)p->el_pt->size(),wt);

		     cutflow->Fill(0.5,wt);
		     
		     //		     //cout<<"Trigger checking"<<endl;
		     //Single electron trigger for timing bump studies
		     if(p->HLT_e26_lhtight_nod0_ivarloose==1){
                       if((int)p->el_pt->size()>1){
                         if(p->el_pt->at(0) > 27.0 && p->el_pt->at(1) > 10.0){
			   map<string,double> result1;
			   result1 = mee_calc(p->el_pt,p->el_eta,p->el_phi,p->el_E);
			   if(fabs(91.18-result1["m_ee"])<15.0 && p->el_maxEcell_onlId->at(1)!=4185790189408354304 && p->el_maxEcell_onlId->at(1)!=4057048372912062464 && p->el_maxEcell_onlId->at(1)!=4057050571935318016 && p->el_maxEcell_onlId->at(1)!=4057042875353923584 &&p->el_maxEcell_onlId->at(1)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(1)!=4057065965098106880 && p->el_maxEcell_onlId->at(1)!=4057060467539968000 && p->el_maxEcell_onlId->at(1)!=4057040676330668032 && p->el_maxEcell_onlId->at(1)!=4057068164121362432 && p->el_maxEcell_onlId->at(0)!=4054704214121644032){
			     ph_t_subleadel_trig_onZ->Fill(p->el_t->at(1),wt);
			     if(fabs(p->el_t->at(1))>15.0 && fabs(p->el_t->at(1))<35.0) ph_25ns_subleadel_trig_onZ_pt->Fill(p->el_pt->at(1),wt);
			   }}}}

		     


		     int trig=0;
		     if(p->HLT_2e17_lhvloose_nod0==1){
		       cutflow->Fill(1.5,wt);
		       if((int)p->el_pt->size()>1){
			 if(p->el_pt->at(0) > 18.0 && p->el_pt->at(1) > 18.0){ 
			   trig=1; 
			   cutflow->Fill(2.5,wt);}}}
		     else if(p->HLT_e26_lhtight_nod0_ivarloose==1){
		       cutflow->Fill(3.5,wt);
		       if((int)p->el_pt->size()>1){
			 if(p->el_pt->at(0) > 27.0 && p->el_pt->at(1) > 10.0){ 
			   trig=2;
			   cutflow->Fill(4.5,wt);}}}
		     if(trig==0) continue;
		     cutflow->Fill(5.5,wt);
		     //cout<<"Trigger checked"<<endl;
		     if((int)p->el_pt->size() != 2) continue;
		     //cout<<"Two electrons needed"<<endl;
		     //Require both electrons cell energy > 5 GeV
		     //		     if(p->el_maxEcell_E->at(0)<5.0 || p->el_maxEcell_E->at(1)<5.0) continue;
		     cutflow->Fill(6.5,wt);
		     //cout<<"maxEcell cut checked"<<endl;
		     map<string,double> result;
		    
		     result = mee_calc(p->el_pt,p->el_eta,p->el_phi,p->el_E);
		     fill_kinematics(p->el_pt,p->el_eta,p->el_phi,p->el_E,0,wt);
		     fill_hists_minv(result,wt);
		     //offZ
		     if(fabs(91.18-result["m_ee"])>15.0){
		       if(p->el_maxEcell_onlId->at(0)!=4185790189408354304 && p->el_maxEcell_onlId->at(0)!=4057048372912062464 && p->el_maxEcell_onlId->at(0)!=4057050571935318016 && p->el_maxEcell_onlId->at(0)!=4057042875353923584 &&p->el_maxEcell_onlId->at(0)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(0)!=4057065965098106880 && p->el_maxEcell_onlId->at(0)!=4057060467539968000 && p->el_maxEcell_onlId->at(0)!=4057040676330668032 && p->el_maxEcell_onlId->at(0)!=4057068164121362432 && p->el_maxEcell_onlId->at(0)!=4054704214121644032){
			 ph_t_offZ->Fill(p->el_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_offZ_bins->Fill(k1+0.5,wt);}
			 ph_t_leadel_offZ->Fill(p->el_t->at(0),wt);
			 //Different maxEcell_E bins
			 if(p->el_maxEcell_E->at(0)/1000.0>5.0 && p->el_maxEcell_E->at(0)/1000.0<7.0){
			   ph_t_leadel_offZ_ematch5_7->Fill(p->el_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch5_7_bins->Fill(k1+0.5,wt);}
			 }
			 if(p->el_maxEcell_E->at(0)/1000.0>7.0 && p->el_maxEcell_E->at(0)/1000.0<10.0){
			   ph_t_leadel_offZ_ematch7_10->Fill(p->el_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch7_10_bins->Fill(k1+0.5,wt);}
			 }
			 if(p->el_maxEcell_E->at(0)/1000.0>10.0 && p->el_maxEcell_E->at(0)/1000.0<12.0){
			   ph_t_leadel_offZ_ematch10_12->Fill(p->el_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch10_12_bins->Fill(k1+0.5,wt);}
			 }
			 if(p->el_maxEcell_E->at(0)/1000.0>12.0 && p->el_maxEcell_E->at(0)/1000.0<15.0){
			   ph_t_leadel_offZ_ematch12_15->Fill(p->el_t->at(0),wt);
			   for(int k1=0;k1<6;k1++){
			     if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_offZ_ematch12_15_bins->Fill(k1+0.5,wt);}
			 }
		       }
		       if(p->el_maxEcell_onlId->at(1)!=4185790189408354304 && p->el_maxEcell_onlId->at(1)!=4057048372912062464 && p->el_maxEcell_onlId->at(1)!=4057050571935318016 && p->el_maxEcell_onlId->at(1)!=4057042875353923584 &&p->el_maxEcell_onlId->at(1)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(1)!=4057065965098106880 && p->el_maxEcell_onlId->at(1)!=4057060467539968000 && p->el_maxEcell_onlId->at(1)!=4057040676330668032 && p->el_maxEcell_onlId->at(1)!=4057068164121362432 && p->el_maxEcell_onlId->at(0)!=4054704214121644032) ph_t_offZ->Fill(p->el_t->at(1),wt);
		     }


		     //onZ
		     if(fabs(91.18-result["m_ee"])>15.0) continue;
		     if(p->el_maxEcell_onlId->at(0)!=4185790189408354304 && p->el_maxEcell_onlId->at(0)!=4057048372912062464 && p->el_maxEcell_onlId->at(0)!=4057050571935318016 && p->el_maxEcell_onlId->at(0)!=4057042875353923584 &&p->el_maxEcell_onlId->at(0)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(0)!=4057065965098106880 && p->el_maxEcell_onlId->at(0)!=4057060467539968000 && p->el_maxEcell_onlId->at(0)!=4057040676330668032 && p->el_maxEcell_onlId->at(0)!=4057068164121362432 && p->el_maxEcell_onlId->at(0)!=4054704214121644032){
		       ph_t_onZ->Fill(p->el_t->at(0),wt);
		       ph_t_leadel_onZ->Fill(p->el_t->at(0),wt);
		       ph_pt_leadel_onZ->Fill(p->el_pt->at(0),wt);
		       ph_cellE_leadel_onZ->Fill(p->el_maxEcell_E->at(0)/1000.0,wt);
		       ph_t_subleadel_onZ->Fill(p->el_t->at(0),wt);
		       for(int k1=0;k1<6;k1++){
                         if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_onZ_bins->Fill(k1+0.5,wt);}
		       
		       if(p->el_maxEcell_E->at(0)/1000.0>5.0 && p->el_maxEcell_E->at(0)/1000.0<7.0){
			 ph_t_leadel_onZ_ematch5_7->Fill(p->el_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch5_7_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->el_maxEcell_E->at(0)/1000.0>7.0 && p->el_maxEcell_E->at(0)/1000.0<10.0){
			 ph_t_leadel_onZ_ematch7_10->Fill(p->el_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch7_10_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->el_maxEcell_E->at(0)/1000.0>10.0 && p->el_maxEcell_E->at(0)/1000.0<12.0){
			 ph_t_leadel_onZ_ematch10_12->Fill(p->el_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch10_12_bins->Fill(k1+0.5,wt);}
		       }
		       if(p->el_maxEcell_E->at(0)/1000.0>12.0 && p->el_maxEcell_E->at(0)/1000.0<15.0){
			 ph_t_leadel_onZ_ematch12_15->Fill(p->el_t->at(0),wt);
			 for(int k1=0;k1<6;k1++){
			   if(p->el_t->at(0)>tbins[k1] && p->el_t->at(0)<tbins[k1+1]) ph_t_onZ_ematch12_15_bins->Fill(k1+0.5,wt);}
		       }
		     }
		     if(p->el_maxEcell_onlId->at(1)!=4185790189408354304 && p->el_maxEcell_onlId->at(1)!=4057048372912062464 && p->el_maxEcell_onlId->at(1)!=4057050571935318016 && p->el_maxEcell_onlId->at(1)!=4057042875353923584 &&p->el_maxEcell_onlId->at(1)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(1)!=4057065965098106880 && p->el_maxEcell_onlId->at(1)!=4057060467539968000 && p->el_maxEcell_onlId->at(1)!=4057040676330668032 && p->el_maxEcell_onlId->at(1)!=4057068164121362432 && p->el_maxEcell_onlId->at(0)!=4054704214121644032) ph_t_onZ->Fill(p->el_t->at(1),wt);

		     //cout<<"Invariant mass checked"<<endl;
		     cutflow->Fill(7.5,wt);
		     //Fill timing distributions
		     fill_timing_hists(p->el_maxEcell_gain,p->el_t,wt);
		     //cout<<"Timing hists filled"<<endl;
		     //maxEcell_E variable is in MeV
		     if(p->el_t->at(0)>7.0 && p->el_t->at(0)<13.0){
		       ph_10ns_eta1->Fill(p->el_eta->at(0),wt);
		       ph_10ns_onlId->Fill(p->el_maxEcell_onlId->at(0)/1000000000000000,wt);
		       //		       if(p->el_maxEcell_onlId->at(0)!=4185790189408354304 && p->el_maxEcell_onlId->at(0)!=4057048372912062464 && p->el_maxEcell_onlId->at(0)!=4057050571935318016 && p->el_maxEcell_onlId->at(0)!=4057042875353923584 &&p->el_maxEcell_onlId->at(0)!=4057058268516712448 &&   p->el_maxEcell_onlId->at(0)!=4057065965098106880 && p->el_maxEcell_onlId->at(0)!=4057060467539968000 && p->el_maxEcell_onlId->at(0)!=4057040676330668032 && p->el_maxEcell_onlId->at(0)!=4057068164121362432  && p->el_maxEcell_onlId->at(0)!=4054704214121644032) cout<<"el_onlId="<<p->el_maxEcell_onlId->at(0)<<endl;
		       ph_10ns_eta_phi->Fill(p->el_eta->at(0),p->el_phi->at(0),wt);}
		     if(p->el_t->at(1)>7.0 && p->el_t->at(1)<13.0){
		       ph_10ns_onlId->Fill(p->el_maxEcell_onlId->at(1)/1000000000000000,wt);
		       //		       cout<<"el_onlId="<<p->el_maxEcell_onlId->at(1)<<endl;
		       ph_10ns_eta1->Fill(p->el_eta->at(1),wt);
		       ph_10ns_eta_phi->Fill(p->el_eta->at(1),p->el_phi->at(1),wt);}
		     
		     timing_resolution_hists(p->el_maxEcell_gain,p->el_E,p->el_maxEcell_E,p->el_t,p->el_eta,wt);
		     //cout<<"Timing resolution hists filled"<<endl;
		     //Fill kinematics after
		     fill_kinematics(p->el_pt,p->el_eta,p->el_phi,p->el_E,1,wt);
		     //Fill shower shape variables
                     SS_variables(p->el_eta,p->PV_z,p->el_Reta,p->el_Rphi,p->el_dEs,p->el_Rmax2,p->el_wstot,p->el_ws3,p->el_Fside,p->el_Eratio,p->el_Rhad,wt);
		     //Fill pointing distributions
		     pointing_distributions(p->el_eta,p->PV_z,p->el_calo_z,wt);
		     //cout<<"pointing filled"<<endl;
		     timing_distributions(p->el_eta,p->PV_z,p->el_t,wt);
		     //cout<<"Finished the event"<<endl;
		   }//numev loop

		 // Histograms need to be written into a TFile
                 TFile f("NPP_EGAM1_hist_output.root","recreate");
		 ph_t_onZ->Write();
                 ph_t_offZ->Write();
		 ph_t_leadel_offZ_ematch5_7->Write();
		 ph_t_leadel_onZ_ematch5_7->Write();
		 ph_t_leadel_offZ_ematch7_10->Write();
		 ph_t_leadel_onZ_ematch7_10->Write();
		 ph_t_leadel_offZ_ematch10_12->Write();
		 ph_t_leadel_onZ_ematch10_12->Write();
		 ph_t_leadel_offZ_ematch12_15->Write();
		 ph_t_leadel_onZ_ematch12_15->Write();
		 ph_t_leadel_onZ->Write();
		 ph_pt_leadel_onZ->Write();
		 ph_cellE_leadel_onZ->Write();
		 ph_t_subleadel_onZ->Write();
		 ph_t_subleadel_trig_onZ->Write();
		 ph_25ns_subleadel_trig_onZ_pt->Write();
                 ph_t_leadel_offZ->Write();
		 ph_t_onZ_bins->Write();
                 ph_t_offZ_bins->Write();
		 ph_t_onZ_ematch5_7_bins->Write();
                 ph_t_offZ_ematch5_7_bins->Write();
		 ph_t_onZ_ematch7_10_bins->Write();
                 ph_t_offZ_ematch7_10_bins->Write();
		 ph_t_onZ_ematch10_12_bins->Write();
                 ph_t_offZ_ematch10_12_bins->Write();
		 ph_t_onZ_ematch12_15_bins->Write();
                 ph_t_offZ_ematch12_15_bins->Write();

		 ph_beforetrig_pt1->Write();
		 ph_beforetrig_eta1->Write();
		 ph_beforetrig_phi1->Write();
		 ph_beforetrig_E1->Write();
		 ph_aftertrig_pt1->Write();
		 ph_aftertrig_eta1->Write();
		 ph_aftertrig_phi1->Write();
		 ph_aftertrig_E1->Write();
		 ph_XPVz_Yphz->Write();
		 ph_XPVz_Yphconvz->Write();
		 ph_XPVz_Yphconvz1->Write();
		 ph_XPVz_Yphconvz3->Write();
		 ph_XPVz_Yphconvz5->Write();
		 ph_XPVz_Yphz_type1->Write();
		 ph_XPVz_Yphz_type3->Write();
		 ph_XPVz_Yphz_type5->Write();
		 ph_Xphconvz_Yphz_type1->Write();
		 ph_Xphconvz_Yphz_type3->Write();
		 ph_Xphconvz_Yphz_type5->Write();
		 trigger_sel->Write();
		 n_e->Write();
		 e_pt1->Write();
		 e_eta1->Write();
		 e_phi1->Write();
		 e_E1->Write();
		 e_pt2->Write();
		 e_eta2->Write();
		 e_phi2->Write();
		 e_E2->Write();
		 n_ph->Write();
		 ph_pt1->Write();
		 ph_eta1->Write();
		 ph_phi1->Write();
		 ph_E1->Write();
		 m_ee->Write();
		 m_eegamma->Write();
		 m_diff->Write();
		 e_sel_pt1->Write();
		 e_sel_eta1->Write();
		 e_sel_phi1->Write();
		 e_sel_E1->Write();
		 e_sel_pt2->Write();
		 e_sel_eta2->Write();
		 e_sel_phi2->Write();
		 e_sel_E2->Write();
		 ph_sel_pt1->Write();
		 ph_sel_eta1->Write();
		 ph_sel_phi1->Write();
		 ph_sel_E1->Write();
		 X_ph_pt_Y_eegamma_minv_sel->Write();
		 X_ph_pt_Y_ee_minv_sel->Write();
		 X_ph_pt_Y_eegamma_minv->Write();
		 X_ph_pt_Y_ee_minv->Write();
		 
		 for(int i=0;i<7;i++){
		   for(int j=0;j<3;j++){
		     Reta[i][j]->Write();
		     Rphi[i][j]->Write();
		     dEs[i][j]->Write();
		     Rmax2[i][j]->Write();
		     wstot[i][j]->Write();
		     ws3[i][j]->Write();
		     Fside[i][j]->Write();
		     Eratio[i][j]->Write();
		     Rhad[i][j]->Write();}}
		 for(int i=0;i<11;i++){
		   z_res_reco[i]->Write();}
		 for(int i=0;i<11;i++){
		   t_res_reco[i]->Write();}
		 cutflow->Write();
		 ph_gain->Write();
		 ph_t_gain2->Write();
		 ph_t_gain1->Write();
		 ph_t_gain0->Write();
		 ph_10ns_onlId->Write();
		 ph_10ns_eta1->Write();
		 ph_10ns_eta_phi->Write();
		 for(int i=0;i<58;i++){
		   for(int j=0;j<3;j++){
		     ph_t_E_gain[i][j]->Write();
		     ph_t_cellE_gain[i][j]->Write();
		     ph_t_cellE_gain_eta1p4[i][j]->Write();
		     ph_t_cellE_gain_eta1p5_2p4[i][j]->Write();}}
		 ph_cellE->Write();
		 ph_ratio_cellE_E->Write();
		 ph_cellE_gain0->Write();
		 ph_cellE_gain1->Write();
		 ph_cellE_gain2->Write();
		 ph_E_gain0->Write();
		 ph_E_gain1->Write();
		 ph_E_gain2->Write();
		 
                 f.Close();


		 return 0 ;
}
