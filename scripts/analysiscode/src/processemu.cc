/*
Created by Sai Neha Santpur June, 2019
*/

#include "processemu.hh"
#include "MCxsec_ee.hh"
//STDM4 derivation

int main (int argc, char **argv)
{
  gROOT->ProcessLine(".x rootlogon.C");
  gROOT->SetStyle("ATLAS");
  gROOT->ForceStyle();

  cout << endl;
		 if (argc < 2)
		 {
		    printf("\nUsage: %s *.root\n\n",argv[0]);
		    exit(0);
		 }
		 
		 int const index = argc - 1;
		
		 TString path[index];
		 
		 for ( int j = 0; j < argc-1; j++)
		 {
			path[j] = (argv[j+1]);
		 }
		 
		 TChain *chain = new TChain("output");
		 
		 for (int k = 0; k < argc-1 ; k++)
		 {
		    chain -> Add(path[k]); // Add the root file to the TChain chain
		    cout << " adding " << path[k] << endl;
		 }
		 
		 p = new output(chain);
		 int numev = p->fChain->GetEntries();
		 p -> fChain ->SetBranchStatus("*",0);

                 p -> fChain ->SetBranchStatus("runNumber",1);
                 p -> fChain ->SetBranchStatus("eventNumber",1);
                 p -> fChain ->SetBranchStatus("m_mcChannelNumber",1);
		 p -> fChain ->SetBranchStatus("m_met",1);
                 p -> fChain ->SetBranchStatus("n_el",1);
                 p -> fChain ->SetBranchStatus("PV_z",1);
                 p -> fChain ->SetBranchStatus("el_pt",1);
                 p -> fChain ->SetBranchStatus("el_eta",1);
                 p -> fChain ->SetBranchStatus("el_phi",1);
                 p -> fChain ->SetBranchStatus("el_E",1);
                 p -> fChain ->SetBranchStatus("el_calo_z",1);
                 p -> fChain ->SetBranchStatus("el_calo_z_err",1);
		 p -> fChain ->SetBranchStatus("n_ph",1);
		 p -> fChain ->SetBranchStatus("ph_pt",1);
		 p -> fChain ->SetBranchStatus("ph_t",1);
		 p -> fChain ->SetBranchStatus("el_t",1);
		 p -> fChain ->SetBranchStatus("ph_maxEcell_E",1);

                 p -> fChain ->SetBranchStatus("ph_eta",1);
                 p -> fChain ->SetBranchStatus("ph_phi",1);
                 p -> fChain ->SetBranchStatus("ph_E",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z",1);
                 p -> fChain ->SetBranchStatus("ph_calo_z_err",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z",1);
                 p -> fChain ->SetBranchStatus("ph_convType",1);
                 p -> fChain ->SetBranchStatus("ph_conv_z_err",1);
		 p -> fChain ->SetBranchStatus("mu_pt",1);
                 p -> fChain ->SetBranchStatus("mu_eta",1);
                 p -> fChain ->SetBranchStatus("mu_phi",1);
                 p -> fChain ->SetBranchStatus("mu_E",1);
		 p -> fChain ->SetBranchStatus("n_mu",1);

                 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_nod0_ivarloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_smooth_ivarloose",1);
		 
                 p -> fChain ->SetBranchStatus("HLT_2e17_lhvloose_nod0",1);

		 p -> fChain ->SetBranchStatus("HLT_e24_medium_iloose_EM18VH",1);
                 p -> fChain ->SetBranchStatus("HLT_e24_medium_iloose_EM20VH",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_tight_iloose_EM20VH",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_tight_iloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e26_tight_iloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_lhtight_iloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e26_lhtight_iloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_lhtight_nod0_ivarloose",1);
		 
		 p -> fChain ->SetBranchStatus("HLT_e60_medium",1);
		 p -> fChain ->SetBranchStatus("HLT_e60_lhmedium_nod0",1);
		 p -> fChain ->SetBranchStatus("HLT_e120_loose",1);
		 p -> fChain ->SetBranchStatus("HLT_e140_loose",1);
		 p -> fChain ->SetBranchStatus("HLT_e140_lhloose_nod0",1);
		 
		 p -> fChain ->SetBranchStatus("HLT_e24_lhmedium_nod0_ivarloose",1);
		 
		 p -> fChain ->SetBranchStatus("HLT_e24_lhmedium_iloose_EM18VH",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_lhmedium_iloose_EM20VH",1);
		 p -> fChain ->SetBranchStatus("HLT_e24_lhtight_iloose_EM20VH",1);
		 p -> fChain ->SetBranchStatus("HLT_e60_lhmedium",1);
		 p -> fChain ->SetBranchStatus("HLT_e120_lhloose",1);
		 p -> fChain ->SetBranchStatus("HLT_e140_lhloose",1);
		 p -> fChain ->SetBranchStatus("HLT_mu20_iloose_L1MU15",1);
		 p -> fChain ->SetBranchStatus("HLT_mu24_imedium",1);
		 p -> fChain ->SetBranchStatus("HLT_mu26_ivarmedium",1);
		 p -> fChain ->SetBranchStatus("HLT_mu24_ivarmedium",1);
		 p -> fChain ->SetBranchStatus("HLT_mu24_ivarloose",1);

		 p -> fChain ->SetBranchStatus("HLT_mu26_imedium",1);
		 p -> fChain ->SetBranchStatus("HLT_mu50",1);
		 p -> fChain ->SetBranchStatus("HLT_mu40",1);

		 cout << "total number of events to be processed " << numev << endl;
		 double lumi = 33.017; //in fb-1
		 double wt_ge = 1.0;
		 get_event(0) ;
                 int DSID = p->m_mcChannelNumber;
                 bool isData = p->m_mcChannelNumber < 0 ;
                 map<string,double> sample = MCxsec_ee(DSID);
                 double xs = sample["xs"];
                 double Nevents = sample["Nevents"];
		 cout<<"DSID="<<DSID<<endl;
		 for( int i = 0 ; i < numev ; i ++ )
		   {
		     //cout<<"------Event="<<i<<endl;
		     if(i>0 && i%1000000==0) cout << "Processed " << i << " events..."<<endl;
		     for(int ll=0; ll<(int)p->ph_pt->size(); ll++){
		       if(p->ph_t->at(ll)>-99999) ph_t_full->Fill(p->ph_t->at(ll));}
		     for(int mm=0; mm<(int)p->el_pt->size(); mm++){
		       if(p->el_t->at(mm)>-99999) el_t_full->Fill(p->el_t->at(mm));}
		       
		     get_event(i) ;

		     //Weight calculation
		     //		     double wt=8.5868;
		     double wt=1.00;
		     //wt=8.5868 if using data_few_cut.root without e/ph timing
		     //wt=21.4552723204 if using 1.5fb-1 sample
		     if(!isData) wt = lumi*xs*wt_ge/Nevents;
		     //		     if(p->HLT_e26_lhtight_nod0_ivarloose==1) trigger_sel->Fill(1.5,wt);
		     //if(p->HLT_e60_lhmedium_nod0==1) trigger_sel->Fill(2.5,wt);
		     //if(p->HLT_e140_lhloose_nod0==1) trigger_sel->Fill(3.5,wt);
		     //cout<<"Triggers"<<endl;
		     //cout<<"filled electron number"<<endl;
		     //cout<<"No. of photons="<<(int)p->ph_pt->size()<<endl;
		     //		     //cout<<"photon 4 vector="<<p->ph_pt->at(0)<<"  "<<p->ph_eta->at(0)<<"  "<<p->ph_phi->at(0)<<"  "<<p->ph_E->at(0)<<"  "<<wt<<endl;
		     //cout<<"Filled all photon kinematics"<<endl;
		     cutflow->Fill(0.5,wt);
		     int trig=0;
		     //		     cout<<"Checking triggers"<<endl;
		     if(p->HLT_e140_lhloose_nod0 || p->HLT_e24_lhmedium_nod0_ivarloose || p->HLT_e24_lhtight_nod0_ivarloose || p->HLT_e26_lhtight_smooth_ivarloose || p->HLT_e26_lhtight_nod0_ivarloose || p->HLT_e24_medium_iloose_EM18VH  || p->HLT_e24_medium_iloose_EM20VH || p->HLT_e24_tight_iloose_EM20VH || p->HLT_e24_tight_iloose || p->HLT_e26_tight_iloose || p->HLT_e60_medium || p-> HLT_e60_lhmedium_nod0 || p->HLT_e120_loose || p->HLT_e140_loose || p->HLT_e24_lhmedium_iloose_EM18VH  || p->HLT_e24_lhmedium_iloose_EM20VH || p->HLT_e24_lhtight_iloose_EM20VH || p->HLT_e24_lhtight_iloose || p->HLT_e26_lhtight_iloose || p->HLT_e60_lhmedium || p->HLT_e120_lhloose || p->HLT_e140_lhloose){
		       if((int)p->el_pt->size()>=1){
			 if(p->el_pt->at(0) > 27.0 && ((fabs(p->el_eta->at(0))<1.37) || ((fabs(p->el_eta->at(0))>1.52) && fabs(p->el_eta->at(0)) < 2.5))) trig=1;}}
		     if( p->HLT_mu24_imedium || p->HLT_mu26_imedium || p->HLT_mu50 || p->HLT_mu26_ivarmedium || p->HLT_mu40 || p->HLT_mu24_ivarloose || p->HLT_mu24_ivarmedium || p->HLT_mu20_iloose_L1MU15){
		       if((int)p->mu_pt->size()>=1){
			 if(p->mu_pt->at(0) > 27.0 && fabs(p->mu_eta->at(0)) < 2.5) trig=2;}}
		     if(trig==0) continue;
		     cutflow->Fill(1.5,wt);
		     //cout<<"Triggers checked"<<endl;

		     if( (int)p->ph_pt->size() < 1) continue;
		     if(trig==1){
		       fill_kinematics(p->el_pt,p->el_eta,p->el_phi,p->el_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,wt);
		       n_lep->Fill(p->n_el,wt);}
		     if(trig==2){
		       fill_kinematics(p->mu_pt,p->mu_eta,p->mu_phi,p->mu_E,p->ph_pt,p->ph_eta,p->ph_phi,p->ph_E,wt);
		       n_lep->Fill(p->n_mu,wt);}


		     cutflow->Fill(3.5,wt);

		     bool photon1 = false;
		     if(p->ph_pt->at(0)>10.0 && fabs(p->ph_eta->at(0))<1.37) photon1=true;
		     if(!photon1) continue;
		     //		     cout<<"Photon cut applied"<<endl;

		     cutflow->Fill(4.5,wt);
		     
		     met->Fill(p->m_met,wt);
		     if(p->m_met<20.0){
		       met_20->Fill(p->m_met,wt);
		       ph_zdca_CR->Fill(p->ph_calo_z->at(0) - p->PV_z,wt);
		       met_bin->Fill(0.5,wt);}
		     if(p->m_met>=20.0 && p->m_met<50.0){
		       ph_zdca_VR->Fill(p->ph_calo_z->at(0) - p->PV_z,wt);
		       met_bin->Fill(1.5,wt);
		       ph_t_VR->Fill(p->ph_t->at(0),wt);
		       ph_pt_VR->Fill(p->ph_pt->at(0),wt);
		       ph_cellE_VR->Fill(p->ph_maxEcell_E->at(0)/1000.0,wt);
		     }
		     if(p->m_met>50.0){
		       met_50->Fill(p->m_met,wt);
		       met_bin->Fill(2.5,wt);}
		     
		     //CR definition
		     if(p->m_met>20.0) continue;
		     //		     cout<<"METapplied"<<endl;
		     ph_t_CR->Fill(p->ph_t->at(0),wt);
		     ph_pt_CR->Fill(p->ph_pt->at(0),wt);
		     ph_cellE_CR->Fill(p->ph_maxEcell_E->at(0)/1000.0,wt);
		     ph_PV_z->Fill(p->PV_z,wt);
		     ph_calo_z->Fill(p->ph_calo_z->at(0),wt);
		     
		     double zdca=p->ph_calo_z->at(0) - p->PV_z;
		     ph_t_zdca_CR->Fill(p->ph_t->at(0),zdca,wt);
		     if(p->ph_t->at(0)>0.0) ph_abs_t_zdca_CR->Fill(p->ph_t->at(0),fabs(zdca),wt);

		     ph_zdca->Fill(zdca ,wt);
		     if(fabs(zdca)<40.0) ph_zdca_bins->Fill(0.5,wt);
		     if(fabs(zdca)>40.0 && fabs(zdca)<80.0) ph_zdca_bins->Fill(1.5,wt);
		     if(fabs(zdca)>80.0 && fabs(zdca)<120.0) ph_zdca_bins->Fill(2.5,wt);
		     if(fabs(zdca)>120.0 && fabs(zdca)<160.0) ph_zdca_bins->Fill(3.5,wt);
		     if(fabs(zdca)>160.0 && fabs(zdca)<200.0) ph_zdca_bins->Fill(4.5,wt);
		     if(fabs(zdca)>200.0 && fabs(zdca)<90000.0) ph_zdca_bins->Fill(5.5,wt);
		     


		   }//numev loop

		 // Histograms need to be written into a TFile
                 TFile f("NPP_STDM4_hist_output.root","recreate");
		 n_lep->Write();
		 lep_pt1->Write();
		 lep_eta1->Write();
		 lep_phi1->Write();
		 lep_E1->Write();
		 ph_t_full->Write();
		 el_t_full->Write();
		 n_ph->Write();
		 ph_pt1->Write();
		 ph_eta1->Write();
		 ph_phi1->Write();
		 ph_E1->Write();
		 met->Write();
		 met_bin->Write();
		 ph_zdca_CR->Write();
		 ph_zdca_VR->Write();		 
		 met_20->Write();
		 met_50->Write();
		 ph_zdca->Write();
		 ph_PV_z->Write();
		 ph_calo_z->Write();
		 ph_zdca_bins->Write();
		 ph_t_CR->Write();
		 ph_t_VR->Write();
		 ph_pt_CR->Write();
		 ph_pt_VR->Write();
		 ph_cellE_CR->Write();
		 ph_cellE_VR->Write();
		 met_bin_1lep->Write();
		 met_bin_2lepSF->Write();
		 met_bin_2lepOF->Write();
		 ph_t_zdca_CR->Write();
		 ph_abs_t_zdca_CR->Write();
		 
		 cutflow->Write();
		 
                 f.Close();


		 return 0 ;
}
