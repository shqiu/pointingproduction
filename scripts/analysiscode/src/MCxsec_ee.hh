map<string,double>MCxsec_ee(int DSID ){
  
  double xs = -9 , Nevents = -9;
  //cross-section from ami page (in fb)
  if( DSID == 364500 ) { xs =  57619.0  ; Nevents = 4000000.0   ; }
  if( DSID == 364501 ) { xs =  34594.0  ; Nevents = 3000000.0   ; }
  if( DSID == 364502 ) { xs =  6290.0  ; Nevents = 1000000.0   ; }
  if( DSID == 364503 ) { xs =  491.200  ; Nevents =  487000.0  ; }
  if( DSID == 364504 ) { xs =  63.474  ; Nevents =  100000.0  ; }
  //Z->ee Powheg+pythia xs=1894800.0, kfactor=1.02600047339 so xs_effective=xs*kfactor
  if( DSID == 361106) { xs = 1944065.7 ; Nevents = 79942000.0 ; }
  map<string,double> xsec;
  xsec["xs"] = xs;
  xsec["Nevents"] = Nevents;
  
  return xsec;
}
