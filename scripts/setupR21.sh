## This script shuold be used in ../source
## source NonPointingAnalysis/scripts/setupR21.sh
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
alias setupATLAS='source /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/user/atlasLocalSetup.sh'
setupATLAS
git clone ssh://git@gitlab.cern.ch:7999/dmahon/OfflineTimingToolRun2.git
#git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
git clone --recursive -b release/v1.8 ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/MCSamples.config ../source/HGamCore/HGamAnalysisFramework/data/MCSamples.config
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PhotonHandler.cxx ../source/HGamCore/HGamAnalysisFramework/Root/PhotonHandler.cxx
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PhotonHandler.h ../source/HGamCore/HGamAnalysisFramework/HGamAnalysisFramework/PhotonHandler.h
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/EventHandler.cxx ../source/HGamCore/HGamAnalysisFramework/Root/EventHandler.cxx
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/EventHandler.h ../source/HGamCore/HGamAnalysisFramework/HGamAnalysisFramework/EventHandler.h
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PRW*.root ../source/HGamCore/HGamAnalysisFramework/data/.
asetup AnalysisBase,21.2.56,here
#asetup AnalysisBase,21.2.90,here
cd $TestArea/../build
cmake $TestArea
cmake --build $TestArea/../build
source $TestArea/../build/$CMTCONFIG/setup.sh
