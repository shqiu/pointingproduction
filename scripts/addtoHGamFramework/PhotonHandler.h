#pragma once

// EDM include(s):
#include "EgammaAnalysisInterfaces/IAsgDeadHVCellRemovalTool.h"
#include "EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h"
#include "ElectronPhotonFourMomentumCorrection/EgammaCalibrationAndSmearingTool.h"
#include "ElectronPhotonSelectorTools/AsgPhotonIsEMSelector.h"
#include "PhotonEfficiencyCorrection/AsgPhotonEfficiencyCorrectionTool.h"
#include "xAODEgamma/PhotonAuxContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEventInfo/EventInfo.h"

// Local include(s):
#include "HGamAnalysisFramework/HgammaHandler.h"


namespace CP {
  class PhotonPointingTool;
  class IsolationSelectionTool;
  class IsolationCorrectionTool;
}

class ElectronPhotonShowerShapeFudgeTool;
class EGammaAmbiguityTool;
class egammaLayerRecalibTool;

namespace HG {
  class PhotonHandler : public HgammaHandler<xAOD::Photon, xAOD::PhotonContainer, xAOD::PhotonAuxContainer> {
  private:
    CP::EgammaCalibrationAndSmearingTool *m_photonCalibTool;
    CP::PhotonPointingTool               *m_pointTool;
    std::map<egammaPID::egammaIDQuality, AsgPhotonIsEMSelector *> m_photonSelectors;
    std::map<egammaPID::egammaIDQuality, SG::AuxElement::Accessor<char>* > m_pidAcc;
    std::map<egammaPID::egammaIDQuality, SG::AuxElement::Accessor<char>* > m_pidAccDF;
    static SG::AuxElement::Accessor<unsigned int> m_pidMaskAccDF;
    static SG::AuxElement::Accessor<char> m_pCleanAccDF;

    std::map<HG::Iso::IsolationType, CP::IsolationSelectionTool *> m_isoTools;
    std::map<HG::Iso::IsolationType, SG::AuxElement::Accessor<char>* > m_isoAcc;
    CP::IsolationCorrectionTool *m_isoCorrTool;

    ElectronPhotonShowerShapeFudgeTool   *m_fudgeMC;
    EGammaAmbiguityTool                  *m_fakeTool;
    AsgPhotonEfficiencyCorrectionTool    *m_photonSF;
    AsgPhotonEfficiencyCorrectionTool    *m_photonIsoSF;
    AsgPhotonEfficiencyCorrectionTool    *m_photonTrigSF;
    asg::AnaToolHandle<IAsgDeadHVCellRemovalTool> m_deadHVTool;

    bool           m_doPidCut;
    StrV           m_pidCuts;
    egammaPID::egammaIDQuality m_defaultPid;
    bool           m_doQuality;
    bool           m_doHV;
    bool           m_doCleaning;
    bool           m_doIsoCut;
    bool           m_doAmbCut;
    bool           m_doAmbiguityInfoDecoration; //!< just write some additional info about associated electron
    StrV           m_isoCuts;
    HG::Iso::IsolationType m_defaultIso;
    double         m_etaCut;
    double         m_ptCut;

    bool    m_crackReject;
    double  m_barrelMax;
    double  m_endcapMin;

    bool    m_doCalib;
    bool    m_doCalibAndSmear;
    bool    m_doFudge;
    bool    m_doScale;
    bool    m_doAuthor;
    bool    m_correctIso;
    bool    m_rejectAllAssocTracks;



  private:
    void removeAuthorCut(xAOD::PhotonContainer &photons);



  public:
    static SG::AuxElement::Accessor<float> effSF, scaleFactor, isoSF, trigSF_2g20;
    static SG::AuxElement::Accessor<float> effSFunc;
    static SG::AuxElement::Accessor<float> Ecalib_ratio;
    static SG::AuxElement::Accessor<float> r_SL1, z_SL1;
    static SG::AuxElement::Accessor<float> etaS1, etaS2, pt_s2;
    static SG::AuxElement::Accessor<float> cl_E, cl_eta, cl_phi, cl_etaCalo, cl_phiCalo, cl_TileGap3;
    static SG::AuxElement::Accessor<float> convtrk1nPixHits, convtrk1nSCTHits;
    static SG::AuxElement::Accessor<float> convtrk2nPixHits, convtrk2nSCTHits;
    static SG::AuxElement::Accessor<float> pt1conv, pt2conv, ptconv;
    static SG::AuxElement::Accessor<float> relEreso;
    static SG::AuxElement::Accessor<char> isLoose, isTight, isTight_nofudge;
    static SG::AuxElement::Accessor<char> passOQ, passHV, passCleaning, passCrackVetoCleaning;
    static SG::AuxElement::Accessor<int>  conversionType;
    static SG::AuxElement::Accessor<float> Rconv, zconv;
    static SG::AuxElement::Accessor<float> truthConvRadius;
    static SG::AuxElement::Accessor<unsigned int> isEMTight, isEMTight_nofudge;
    static SG::AuxElement::Accessor<float> topoetcone20_SC;
    static SG::AuxElement::Accessor<float> topoetcone40_SC;
    static SG::AuxElement::Accessor<float> ptcone20_TightTTVA_pt1000;

    //JB
    static SG::AuxElement::Accessor<ElementLink<xAOD::IParticleContainer> > accTruthLink;
    static SG::AuxElement::Accessor<int> accPdgId;
    static SG::AuxElement::Accessor<int> accParentPdgId;


  public:
    PhotonHandler(const char *name, xAOD::TEvent *event, xAOD::TStore *store);
    virtual ~PhotonHandler();

    virtual EL::StatusCode initialize(Config &config);

    virtual xAOD::PhotonContainer getCorrectedContainer();
    virtual xAOD::PhotonContainer applySelection(xAOD::PhotonContainer &container);
    virtual xAOD::PhotonContainer applySelectionNoPID(xAOD::PhotonContainer &container);
    virtual xAOD::PhotonContainer applyPreSelection(xAOD::PhotonContainer &container);
    virtual CP::SystematicCode    applySystematicVariation(const CP::SystematicSet &sys);

    /// applies kinematic selection cuts: not-in-crack + pT cut
    bool passPtEtaCuts(const xAOD::Photon *gam);

    /// applies OQ cut, if specified
    bool passOQCut(const xAOD::Photon *gam);
    void decorateOQ(xAOD::Photon &gam);

    /// applies HV cut, if specified
    bool passHVCut(const xAOD::Photon *gam);
    void decorateHV(xAOD::Photon &gam);

    /// applies cleaning cut, if specified
    bool passCleaningCut(const xAOD::Photon *gam);
    void decorateCleaning(xAOD::Photon &gam);

    /// applies ambiguity cut, if specified
    bool passAmbCut(const xAOD::Photon *gam);
    void decorateAmbCut(xAOD::Photon &gam);

    // apply some decoration relative to the associate electron if ambiguos
    void decorateAmbInfo(xAOD::Photon &gam) const;


    /// applies Iso cut specified in config file
    bool passIsoCut(const xAOD::Photon *gam, HG::Iso::IsolationType iso = HG::Iso::Undefined);
    void decorateClusterInfo(xAOD::Photon &gam);
    void decorateIso(xAOD::Photon &gam);
    void decorateIsoSC(xAOD::Photon &gam);
    void decorateIso(xAOD::IParticle &part);
    void correctIsoLeakage(xAOD::Photon &gam);

    /// applies PID cut specified in config file
    bool passLoosePrime(const xAOD::Photon *gam, int cutNumber = 4);
    bool passPIDCut(const xAOD::Photon *gam, egammaPID::egammaIDQuality pid = egammaPID::NoIDCut);
    void decoratePID(xAOD::Photon &gam);

    /// Requires photon to pass preselection, defined by
    /// a) OQ, b) pT>25, |eta_s2| selection, and c) Loose PID
    bool passPreSelection(const xAOD::Photon *gam);

    /// applies author cut, to remove topocluster seeded photons
    bool passAuthorCut(const xAOD::Photon *gam);

    /// Access the calibration tool
    CP::EgammaCalibrationAndSmearingTool *getCalibrationAndSmearingTool() { return m_photonCalibTool; };

    /// calibrates and smears a photon
    static void calibrateAndSmearPhoton(xAOD::Photon *gam,
                                        CP::EgammaCalibrationAndSmearingTool *photonCalibTool);

    /// access the isolation and PID types needed to initalize the tools
    HG::Iso::IsolationType getIsoType(TString isoName);
    unsigned int getPIDmask(TString pidName);
    egammaPID::egammaIDQuality getPID(TString pidName);

    /// Use NN to find PV vertex of two leading photons
    double getPVz(xAOD::PhotonContainer &container, const xAOD::EventInfo *eventInfo);

    /// applies PVz correction only to Pt/Eta
    virtual void correctPrimaryVertex(xAOD::Photon &gam, float z) const;

    /// applies PVz correction
    virtual void correctPrimaryVertex(const xAOD::Vertex *vertex, xAOD::Photon &gam);
    void decorateHardVertex(xAOD::Photon &gam);

    /// if MC, the photon is smeared using randSeed = 100*evtNum+photonIndex
    /// and the shower shape variables are "fudged"
    void applyFudgeFactor(xAOD::Photon *gam);

    /// decorate photon with efficiency scale factor and uncertainty
    void applyScaleFactor(xAOD::Photon *gam);

    const CP::PhotonPointingTool *getPointingTool() const { return m_pointTool; }

    /// access of the PVz corrected eta of a photon based on the eta in the first sampling
    static double PVz_corrected_eta(double eta_s1, double PVz);

    /// calculates transverse distance [mm] in first sampling layer given the eta coordinate
    static double BarrelR_s1(double eta_s1);

    /// calculates endcap z position [mm] in the first sampling layer given the eta coordinate
    static double EndcapZ_s1(double eta_s1);

    /// calculates transverse distance [mm] in first sampling layer given the eta coordinate
    static double r_s1(double eta_s1);

    /// calculates z position [mm] in the first sampling layer
    static double z_s1(double eta_s1);

    /// Fernando's method
    static const xAOD::Photon *findTrigMatchedPhoton(const xAOD::Photon *photon,
                                                     const xAOD::PhotonContainer *trigPhotons,
                                                     bool debug = false);

    /// print details about photon to the screen
    static void printPhoton(const xAOD::Photon *gam, TString comment = "");

  };
}
