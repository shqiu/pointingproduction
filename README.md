This package is created from the HGamAnalysisFramework. It utilizes functions in that framework. 


# Dependencies

See this twiki page for HGamAnalysisFramework
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HggDerivationAnalysisFramework

To check out the HGamAnalysisFramework and install, follow the instructions here
https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HggDerivationAnalysisFramework#Getting%20started

<!-- we are using "v1.9.1-h025 21.2.113" -->

The timing calibration is done by OfflineTimingTool, which can be checked by

```
git clone https://gitlab.cern.ch/dmahon/OfflineTimingToolRun2 OfflineTimingToolRun2
```


# Detailed instruction for installation the "whole thing" 

-This first part is total copy&paste from the HGamAnalysisFramework twiki, except the correct tags and analysis base release are used

```
mkdir HGam 
cd HGam 
mkdir build run source 
cd source
setupATLAS 
asetup AnalysisBase,21.2.131,here
```
cd $TestArea # this should be the "source" directory that you created earlier 
```
git clone --recursive ssh://git@gitlab.cern.ch:7999/atlas-hgam-sw/HGamCore.git 
cd HGamCore 
git checkout v1.10.15-h026
git submodule update --init --recursive 
cd - 
source HGamCore/HGamAnalysisFramework/scripts/setupCMake 

cd $TestArea/../build 
cmake ../source 
cmake --build . 

source $TestArea/../build/$AnalysisBase_PLATFORM/setup.sh 
```

-Check out OfflineTimingTool package in $TestArea/../source/ then do the following
```
cd $TestArea/../build
cmake ../source
cmake --build .
```
Note that in the NonPointingAnalysis package, I have use "OfflineTimingToolRun2" as the package name for OfflineTimingTool


-Install NonPointingAnalysis

```
cd $TestArea/../source/
git clone ...
```

Please copy these files to HGamFramework to add pileup reweighting files for our signal along with the cross-section information and to enable photon selection without ID and isolation requirements

```
pwd
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/MCSamples.config ../source/HGamCore/HGamAnalysisFramework/data/MCSamples.config
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PhotonHandler.cxx ../source/HGamCore/HGamAnalysisFramework/Root/PhotonHandler.cxx
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PhotonHandler.h ../source/HGamCore/HGamAnalysisFramework/HGamAnalysisFramework/PhotonHandler.h
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/EventHandler.cxx ../source/HGamCore/HGamAnalysisFramework/Root/EventHandler.cxx
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/EventHandler.h ../source/HGamCore/HGamAnalysisFramework/HGamAnalysisFramework/EventHandler.h
cp ../source/NonPointingAnalysis/scripts/addtoHGamFramework/PRW*.root ../source/HGamCore/HGamAnalysisFramework/data/.
```

Copy the PRW  file
```
cp ../source/NTUP_PILEUP.15864839._000001.pool.root.1 ../source/HGamCore/HGamAnalysisFramework/data/
```

#Now compile the package

```
cd $TestArea/../build
cmake ../source
cmake --build .
```


# Run the package

```
cd $TestArea/../run
```

## if you just log on to your machine, then go to the source directory and run the following

```
asetup AnalysisBase,21.2.131,here
source ../build/x86_64-centos7-gcc8-opt/setup.sh
```

## run locally

```
runNPAnalysis NonPointingAnalysis/STDM4.cfg test.root
```

or

```
runNPAnalysis NonPointingAnalysis/STDM4.cfg  InputFileList: $1 NumEvents: 1000
```

- the `inputfile.root` here is a DAOD file. Use `NPAnalysis.run*` in the cfg file to turn on/off processing for different objects, and timing calculation. This is needed as some DAODs may not have all the content needed to execute the code 
- the `InputFileList:` is a list of DAOD files.

## run on the grid

```
lsetup rucio

voms-proxy-init -voms atlas

lsetup panda

DAOD=mc16_13TeV.366142.Sh_224_NN30NNLO_eegamma_LO_pty_35_70.deriv.DAOD_EGAM3.e7006_e5984_s3126_r10201_r10210_p4252_tid23879955_00
runNPAnalysis NonPointingAnalysis/STDM4.cfg GridDS: $DAOD OutputDS: user.shqiu.0723_eeyy_0 nc_nFilesPerJob: 1
```

# Config file 

Configuration is done by several config files. The input .cfg file links to various config files. For example, HIGG1D1.cfg includes MCSamples.config HGamRel21.config. MC sample information is stored in HGamAnalysisFramework/data/MCSamples.config. When new Monte Carlo samples are produced, the cross section information will need to be included in the MCSamples.config

Since HGamRel21.config will need to be adjusted for different derivations, derivation specific versions of HGamRel21.config are saved in 

```
NonPointingAnalysis/data/HGamAlternativeConfig/EGAM4Rel21.config
```

Available config files for EGAM3, EGAM4, EGAM9, STDM4 and HIGG1D1 derivations.

These config file paths can be specified in the cfg file, to avoid adding additional config files to the HGamCore package.

# To be added

## 2-D pointing vertex code

## grid job submision script
- check out this one

```
scripts/submit.sh
```


# Data and MC samples

## HIGG1D1 data derivation
```
rucio ls data16_13TeV.*.physics_Main.deriv.DAOD_HIGG1D1.r9264_p3083_p3576 --short > data16_13TeV.DAOD_HIGG1D1.r9264_p3083_p3576.txt
```
more information can be found in this twiki https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HggDerivationSamples#Data15_Data16_Data17_MC16x_cache


# Trigger

The trigger decisions are checked by 
```
HG::StrV passedtrigs = eventHandler()->getPassedTriggers();
```
In the HGamAnalysisFramework, the trigger decisions are only checked for "EventHandler.RequiredTriggers". If you want to save the trigger decisions, please make sure your triggers are included in EventHandler.RequiredTriggers in the HGamRel.config file (or a customized version of this file made by you).The HGamAnalysisFramework also checks the trigger matching while saving the trigger decision. For that purpose, we will need the trigger pT thresholds for the required triggers. The trigger thresholds for diphoton triggers are automatically read from the name of the triggers, but for other triggers, they will have to be specified in EGam4.cfg / HIGG1D1.cfg, etc.. These trigger threshold lines can appear in the HIGG1D1.cfg file, as the same option is not included in HGamRel21.config and so there is no conflict.

